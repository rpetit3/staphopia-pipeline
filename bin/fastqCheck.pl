#! /usr/bin/perl -w
use strict;

###########################################################################################################################################
# Robert Petit - Staphopia FastQ Format Verfification
#
#	Reads a file and tests whether it meets FASTQ format requirements.  The first 1000 reads are tested in order to reduce the time taken.
#
###########################################################################################################################################

open(INPUT, $ARGV[0]);

my $line = 1;
my $line_number = 1;
my $line_length = 0;
my $error = 0;
my $read_count = 0;

my @comparison;
my ($id_line, $read);
my $score = "";
open(INPUT, $ARGV[0]);
while ($read = <INPUT>) {
  # Line 1 = Sequence identifier and optional description and begins with the '@' symbol
  if ($line == 1 && $read =~ m/^@.*/) {
    $line = 2;
    $line_number++;

    # The first line of the FASTQ file is used to create $id_line
    # THIS WILL NEED TO BE IMPROVED AS WE RUN INTO AN ISSUE IF THE ID LINE HAS LESS THEN FOUR CHARACTERS THAT MATCH IN EVERY
    # OTHER ID LINE.  MAYBE TAKE EVERY CHARACTER UNTIL YOU REACH A NON ALPHANUMERIC SYMBOL
    if (!$id_line) {
      # Split the first line into an array, each element being only 5 characters
      @comparison = ($read =~ m/(.{1,5})/gs);

      # Set $id_line equal to the first element, '@****', then remove the '@' symbol
      $id_line = $comparison[0];
      $id_line =~ s/@//;
    }
  }

  # Line 2 = Sequence letters
  elsif ($line == 2) {
    $line = 3;

    # Set the lenth of the sequence line equal to $line_length
    $line_length = length($read);
    $line_number++;
    $read_count++;
  }

  # Line 3 = Begins with the '+' symbol optionally followed by line 1 identifier and description
  elsif ($line == 3 && $read =~ m/^\+(.*|$id_line.*)/) {
    $line = 4;
    $line_number++;
  }

  # Line 4 = Quality scores of the sequences of line 2, it must be the same length as 'Line 2'
  elsif ($line == 4) {
    if ($read =~ m/^\@$id_line.*/) {
      # Check to make sure the length of Sequence data ($line_length) and Quality Score data (length($score) are equal
      if ($line_length == length($score)) {
        $line = 2;
        $line_number++;
        $score = "";
      }
      else {
        $line = 2;
        $error++;
        $line_number++;
        $score = "";
      }
    }
    else {
      $score .= $read; # Continue to append $read to $score until a new entry is reached
    }
  }
  else {
    $line++;
    $line_number++;
    $error++;
    if ($line == 5) {
      $line = 1;
    }
  }

  # Only test the first 1000 reads, and assume the rest is correct
  if ($read_count >= 1000) {
    last;
  }
}
close(INPUT);

if ($error == 0) {
      print "1";
}
else {
      print "0";
}
