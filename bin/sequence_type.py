#! /usr/bin/python
"""
     Author: Robert A Petit III
       Date: 5/15/2013
    
    Determine the sequence type of a sample.

    Input/Output:
      -a|--assembly STRING     Assembled FASTQ file
      -q|--fastq STRING        Input FASTQ file
      -o|--out STRING          Output directory
      -b|--blastdb STRING      Blast database of the assembly
      --mean INT               Mean read length.

    Extra:
      -i|--jobid STRING        Job ID of the sample for database purposes.
      -s|--sample_id STRING    Sample ID of the sample for database purposes.
      
    Optional:
      -h, --help            Show this help message and exit
      --version             Show program's version number and exit
"""

if __name__ == '__main__':
    import sys
    import os
    import time
    import argparse as ap
    from mlst import SequenceType
    from queue import SGE
    
    parser = ap.ArgumentParser(prog='sequence_type.py', conflict_handler='resolve', 
                               description="Determine the sequence type of a sample.")
    group1 = parser.add_argument_group('Input/Output', '')
    group1.add_argument('-a', '--assembly', required=True, help='Assembled FASTA file', metavar="STRING")
    group1.add_argument('-q', '--fastq', required=True, help='Input FASTQ file', metavar="STRING")
    group1.add_argument('-o', '--out', required=True, help='Output directory', metavar="STRING")
    group1.add_argument('-b', '--blastdb', required=True, help='Blast database of the assembly', metavar="STRING")
    group1.add_argument('--mean', required=True, help='Mean read length.', metavar="INT", type=int)

    group2 = parser.add_argument_group('Extra', '')
    group2.add_argument('-i', '--jobid', help='Job ID of sequence', metavar="STRING")
    group2.add_argument('-s', '--sample_id', help='Sample ID of sequence', metavar="STRING")

    group3 = parser.add_argument_group('Optional', '')
    group3.add_argument('-d', '--debug', help='Do not use database functions, use for testing purposes.', 
                                         action='store_true')
    group3.add_argument('-h', '--help', action='help', help='Show this help message and exit')


    if len(sys.argv)==1:
        parser.print_usage()
        sys.exit(1)

    args = parser.parse_args()  
    error = False  
    if not os.path.isfile(args.assembly):
        print "Please check if input assembly '%s' exists, then try again." % args.assembly
        error = True
        
    if not os.path.isfile(args.fastq):
        print "Please check if input FASTQ '%s' exists, then try again." % args.fastq
        error = True
        
    if error:
        print ""
        print "Use --help for more information."
        parser.print_usage()
        sys.exit(2)
        
    # Determine sequence type of Sample
    start = time.time()
    st = SequenceType(args.out+'/logs/sequence_type.log', ' '.join(sys.argv), args.fastq, 
                      args.assembly, args.out, args.mean, args.blastdb)
    if not args.debug:
        st.update_status('2',args.jobid) 
    st.call_st('assembly')
    st.call_st('mapping')
    st.compare_results()
    st.determine_sequence_type()
    runtime = int(round(time.time() - start))
    
    st.print_MLST_results()

    print 'Total runtime: %d seconds' % runtime
    
    if args.jobid and args.sample_id and not args.debug:
        rows = st.insert_stats(args.sample_id, runtime)
        if rows == 1:
            st.update_status('3',args.jobid)            
        else:
            st.update_status('8',args.jobid) 
            print 'There was an error inserting mlst stats.'
            
    st.clean_up()