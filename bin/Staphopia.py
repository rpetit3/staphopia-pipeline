def Usage():
    """Prints usage information for this scipt"""

    print "Staphopia.py - Pipeline executioner."
    print ""
    print "Command line options:"
    print "\t-i | --input    --- Input fastq file"
    print "\t-o | --outdir   --- Output directory"
    print "\t-s | --id       --- Genome id of the input sequence"
    print "\t-n | --name     --- Sample name of the given input."
    print "\t-t | --tech     --- Sequencing technology of input sequence."
  
if __name__ == '__main__':
    import sys
    import getopt
    import os
    import re
    import time
    import Common as cmn
    import Assemble as ass
    import MLST as st
    import FastQ as fq
    import SCCmec as scc
    from multiprocessing import Process

    try:
        opts, remainder = getopt.getopt(sys.argv[1:], 'i:o:s:t:n:', ['input=', 'outdir=', 'id=', 'tech=', 'name='])
    except getopt.GetoptError, error:
        print str(error)
        print Usage()
        sys.exit(2)

    if len(opts) == 0:
        print Usage()
        sys.exit(3)

    outDir  = os.getcwd()
    for opt, arg in opts:
        if opt in ('-i', '--input'):
            input = arg
        elif opt in ('-o', '--outdir'):
            outDir = re.sub("/$", "", arg)
        elif opt in ('-s', '--id'):
            id = arg
        elif opt in ('-t', '--tech'):
            tech = arg
        elif opt in ('-n', '--name'):
            name = arg

    # Initialize Common class, and make output directory
    globalStart = time.time()
    c = cmn.common(outDir, name)

    # Read a fastq file and determine the read count
    wcOut = c.CallCommand(['wc', True, True], ["wc", "-l", input])
    readCount = int(wcOut.split(' ')[0]) / 4
    
    # Initialize the FastQ class, and clean up the input
    start = time.time()
    x = fq.FastqStats(input)
    x.Stats()
    y = fq.FastqReduce(id, input, outDir, x.perRead['mean'], readCount, x.lowerBound, x.upperBound, 50, x.readStats['mean'], x.minAvgQual)
    y.FilterFastQ(c)
    fastqTime = int(round(time.time() - start))
    
    # Initialize Assembly class and conduct the assembly
    start = time.time()
    tech  = '454' if y.readStats['mean'] > 101 else 'Illumina'
    a = ass.assemble(y.fastq, outDir, id, name, c)
    a.runAssembly(tech, y.readStats['mean'])
    a.PrintStats()
    assemblyTime = int(round(time.time() - start))

    # Make the blast directory, then make blast database of the pseudo contig
    c.CallCommand('mkdir', ['mkdir', outDir + '/blastdb'])
    blastdb = outDir + '/blastdb/' + name + '_pseudocontig'
    c.MakeBlastDB(a.pseudoContig, 'nucl', name + ' de-novo assembly pseudocontig', blastdb)
    blastdb = outDir + '/blastdb/' + name + '_assembly'
    c.MakeBlastDB(a.finalAssembly, 'nucl', name + ' de-novo assembly', blastdb)
  
    # Initialize MSLT class, and determine the ST
    start = time.time()
    m = st.mlst(y.fastq, outDir, id, y.readStats['mean'], name, c, blastdb)
    m.callST('assembly')
    m.callST('mapping')
    m.CompareResults()
    m.DetermineST()
    m.PrintMLSTResults()
    mlstTime = int(round(time.time() - start))
    
    # SCCmec
    start = time.time()
    s = scc.sccmec(y.fastq, outDir, id, c, blastdb)
    s.SCCmecMapping(y.readStats['mean'])
    s.SCCmecAssembly()
    s.PrintOutputs()
    sccmecTime = int(round(time.time() - start))
  
    # Resistance and Virulence
    start = time.time()
    c.BlastResVir(blastdb, id)
    resVirTime = int(round(time.time() - start))

    # Insert to database
    c.ExecuteSQL("INSERT", y.sql, y.values)
    c.ExecuteSQL("INSERT", a.sql, a.values)
    c.ExecuteSQL("INSERT", m.sql, m.values)
    c.ExecuteSQL("INSERT", s.sql, s.values)
    
    sql = """INSERT INTO `seqTimes` (`id`, `process`, `pVersion`, `assembly`, `aVersion`, `mlst`, `mVersion`, `sccmec`, 
                                     `sVersion`, `resistance`, `rVersion`, `virulence`, `vVersion`) 
             VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
    values = [id, fastqTime, x.VERSION, assemblyTime, a.VERSION, mlstTime, m.VERSION, sccmecTime, s.VERSION, resVirTime, "0.1", resVirTime, "0.1"]
    
    c.ExecuteSQL("INSERT", sql, values)
  
    # gzip important files.
    y.fastq = c.Compress(y.fastq)
    a.pseudoContig = c.Compress(a.pseudoContig)
    a.finalAssembly = c.Compress(a.finalAssembly)
    
    # Remove temporary files/folders
    c.CleanUp()
  
