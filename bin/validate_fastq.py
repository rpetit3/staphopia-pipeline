#! /usr/bin/python
"""
     Author: Robert A Petit III
       Date: 4/26/2013
    
    Validate an input FASTQ file

    Input/Output:
      -q|--fastq STRING     Input compressed FASTQ file
      -o|--out STRING       Output directory

    Extra:
      -u|--user STRING      User name of the sequence owner.
      -i|--jobid STRING     Job Id of the sequence for database purposes.

    Optional:
      -h, --help            Show this help message and exit
      --version             Show program's version number and exit
"""

if __name__ == '__main__':
    import sys
    import os
    import json
    import argparse as ap
    from queue import SGE
    from validate import ValidateFastQ
    from sample import NewSample

    
    parser = ap.ArgumentParser(prog='validate_fastq.py', conflict_handler='resolve', 
                               description="Validate an input FASTQ file")
    group1 = parser.add_argument_group('Input/Output', '')
    group1.add_argument('-q', '--fastq', required=True, help='Input compressed FASTQ file', metavar="STRING")
    group1.add_argument('-o', '--out', required=True, help='Output directory', metavar="STRING")
    
    group2 = parser.add_argument_group('Extra', '')
    group2.add_argument('-i', '--jobid', help='Job ID of sequence', metavar="STRING")

    group3 = parser.add_argument_group('Optional', '')
    group3.add_argument('-d', '--debug', help='Do not use database functions, use for testing purposes.', 
                                         action='store_true')
    group3.add_argument('--qsub', help='By default, debug will not submit new jobs, use this to do so.', 
                                  action='store_true')
    group3.add_argument('-h', '--help', action='help', help='Show this help message and exit')
    group3.add_argument('--version', action='version', version='%(prog)s v0.1', 
                        help='Show program\'s version number and exit')
                        
    if len(sys.argv)==1:
        parser.print_usage()
        sys.exit(1)

    args = parser.parse_args()  
    error = False
    # Verify input FASTQ file exists
    if not os.path.isfile(args.fastq):
        print "Please check if input '%s' exists, then try again." % args.fastq
        error = True
        
    if error:
        print ""
        print "Use --help for more information."
        parser.print_usage()
        sys.exit(2)
        
    # Validate FastQ
    v = ValidateFastQ( args.out+'/logs/validate_fastq.log', ' '.join(sys.argv) )
    n = NewSample(v.sh)
    if not args.debug:
        rows = n.init_status(args.jobid)
    v.decompress( args.out, args.fastq ) 
    v.test()
    
    if v.is_fastq:
        print "Valid fastq, see log."
    else:
        print "Invalid fastq, see log."
    
    """
    Staphopia Related
    """
    if args.jobid:
        if v.is_fastq:
            rows = n.update_status('Valid', '3', args.jobid)
            if rows == 1:
                rows = n.job_info(args.jobid)   
                if rows[0]['UserName']:
                    
                    # Create a new sample
                    sample_tag = n.sample_tag(rows[0]['UserID'], rows[0]['UserTag'])
                    if sample_tag:
                        
                        sample_id = n.new_sample(rows[0]['UserID'], sample_tag, rows[0]['MD5sum']) 
                        if sample_id:
                            # Rename the files
                            archive = n.rename(args.fastq, sample_tag+'.fastq.bz2')
                            v.fastq = n.rename(v.fastq, sample_tag+'_original.fastq')
                            
                            # Insert information about sample
                            info = n.sample_information(sample_id, json.loads(rows[0]['Post']))
                            
                            # Directories
                            old = '/staphopia-ebs/tmp/'+ rows[0]['MD5sum']
                            new = '/staphopia-ebs/samples/'+ rows[0]['UserName'] +'/'+ sample_tag
                            n.make_sample_directory(new+'/logs/sge')
                            n.make_sample_directory(new+'/scripts')
                            
                            # Queue the next step
                            sge = SGE(args.jobid, new)
                            script = sge.write_process(str(sample_id), new +'/'+ v.fastq, old, args.debug, args.qsub)
                            sge.qsub(script)
                        else:
                            print 'Unable to create new sample using sample tag: %s' % sample_tag
                    else:
                        print 'Unable to create new sample tag'
                else:
                    print 'Unable to determine username based on job ID: %s' % args.jobid
            else:
                print "There was an error updated the database."
        else: 
            rows = n.update_status('Valid', '8', args.jobid)
    elif args.debug:
        sample_tag = 'staphopia_sample'
        v.fastq = n.rename(v.fastq, sample_tag+'_original.fastq')
        
        # Directories
        old = args.out
        new = '/mnt/RAID10/rpetit/staphopia_debug/'+ sample_tag
        n.make_sample_directory(new+'/logs/sge')
        n.make_sample_directory(new+'/scripts')
        
        # Queue the next step
        sge = SGE('test', new)
        script = sge.write_process('test', new +'/'+ v.fastq, old, args.debug, args.qsub)
        if args.qsub:
            sge.qsub(script) 
