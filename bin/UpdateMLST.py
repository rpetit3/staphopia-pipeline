#! /usr/bin/python
# Update the MLST database on the first day of every month.
# Cron: 0 0 1 * * /data1/home/rpetit/staphopia/bin/UpdateMLST.py

if __name__ == '__main__':
  import time, Common
  c = Common.common('/data1/home/rpetit/staphopia/logs','UpdateMLST')
  
  # Get the updated loci, and profiles
  outDir = '/data1/home/rpetit/staphopia/fasta/MLST/'
  c.CallCommand('wget', ['wget', '-N', '-P', outDir, 'http://pubmlst.org/data/alleles/saureus/arcc.tfa'])
  c.CallCommand('wget', ['wget', '-N', '-P', outDir, 'http://pubmlst.org/data/alleles/saureus/aroe.tfa'])
  c.CallCommand('wget', ['wget', '-N', '-P', outDir, 'http://pubmlst.org/data/alleles/saureus/glpf.tfa'])
  c.CallCommand('wget', ['wget', '-N', '-P', outDir, 'http://pubmlst.org/data/alleles/saureus/gmk_.tfa'])
  c.CallCommand('wget', ['wget', '-N', '-P', outDir, 'http://pubmlst.org/data/alleles/saureus/pta_.tfa'])
  c.CallCommand('wget', ['wget', '-N', '-P', outDir, 'http://pubmlst.org/data/alleles/saureus/tpi_.tfa'])
  c.CallCommand('wget', ['wget', '-N', '-P', outDir, 'http://pubmlst.org/data/alleles/saureus/yqil.tfa'])
  c.CallCommand('wget', ['wget', '-N', '-P', outDir, 'http://pubmlst.org/data/profiles/saureus.txt'])
  
  # Update SRST files
  Newman = "/data1/home/rpetit/staphopia/fasta/SA_Newman.fasta"
  arcc   = '/data1/home/rpetit/staphopia/fasta/MLST/arcc.tfa'
  aroe   = '/data1/home/rpetit/staphopia/fasta/MLST/aroe.tfa'
  glpf   = '/data1/home/rpetit/staphopia/fasta/MLST/glpf.tfa'
  gmk_   = '/data1/home/rpetit/staphopia/fasta/MLST/gmk_.tfa'
  pta_   = '/data1/home/rpetit/staphopia/fasta/MLST/pta_.tfa'
  tpi_   = '/data1/home/rpetit/staphopia/fasta/MLST/tpi_.tfa'
  yqil   = '/data1/home/rpetit/staphopia/fasta/MLST/yqil.tfa'
  c.CallCommand('python prepSRST', ['python', c.getPrepSRST(), '-d', Newman, '-o', outDir + 'SRST', arcc, aroe, glpf, gmk_, pta_, tpi_, yqil])

  # TODO: Rerun failed MLST

  

