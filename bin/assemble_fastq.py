#! /usr/bin/python
"""
     Author: Robert A Petit III
       Date: 5/6/2013
    
    Assemble the input fastq file

    Input/Output:
      -q|--fastq STRING     Input compressed FASTQ file
      -o|--out STRING       Output directory
      --min INT             Minimum read length.
      --max INT             Maximum read length.
      --mean INT            Mean read length.

    Extra:
      -i|--jobid STRING     Job Id of the sequence for database purposes.
      
    Optional:
      -h, --help            Show this help message and exit
      --version             Show program's version number and exit
"""

if __name__ == '__main__':
    import sys
    import os
    import time
    import argparse as ap
    from assembly import Assemble
    from queue import SGE
    
    parser = ap.ArgumentParser(prog='assemble_fastq.py', conflict_handler='resolve', 
                               description="Assemble input FASTQ file.")
    group1 = parser.add_argument_group('Input/Output', '')
    group1.add_argument('-q', '--fastq', required=True, help='Input FASTQ file', metavar="STRING")
    group1.add_argument('-o', '--out', required=True, help='Output directory', metavar="STRING")
    group1.add_argument('--min', required=True, help='Minimum read length.', metavar="INT", type=int)
    group1.add_argument('--max', required=True, help='Maximum read length.', metavar="INT", type=int)
    group1.add_argument('--mean', required=True, help='Mean read length.', metavar="INT", type=int)

    group2 = parser.add_argument_group('Extra', '')
    group2.add_argument('-i', '--jobid', help='Job ID of sequence', metavar="STRING")
    group2.add_argument('-s', '--sample_id', help='Sample ID of sequence', metavar="STRING")

    group3 = parser.add_argument_group('Optional', '')
    group3.add_argument('-d', '--debug', help='Do not use database functions, use for testing purposes.', 
                                         action='store_true')
    group3.add_argument('--qsub', help='By default, debug will not submit new jobs, use this to do so.', 
                                  action='store_true')
    group3.add_argument('-h', '--help', action='help', help='Show this help message and exit')
    group3.add_argument('--version', action='version', version='%(prog)s v0.1', 
                                     help='Show program\'s version number and exit')
                                     
    if len(sys.argv)==1:
        parser.print_usage()
        sys.exit(1)

    args = parser.parse_args()  
    error = False
    if args.min > args.max:
        print 'Minimum read length is greater then the maximum read length, please correct this and try again.'
        error = True
        
    if not os.path.isfile(args.fastq):
        print "Please check if input '%s' exists, then try again." % args.fastq
        error = True
        
    if error:
        print ""
        print "Use --help for more information."
        parser.print_usage()
        sys.exit(2)
        
    tech = 'illumina'
    r = args.max - args.min
    if r > 10:
        tech = '454'
        
    # Assemble fastq
    start = time.time()
    a = Assemble(args.out+'/logs/assemble_fastq.log', ' '.join(sys.argv), args.fastq, args.out)
    if not args.debug:
        a.update_status('Assembly', '2', args.jobid)
    a.run_assembly(tech, args.mean)
    runtime = int(round(time.time() - start))
    
    a.print_stats()
    print 'Total runtime: %d seconds' % runtime
    
    if args.jobid and args.sample_id and not args.debug:
        rows = a.insert_stats(args.sample_id, runtime)
        if rows == 1:
            a.update_status('Assembly', '3', args.jobid)
            blastdb_assembly = a.make_blast_database('assembly')
            blastdb_pseudocontig = a.make_blast_database('pseudocontig')
            
            # Queue remaining jobs 
            sge = SGE(args.jobid, args.out)
            mlst = sge.write_mlst(args.fastq, a.final_assembly, args.sample_id, blastdb_assembly, args.mean, args.debug)
            sge.qsub(mlst)
            a.update_status('MLST', '1', args.jobid)
            
            sccmec = sge.write_sccmec(args.fastq, args.sample_id, blastdb_assembly, args.mean, args.debug)
            sge.qsub(sccmec)
            a.update_status('SCCmec', '1', args.jobid)
            
            resistance = sge.write_resistance(args.sample_id, blastdb_assembly, args.debug)
            sge.qsub(resistance)
            a.update_status('Resistance', '1', args.jobid)
            
            virulence = sge.write_virulence(args.sample_id, blastdb_assembly, args.debug)
            sge.qsub(virulence)
            a.update_status('Virulence', '1', args.jobid)
            
            snp = sge.write_snp(args.sample_id, args.fastq, a.sample_tag, args.debug)
            sge.qsub(snp)   
            a.update_status('SNP', '1', args.jobid)
            
            cleanup = sge.write_cleanup(args.fastq)
            sge.qsub(cleanup)   
            
        else:
            print 'There was an error inserting assembly stats.'
    elif args.debug:
        blastdb_assembly = a.make_blast_database('assembly')
        blastdb_pseudocontig = a.make_blast_database('pseudocontig')
        
        # Queue remaining jobs 
        sge = SGE(args.jobid, args.out)
        mlst = sge.write_mlst(args.fastq, a.final_assembly, args.sample_id, blastdb_assembly, args.mean, args.debug)
        sccmec = sge.write_sccmec(args.fastq, args.sample_id, blastdb_assembly, args.mean, args.debug)
        resistance = sge.write_resistance(args.sample_id, blastdb_assembly, args.debug)
        virulence = sge.write_virulence(args.sample_id, blastdb_assembly, args.debug)
        snp = sge.write_snp(args.sample_id, args.fastq, a.sample_tag, args.debug)
        cleanup = sge.write_cleanup(args.fastq)
            
        if args.qsub:
            # Queue remaining jobs 
            sge.qsub(mlst)
            sge.qsub(sccmec)
            sge.qsub(resistance)
            sge.qsub(virulence)       
            sge.qsub(snp) 
            sge.qsub(cleanup)  

    a.clean_up()