#! /usr/bin/perl -w
use strict; 
use threads;
use lib '/data1/home/rpetit/staphopia-pipeline/lib/perl';
use Staphopia;

my $blastdb = $ARGV[0];
my $sid     = $ARGV[1];
my $resVir  = threads->create('denovo', $blastdb, $sid);
$resVir = $resVir->join();

exit;
