#! /usr/bin/python
"""
     Author: Robert A Petit III
       Date: 5/15/2013
    
    Determine the SCCmec type of a sample.

    Input/Output:
      -a|--assembly STRING     Assembled FASTQ file
      -q|--fastq STRING        Input FASTQ file
      -o|--out STRING          Output directory
      -b|--blastdb STRING      Blast database of the assembly
      --mean INT               Mean read length.

    Extra:
      -i|--jobid STRING        Job ID of the sample for database purposes.
      -s|--sample_id STRING    Sample ID of the sample for database purposes.
      
    Optional:
      -h, --help            Show this help message and exit
      --version             Show program's version number and exit
"""

if __name__ == '__main__':
    import sys
    import os
    import time
    import argparse as ap
    from sccmec import SCCmecType
    
    parser = ap.ArgumentParser(prog='sccmec_type.py', conflict_handler='resolve', 
                               description="Determine the SCCmec type of a sample.")
    group1 = parser.add_argument_group('Input/Output', '')
    group1.add_argument('-q', '--fastq', required=True, help='Input FASTQ file', metavar="STRING")
    group1.add_argument('-o', '--out', required=True, help='Output directory', metavar="STRING")
    group1.add_argument('-b', '--blastdb', required=True, help='Blast database of the assembly', metavar="STRING")
    group1.add_argument('--mean', required=True, help='Mean read length.', metavar="INT", type=int)

    group2 = parser.add_argument_group('Extra', '')
    group2.add_argument('-i', '--jobid', help='Job ID of sequence', metavar="STRING")
    group2.add_argument('-s', '--sample_id', help='Sample ID of sequence', metavar="STRING")

    group3 = parser.add_argument_group('Optional', '')
    group3.add_argument('-d', '--debug', help='Do not use database functions, use for testing purposes.', 
                                         action='store_true')
    group3.add_argument('-h', '--help', action='help', help='Show this help message and exit')


    if len(sys.argv)==1:
        parser.print_usage()
        sys.exit(1)

    args = parser.parse_args()  
    error = False          
    if not os.path.isfile(args.fastq):
        print "Please check if input FASTQ '%s' exists, then try again." % args.fastq
        error = True
        
    if error:
        print ""
        print "Use --help for more information."
        parser.print_usage()
        sys.exit(2)    
    
    # SCCmec
    start = time.time()
    s = SCCmecType(args.out+'/logs/sccmec_type.log', ' '.join(sys.argv), args.fastq, args.out, args.blastdb)
    if not args.debug:
        s.update_status('2', args.jobid) 
    s.sccmec_mapping(args.mean)
    s.sccmec_assembly()
    runtime = int(round(time.time() - start))
    
    s.print_results()
    s.draw_plots(args.sample_id)
    print 'Total runtime: %d seconds' % runtime
    
    if args.jobid and args.sample_id and not args.debug:
        rows = s.insert_stats(args.sample_id, runtime)
        if rows:
            s.update_status('3', args.jobid)            
        else:
            s.update_status('8', args.jobid) 
            print 'There was an error inserting sccmec stats.'
            
    s.clean_up()