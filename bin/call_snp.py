#! /usr/bin/python
# TODO: Support for interleaved PE fastq
# TODO: align with all aligners
# TODO: Bowtie2, other aligners
# TODO: GATK filter, VariantFiltration?

if __name__ == '__main__':
    import sys
    import os
    import re
    from snp import SNPpipeline
    import argparse as ap
    
    parser = ap.ArgumentParser(prog='SNPpipeline', conflict_handler='resolve', 
                               description="SNPpipeline - Call SNPs and InDels")
    group1 = parser.add_argument_group('Input', '')
    group1.add_argument('-q', '--fastq', required=True, help='Input FASTQ file', metavar="STRING")
    group1.add_argument('-r', '--reference', metavar="STRING", help='Reference genome in FASTA format.')
    group1.add_argument('-n', '--name', required=True, metavar="STRING", help='Sample name to be used as a prefix.')
    group1.add_argument('-q2', '--fastq2', default='', metavar="STRING", help='Second paired-end FASTQ file.')
    
    group2 = parser.add_argument_group('Output', '')
    group2.add_argument('-o', '--outdir', help='Output directory', metavar="STRING")
    group2.add_argument('--keepfiles', action='store_true', help='Keep intermediate files.')
    group3 = parser.add_argument_group('Aligners', 'Select a specific aligner.')
    group3.add_argument('--bwa', action='store_true', help='Align Illumina reads using bwa. (Default)')
    group3.add_argument('--bwasw', action='store_true', help='Use BWA-SW for 454, IonTorrent reads.')
    group3.add_argument('--mosaik-illumina', action='store_true', help='Align Illumina reads using MOSAIK.')
    group3.add_argument('--mosaik-454', action='store_true', help='Align 454, IonTorrent reads using MOSAIK.')
    group4 = parser.add_argument_group('Callers', 'Choose program(s) to call SNPs/InDels with.')
    group4.add_argument('--all', action='store_true', help='Run all SNP / InDel calling programs.')
    group4.add_argument('--gatk', action='store_true', help='Run GATK SNP / InDel calling. (Default)')
    group4.add_argument('--samtools', action='store_true', help='Run SamTools SNP / InDel calling.')
    group4.add_argument('--varscan', action='store_true', help='Run VarScan SNP / InDel calling.')
    group4.add_argument('--gems', action='store_true', help='Run GeMs SNP calling.')
    group4.add_argument('--freebayes', action='store_true', help='Run Freebayes SNP / InDel calling.')
    group5 = parser.add_argument_group('Annotation', 'Use a GenBank file to annotate SNPs/InDels')
    group5.add_argument('-g', '--genbank', default='', metavar="STRING",
                        help='GenBank file of the reference genome.')
    group6 = parser.add_argument_group('Optional', '')
    group6.add_argument('-i', '--jobid', help='Job ID of sequence', metavar="STRING")
    group6.add_argument('-s', '--sample_id', help='Sample ID of sequence', metavar="STRING")
    group6.add_argument('-d', '--debug', help='Do not use database functions, use for testing purposes.', 
                                         action='store_true')
    group6.add_argument('-v', '--verbose', action='store_true', help='Produce status updates of the run.') 
    group6.add_argument('-h', '--help', action='help', help='Show this help message and exit')
    group6.add_argument('--version', action='version', version='%(prog)s v0.2', 
                        help='Show program\'s version number and exit')

    if len(sys.argv)==1:
        parser.print_usage()
        sys.exit(1)

    args = parser.parse_args()
    
    error = 0
    """ Validate input arguements. """
    # Verify input FASTQ file exists
    if not os.path.isfile(args.fastq):
        error += 1
        print "Please check if input '%s' exists, then try again." % args.fastq

    # Verify reference file exists
    if not args.reference:
        args.reference = '/staphopia-ebs/staphopia-pipeline/fasta/N315.fasta'
    elif not os.path.isfile(args.reference):
        error += 1
        print "Please check if input '%s' exists, then try again." % args.reference
    
    # Check paired end, and verify exists
    if args.fastq2:
        if not os.path.isfile(args.fastq2):
            error += 1
            print "Please check that '%s' exists, then try again." % args.fastq2
        else:
            paired = True
    else:
        paired = False

    # Check annotation and if GenBank file exists
    if args.genbank:
        if not os.path.isfile(args.genbank):
            error += 1
            print "Please check that '%s' exists, then try again." % args.genbank
        else:
            annotate = True
    else:
        annotate = False
    
    # Choose an aligner 
    aligners = 0
    if args.bwa: aligners += 1
    if args.bwasw: aligners += 1
    if args.mosaik_454: aligners += 1
    if args.mosaik_illumina: aligners += 1
    
    if aligners > 1:
        error += 1
        print "Unable to use more than one aligner, please choose only one."
    elif aligners == 1:
        args.bwa = False
    else:
        args.bwa = True

    # Check for errors if so, print usage
    if error:
        print ""
        print "Use --help for more information."
        parser.print_usage()
        sys.exit(2)
    
    # Choose a Caller
    if args.all:
        args.gatk = True
        args.gems = True
        args.varscan = True
        args.samtools = True
        args.freebayes = True
    else:
        callers = 0
        if args.gatk: callers += 1
        if args.samtools: callers += 1
        if args.gems: callers += 1
        if args.varscan: callers += 1
        if args.freebayes: callers += 1
        
        if not callers: args.gatk = True
    
    # If no outdir, use the given name as the outdir
    if not args.outdir:
        args.outdir = args.name

    if args.verbose:
        print ' '.join(sys.argv)
        print ""
        
    # All is well let's get started!
    s = SNPpipeline(args.fastq, args.outdir, args.reference, args.name, True, args.bwasw, paired, args.fastq2, 
                    args.verbose, args.debug, ' '.join(sys.argv), annotate, args.genbank)
    
    # Pick an aligner
    if args.mosaik_illumina:
        s.runMosaik('illumina')
    elif args.mosaik_454:
        s.runMosaik('454')
    else:
        s.runBWA()

    # If asked, run SNP callers
    if args.gatk: s.runGATK()
    if args.samtools: s.runSamTools()
    if args.gems: s.runGeMs()
    if args.varscan: s.runVarScan() 
    if args.freebayes: s.runFreebayes()
    
    # Annotate Final VCF
    if annotate: s.annotateVCF(args.genbank)
    
    # By default clean up intermediate files
    if not args.keepfiles: s.CleanUp()
