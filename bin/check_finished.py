#! /usr/bin/python
"""
     Author: Robert A. Petit III
       Date: 08/05/2013
    Version: v0.1

    Update the Sample.IsFinished status for those jobs that have completed.
    
"""
from database import Database
db = Database()

print "Checking for finished jobs."
print db.update_finished()