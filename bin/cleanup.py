#! /usr/bin/python
"""
     Author: Robert A Petit III
       Date: 7/30/2013
    
    Clean up directory after a job is completed.

    Input/Output:
      -q|--fastq STRING     Input FASTQ file
      -o|--out STRING       Output directory
      
    Optional:
      -h, --help            Show this help message and exit
      --version             Show program's version number and exit
"""

if __name__ == '__main__':
    import sys
    import os
    import time
    import argparse as ap
    from common import Shell
    
    parser = ap.ArgumentParser(prog='cleanup.py', conflict_handler='resolve', 
                               description="Clean up directory after a job is completed.")
    group1 = parser.add_argument_group('Input/Output', '')
    group1.add_argument('-q', '--fastq', required=True, help='Input FASTQ file', metavar="STRING")
    group1.add_argument('-o', '--out', required=True, help='Output directory', metavar="STRING")

    group2 = parser.add_argument_group('Optional', '')
    group2.add_argument('-h', '--help', action='help', help='Show this help message and exit')
    group2.add_argument('--version', action='version', version='%(prog)s v0.1', 
                                     help='Show program\'s version number and exit')
                                     
    if len(sys.argv)==1:
        parser.print_usage()
        sys.exit(1)

    args = parser.parse_args()
    
    log = args.out +'/logs/cleanup.log' 
    sh = Shell(log, ' '.join(sys.argv))
    
    mlst = args.out +'/logs/complete.mlst'
    sccmec = args.out +'/logs/complete.sccmec'
    snp = args.out +'/logs/complete.snp'
    cleanup =  args.out +'/logs/complete.cleanup'
    
    while not os.path.isfile(mlst) or not os.path.isfile(sccmec) or not os.path.isfile(snp):
        # Sleep for 1 minute
        print 'Waiting on jobs to complete, sleeping for 1 minute...'
        time.sleep(60)
    
    if not os.path.isfile(cleanup):
        sh.run_command('bzip2', ['bzip2', '--best', args.fastq])
        sh.run_command('touch', ['touch', cleanup])
    
    