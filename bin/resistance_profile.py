#! /usr/bin/python
"""
     Author: Robert A Petit III
       Date: 5/19/2013
    
    Predict the resistance profile of a sample.

    Input/Output:
      -o|--out STRING          Output directory
      -b|--blastdb STRING      Blast database of the assembly

    Extra:
      -i|--jobid STRING        Job ID of the sample for database purposes.
      -s|--sample_id STRING    Sample ID of the sample for database purposes.
      
    Optional:
      -h, --help            Show this help message and exit
      --version             Show program's version number and exit
"""
if __name__ == '__main__':
    import sys
    import os
    import time
    import argparse as ap
    from resistance import ResistanceType
    
    parser = ap.ArgumentParser(prog='resistance_profile.py', conflict_handler='resolve', 
                               description="Predict the resistance profile of a sample.")
    group1 = parser.add_argument_group('Input/Output', '')
    group1.add_argument('-o', '--out', required=True, help='Output directory', metavar="STRING")
    group1.add_argument('-b', '--blastdb', required=True, help='Blast database of the assembly', metavar="STRING")

    group2 = parser.add_argument_group('Extra', '')
    group2.add_argument('-i', '--jobid', help='Job ID of sequence', metavar="STRING")
    group2.add_argument('-s', '--sample_id', help='Sample ID of sequence', metavar="STRING")

    group3 = parser.add_argument_group('Optional', '')
    group3.add_argument('-d', '--debug', help='Do not use database functions, use for testing purposes.', 
                                         action='store_true')
    group3.add_argument('-h', '--help', action='help', help='Show this help message and exit')


    if len(sys.argv)==1:
        parser.print_usage()
        sys.exit(1)

    args = parser.parse_args()  
    
    # Resistance
    start = time.time()
    r = ResistanceType(args.out+'/logs/resistance_profile.log', ' '.join(sys.argv), args.blastdb)
    
    if not args.debug:
        r.update_status('2', args.jobid) 
        
    r.run_blast()
    runtime = int(round(time.time() - start))
    
    r.print_results()
    print 'Total runtime: %d seconds' % runtime
    
    if args.jobid and args.sample_id and not args.debug:
        rows = r.insert_stats(args.sample_id, runtime)
        if rows:
            r.update_status('3', args.jobid)            
        else:
            r.update_status('8', args.jobid)  
            print 'There was an error inserting resistance stats.'