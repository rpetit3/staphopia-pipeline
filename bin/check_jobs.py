#! /usr/bin/python
"""
     Author: Robert A Petit III
       Date: 7/3/2013
    
    Verify that a job finished, if not resubmit it.
"""
if __name__ == '__main__':
    import os
    import argparse as ap
    from time import sleep
    from jobs import Jobs

    j = Jobs()
    while 1:
        # Check for unfished Samples
        rows = j.unfinished_samples()
        if rows[0]['JobID']:
            for i in xrange(len(rows)):
                # Check Jobs
                script_dir = '/staphopia-ebs/samples/%s/%s/scripts' % rows[i]['UserName'], rows[i]['SampleTag']
                
                # Valid
                if rows[i]['Valid'] != 3:
                    if (j.manage_job(rows[0]['JobID'], 'Valid', rows[i]['Valid'], script_dir, 'Valid')):
                        continue
                    
                # Filter
                if rows[i]['Valid'] == 3 and rows[i]['Filter'] != 3:
                    if (j.manage_job(rows[0]['JobID'], 'Filter', rows[i]['Filter'], script_dir, 'Valid')):
                        continue
                
                # Assembly
                if rows[i]['Filter'] == 3 and rows[i]['Assembly'] != 3:
                    if (j.manage_job(rows[0]['JobID'], 'Assembly', rows[i]['Assembly'], script_dir, 'Filter')):
                        continue
                    
                # MLST
                if rows[i]['Assembly'] == 3 and rows[i]['MLST'] != 3:
                    j.manage_job(rows[0]['JobID'], 'MLST', rows[i]['MLST'], script_dir, 'Assembly')
                    
                # SCCmec
                if rows[i]['Assembly'] == 3 and rows[i]['SCCmec'] != 3:
                    j.manage_job(rows[0]['JobID'], 'SCCmec', rows[i]['SCCmec'], script_dir, 'Assembly')
                    
                # Resistance
                if rows[i]['Assembly'] == 3 and rows[i]['Resistance'] != 3:
                    j.manage_job(rows[0]['JobID'], 'Resistance', rows[i]['Resistance'], script_dir, 'Assembly')
                    
                # Virulence
                if rows[i]['Assembly'] == 3 and rows[i]['Virulence'] != 3:
                    j.manage_job(rows[0]['JobID'], 'Virulence', rows[i]['Virulence'], script_dir, 'Assembly')
        else:
            print 'PASS: No unfinished samples.'
            
        # Check for invalid FASTQ submissions
        rows = j.check_invalid()
        if rows[0]['JobID']:
            for i in xrange(len(rows)):
                # Has it been rerun?
                reruns = j.check_rerun(row[i]['JobID'], 'Valid')
                
                if reruns[0]['RunID']:
                    if reruns[0]['Reruns'] < 5:
                        j.increment_rerun(reruns[0]['RunID'])
                        print j.submit_job('/staphopia-ebs/tmp'+rows[0]['MD5sum']+'/scripts', 'Valid')
                        print 'FastQ Validation for job %s, retry number %s.' % rows[0]['JobID'], reruns[0]['RunID']
                    else:
                        print 'Job %s has been rerun more 5 times, this has been logged.' % rows[0]['JobID']
                else: 
                    rerun = j.insert_rerun(rows[i]['JobID'], 'Valid')
                    print j.submit_job('/staphopia-ebs/tmp'+rows[0]['MD5sum']+'/scripts', 'Valid')
                    print 'FastQ Validation for job %s, retry number 1.' % rows[0]['JobID']
        else:
            print 'PASS: No invalid FASTQ files.'
            
        # Put the script to sleep   
        print 'Sleeping for 5 minutes...'
        print ''
        sleep(300)

