# sccmec_coverage.r sample_id coverage_table.txt output sample_tag
# sccmec_coverage.r 1 /staphopia-ebs/samples/rpetit/rpetit_000001/SCCmec/coverage.txt /var/www/html/staphopia/html/images/ rpetit_000001
library("ggplot2")
library("digest")
args <- commandArgs(trailingOnly = TRUE)

sample_id= args[1]
outdir = args[3]
sample_tag = args[4]
sccmec <- read.table("/staphopia-ebs/staphopia-pipeline/fasta/SCCmec/sccmec.txt", header=TRUE, sep="\t")
cov <- read.table(args[2], header=FALSE, sep="\t")
colnames(cov) <- c("type", "pos", "coverage")
type <- unique(cov$type)

for (i in 1:length(type)) {
  a <- smooth.spline(cov[cov$type == type[i],]$coverage, df=10000);
  d <- data.frame(x = a$x, y = a$y)
  s <- sccmec[sccmec$type == type[i],]; 
  rects <- data.frame(xstart=s$x1, xend=s$x2, gene=s$gene, color=s$color)
  ggplot() + 
    geom_line(data = d, aes(x,y), linetype=1) +
    geom_hline(data = d, aes(yintercept=mean(y)), linetype=4) +
    geom_rect(data = rects, aes(xmin = xstart, xmax = xend, fill=gene,
                                ymin = -Inf, ymax = Inf), alpha = 0.4) +
    coord_cartesian(xlim = c(-10,length(d$x)),ylim = c(-1,max(d$y)+5)) +
    xlab(paste(type[i]," Chromosome Cassette (bp)", sep="")) +
    ylab("Coverage (x)") +
    ggtitle(paste("Sample '",sample_tag,"' Coverage Against SCCmec type '",type[i],"'", sep=""))
  
  sha1 <- digest(paste(sample_id, type[i], sep="_"), algo="sha1", serialize=FALSE)
  dir <- paste(outdir, substr(sha1, 1, 2), "/", sep="")
  dir.create(dir, showWarnings=FALSE) 
  dir <- paste(dir, substr(sha1, 3, 4), "/", sep="")
  dir.create(dir, showWarnings=FALSE) 
  ggsave(paste(dir, sha1,".png", sep=""), width=12, height=4, dpi=100)
  print(paste(sample_id, type[i], sha1, sep=" "))
}
