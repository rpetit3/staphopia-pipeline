#! /usr/bin/env python
# Check if there are any new entries on SRA, if so retrieve them.
# Cron: 0 0 1 * * /data1/home/rpetit/staphopia/bin/UpdateMLST.py

import urllib
import re

class REMatcher(object):
    def __init__(self, matchstring):
        self.matchstring = matchstring

    def match(self,regexp):
        self.rematch = re.match(regexp, self.matchstring)
        return bool(self.rematch)

    def group(self,i):
        return self.rematch.group(i)

class slurpSRA():
  
  def __init__(self, cmn):
    self.__ids        = []
    self.__publicRuns = []
    self.__UTILS      = 'http://www.ncbi.nlm.nih.gov/entrez/eutils'
    self.__retmax     = 100000
    self.__query      = """("Staphylococcus aureus"[Organism] NOT "Streptococcus pneumoniae"[orgn] NOT "unidentified"[orgn] NOT "Ostrya rehderiana"[orgn] NOT "Staphylococcus warneri RN833"[orgn] NOT "Staphylococcus xylosus"[orgn]) NOT cluster_dbgap[PROP] AND cluster_public[PROP] AND "sra_public"[Filter] AND "type_genome"[Filter]"""
    self.__allFH      = open('/data1/home/rpetit/staphopia/logs/UpdateSRA/Summary.txt', 'w')
    self.__runFH      = open('/data1/home/rpetit/staphopia/logs/UpdateSRA/Runs.txt', 'w')
    self.__runToExp   = open('/data1/home/rpetit/staphopia/logs/UpdateSRA/Run2Exp.txt', 'w')
    self.__wgetFH     = open('/data1/home/rpetit/staphopia/logs/UpdateSRA/Downloads.txt', 'w')
    self.__cmn        = cmn
        
    self.eSearch()
    for i in range(len(self.__ids)):
      print 'Working on '+ str(i+1) +' of '+ str(len(self.__ids))
      self.__reInit(self.__ids[i])
      self.eLink(self.__ids[i])
      self.eSummary(self.__ids[i])
      self.printAll()
      self.printRunToExp()
      self.downloadRuns()
      
  def __reInit(self, id):
    """ Reset the experiment values to empty strings. """
    self.__uid          = id
    self.__title        = ''
    self.__instrument   = ''
    self.__platform     = ''
    self.__publicRuns   = []
    self.__totalRuns    = ''
    self.__totalSpots   = ''
    self.__totalBases   = ''
    self.__totalSize    = ''
    self.__loadDone     = ''
    self.__clusterName  = ''
    self.__submitterAcc = ''
    self.__centerName   = ''
    self.__contactName  = ''
    self.__labName      = ''
    self.__expAcc       = ''
    self.__expVersion   = ''
    self.__expStatus    = ''
    self.__expName      = ''
    self.__studyAcc     = ''
    self.__studyName    = ''
    self.__taxID        = ''
    self.__sciName      = ''
    self.__cmnName      = ''
    self.__sampleAcc    = ''
    self.__sampleName   = ''
    self.__libName      = ''
    self.__libStrategy  = ''
    self.__libSource    = ''
    self.__libSelection = ''
    self.__libConstruct = ''
    self.__createDate   = ''
    self.__updateDate   = ''
    self.__output       = ''
    self.__pmids        = []
    self.__isPublished  = False
    self.__pubMed       = ''
    
  def eSearch(self):
    """ Query NCBI using eSearch """
    q = self.__UTILS + '/esearch.fcgi?db=sra&retmax=' + str(self.__retmax) + '&term=' + self.__query
    f = urllib.urlopen(q)
    l = f.read().split('\n')
    f.close()
    for i in range(len(l)):
      m = REMatcher(l[i])
      if (m.match(r'^\s+<Id>(\d+)</Id>')):
        self.__ids.append(m.group(1))
    
  def eSummary(self, id):
    """ Get summary information for a given id using eSummary """
    q = self.__UTILS + '/esummary.fcgi?db=sra&id=' + id
    f = urllib.urlopen(q)
    pat = re.compile(r'\t+')
    
    # Remove whitespace, then substitute >/<
    s = pat.sub('', f.read())
    f.close()
    r = s.replace('&lt;', '<')
    r = r.replace('&gt;', '>')
    r = r.replace('><', '>><<').split('><')
    
    # Parse the summary
    for i in range(len(r)):
      #print r[i]
      self.parseSummary(r[i])
      
    
  def eLink(self, id):
    """ Determine if the given id has a link to PubMed. """
    q = self.__UTILS + '/elink.fcgi?dbfrom=sra&db=pubmed&id=' + id
    f = urllib.urlopen(q)
    l = f.read().split('\n')
    f.close()
    for i in range(len(l)):
      m = REMatcher(l[i])
      if (m.match(r'^\s+<Id>(\d+)</Id>')):
        if (m.group(1) != id):
          self.__pmids.append(m.group(1))
          self.__isPublished = True
          
    if (self.__isPublished):
      self.__pubMed = 'Pubmed=' + ':'.join(self.__pmids)
    else:
      self.__pubMed = 'Unpublished'
    
  def parseSummary(self, line):
    """ Parse the input line and set the values """
    m = REMatcher(line)
    if (m.match(r'<Id>(.*)</Id>')):
      self.__uid     = m.group(1)
    elif (m.match(r'<Title>(.*)</Title>')):
      self.__title   = m.group(1)
    elif (m.match(r'<Platform instrument_model="(.*)">(.*)</Platform>')):
      self.__instrument = m.group(1)
      self.__platform   = m.group(2)
    elif (m.match(r'<Statistics total_runs="(.*)" total_spots="(.*)" total_bases="(.*)" total_size="(.*)" load_done="(.*)" cluster_name="(.*)"/>')):
      self.__totalRuns   = m.group(1)
      self.__totalSpots  = m.group(2)
      self.__totalBases  = m.group(3)
      self.__totalSize   = m.group(4)
      self.__loadDone    = m.group(5)
      self.__clusterName = m.group(6)
    elif (m.match(r'<Submitter acc="(.*)" center_name="(.*)" contact_name="(.*)" lab_name="(.*)"/>')):
      self.__submitterAcc = m.group(1)
      self.__centerName   = m.group(2)
      self.__contactName  = m.group(3)
      self.__labName      = m.group(4)
    elif (m.match(r'<Experiment acc="(.*)" ver="(.*)" status="(.*)" name="(.*)"/>')):
      self.__expAcc     = m.group(1)
      self.__expVersion = m.group(2)
      self.__expStatus  = m.group(3)
      self.__expName    = m.group(4)
    elif (m.match(r'<Study acc="(.*)" name="(.*)"/>')):
      self.__studyAcc  = m.group(1)
      self.__studyName = m.group(2)
    elif (m.match(r'<Organism taxid="(.*)" ScientificName="(.*)" CommonName="(.*)"/>')):
      if (m.group(1) != '32644'):
        self.__taxID   = m.group(1)
        self.__sciName = m.group(2)
        self.__cmnName = m.group(3)
    elif (m.match(r'<Organism taxid="(.*)" ScientificName="(.*)"/>')):
      if (m.group(1) != '32644'):
        self.__taxID   = m.group(1)
        self.__sciName = m.group(2)
    elif (m.match(r'<Organism taxid="(.*)" CommonName="(.*)"/>')):
      if (m.group(1) != '32644'):
        self.__taxID   = m.group(1)
        self.__cmnName = m.group(2)
    elif (m.match(r'<Sample acc="(.*)" name="(.*)"/>')):
      self.__sampleAcc  = m.group(1)
      self.__sampleName = m.group(2)
    elif (m.match(r'<LIBRARY_NAME>(.*)</LIBRARY_NAME>')):
      self.__libName = m.group(1)
    elif (m.match(r'<LIBRARY_STRATEGY>(.*)</LIBRARY_STRATEGY>')):
      self.__libStrategy = m.group(1)
    elif (m.match(r'<LIBRARY_SOURCE>(.*)</LIBRARY_SOURCE>')):
      self.__libSource = m.group(1)
    elif (m.match(r'<LIBRARY_SELECTION>(.*)</LIBRARY_SELECTION>')):
      self.__libSelection = m.group(1)
    elif (m.match(r'<LIBRARY_CONSTRUCTION_PROTOCOL>(.*)</LIBRARY_CONSTRUCTION_PROTOCOL>')):
      self.__libConstruct  = m.group(1)
    elif (m.match(r'<Run acc="(.*)" total_spots="(.*)" total_bases="(.*)" load_done="(.*)" is_public="(.*)" cluster_name="(.*)" static_data_available="(.*)"/>')):
      self.printRun(self.__uid, m.group(1), m.group(2), m.group(3), m.group(4), m.group(5), m.group(6), m.group(7))
      if (m.group(6) == "public"):
        self.__publicRuns.append(m.group(1))
    elif (m.match(r'<Item Name="CreateDate" Type="String">(.*)</Item>')):
      self.__createDate = m.group(1)
    elif (m.match(r'<Item Name="UpdateDate" Type="String">(.*)</Item>')):
      self.__updateDate = m.group(1)
      
  def downloadRuns(self):
    """ Go to SRA and retrieve the run. """
    import os
    for i in range(len(self.__publicRuns)):
      run = self.__publicRuns[i]
      print '   Working on '+ run +' (' + str(i+1) +' of '+ str(len(self.__publicRuns))+ ')'
      url = 'anonftp@ftp-private.ncbi.nlm.nih.gov:/sra/sra-instant/reads/ByRun/sra/'+ run[0:3] +'/'+ run[0:6] +'/'+ run +'/'+ run + '.sra'
      sra = '/data1/home/rpetit/readgp/StaphSRA/SRA/'+ run +'.sra'
      exists = ''
      
      if not os.path.exists(sra):
        sra = '/data1/home/rpetit/readgp/StaphSRA/tmp/'+ run +'.sra'
        self.__cmn.CallCommand('ascp', ['/data1/home/rpetit/.aspera/connect/bin/ascp', '-i', '/data1/home/rpetit/.aspera/connect/etc/asperaweb_id_dsa.putty', '-Q', '-l450m', url, '/data1/home/rpetit/readgp/StaphSRA/tmp/']);
        exists = 'Downloaded'
      else:
        self.__cmn.CallCommand('mv', ['mv', sra, '/data1/home/rpetit/readgp/StaphSRA/tmp/'+ run +'.sra'])
        sra = '/data1/home/rpetit/readgp/StaphSRA/tmp/'+ run +'.sra'
        exists = 'Already existed'
      
      if not os.path.exists(sra):
        url = 'anonftp@ftp-private.ncbi.nlm.nih.gov:/sra/sra-instant/reads/ByRun/sra/'+ run[0:3] +'/'+ run[0:6] +'/'+ run +'/'+ run + '.csra'
        sra = '/data1/home/rpetit/readgp/StaphSRA/SRA/'+ run +'.csra'
        
        if not os.path.exists(sra):
          sra = '/data1/home/rpetit/readgp/StaphSRA/tmp/'+ run +'.csra'
          self.__cmn.CallCommand('ascp', ['/data1/home/rpetit/.aspera/connect/bin/ascp', '-i', '/data1/home/rpetit/.aspera/connect/etc/asperaweb_id_dsa.putty', '-Q', '-l450m', url, '/data1/home/rpetit/readgp/StaphSRA/tmp/']);
          exists = 'Downloaded'
        else:
          self.__cmn.CallCommand('mv', ['mv', sra, '/data1/home/rpetit/readgp/StaphSRA/tmp/'+ run +'.csra'])
          sra = '/data1/home/rpetit/readgp/StaphSRA/SRA/'+ run +'.csra'
          exists = 'Already existed'
        
      if os.path.exists(sra):
        print '      Running fastq-dump on '+ sra
        self.fastqDump(sra, run, self.__expAcc, self.__sampleAcc)
        self.__wgetFH.write(self.__uid +'\t'+ exists +'\t'+ sra +'\t'+ url +'\n')
      else:
        self.__wgetFH.write(self.__uid +'\tError Please Check\t'+ sra +'\t'+ url +'\n')


  def fastqDump(self, sra, run, exp, sample):
    """ Convert the sra formated file into fastq. """
    dumpFH = open('/data1/home/rpetit/readgp/StaphSRA/tmp/fastq-dump.sh', 'w')
    dumpFH.write('#!/bin/bash\n');
    dumpFH.write('### Change to the current working directory:\n')
    dumpFH.write('#$ -wd /data1/home/rpetit/readgp/StaphSRA/tmp/\n')
    dumpFH.write('### Job name:\n')
    dumpFH.write('#$ -o /data1/home/rpetit/readgp/StaphSRA/logs/'+ run +'.out\n')
    dumpFH.write('#$ -e /data1/home/rpetit/readgp/StaphSRA/logs/'+ run +'.err\n')
    dumpFH.write('#$ -N '+ run +'\n')
    dumpFH.write('#$ -S /bin/bash\n')
    dumpFH.write('mkdir /data1/home/rpetit/readgp/StaphSRA/tmp/'+ run +'\n')
    dumpFH.write('mkdir -p /data1/home/rpetit/readgp/StaphSRA/Samples/'+ sample +'\n')
    dumpFH.write('perl /data1/home/rpetit/bin/configuration-assistant.perl ' + sra + '\n')
    dumpFH.write('/data1/home/rpetit/bin/fastq-dump '+ sra +' -O /data1/home/rpetit/readgp/StaphSRA/tmp/'+ run +' --split-3 --dumpbase --accession '+ run +' --minReadLen 20\n')
    dumpFH.write('count=0\n');
    dumpFH.write('if [ -e /data1/home/rpetit/readgp/StaphSRA/tmp/'+ run +'/'+ run +'_1.fastq ]; then \n');
    dumpFH.write('    count=$((count+1))\n');
    dumpFH.write('    cat /data1/home/rpetit/readgp/StaphSRA/tmp/'+ run +'/'+ run +'_1.fastq >> /data1/home/rpetit/readgp/StaphSRA/Samples/'+ sample +'/'+ exp +'_1.fastq\n') 
    dumpFH.write('    echo "'+ run +'_1" >> /data1/home/rpetit/readgp/StaphSRA/Samples/'+ sample +'/'+ exp +'.runs\n')
    dumpFH.write('fi\n');
    dumpFH.write('if [ -e /data1/home/rpetit/readgp/StaphSRA/tmp/'+ run +'/'+ run +'_2.fastq ]; then \n');
    dumpFH.write('    count=$((count+1))\n');
    dumpFH.write('    cat /data1/home/rpetit/readgp/StaphSRA/tmp/'+ run +'/'+ run +'_2.fastq >> /data1/home/rpetit/readgp/StaphSRA/Samples/'+ sample +'/'+ exp +'_2.fastq\n') 
    dumpFH.write('    echo "'+ run +'_2" >> /data1/home/rpetit/readgp/StaphSRA/Samples/'+ sample +'/'+ exp +'.runs\n')
    dumpFH.write('fi\n');
    dumpFH.write('if [ -e /data1/home/rpetit/readgp/StaphSRA/tmp/'+ run +'/'+ run +'.fastq ]; then \n');
    dumpFH.write('    count=$((count+1))\n');
    dumpFH.write('    cat /data1/home/rpetit/readgp/StaphSRA/tmp/'+ run +'/'+ run +'.fastq >> /data1/home/rpetit/readgp/StaphSRA/Samples/'+ sample +'/'+ exp +'.fastq\n') 
    dumpFH.write('    echo "'+ run +'" >> /data1/home/rpetit/readgp/StaphSRA/Samples/'+ sample +'/'+ exp +'.runs\n')
    dumpFH.write('fi\n');
    dumpFH.write('if [ $count -gt 2 ]; then\n');
    dumpFH.write('    echo "'+ exp +':'+ run +':$count" >> /data1/home/rpetit/readgp/StaphSRA/CheckThese.runs\n');
    dumpFH.write('fi\n');
    dumpFH.write('rm -r /data1/home/rpetit/readgp/StaphSRA/tmp/'+ run +'/\n')
    dumpFH.write('rm -r /data1/home/rpetit/readgp/StaphSRA/tmp/'+ run +'.sra\n')
    dumpFH.close()
    self.__cmn.CallCommand('qsub', ['qsub', '/data1/home/rpetit/readgp/StaphSRA/tmp/fastq-dump.sh'])

  
  def printAll(self):
    """ Print all of the summary information. """
    #  0) Experiment Accession    10) Experiment Name         20) Submitting Center Contact     30) Cluster Name
    #  1) SRA UID                 11) Library Name            21) Submitting Lab                31) Create Date
    #  2) Title                   12) Library Strategy        22) Experiment Version            32) Update Date
    #  3) Sample Accession        13) Library Source          23) Experiment Status             33) Publication
    #  4) Taxon ID                14) Library Selection       24) Total Runs
    #  5) Scientific Name         15) Library Contruct        25) Total Public Runs
    #  6) Common Name             16) Instrument              26) Total Spots
    #  7) Sample Name             17) Platform                27) Total Bases   
    #  8) Study Accession         18) Submitter Accession     28) Total Size 
    #  9) Study Name              19) Submitting Center       29) Load Done
    
    output = [ self.__expAcc, self.__uid, self.__title, self.__sampleAcc, self.__taxID, self.__sciName, self.__cmnName, 
               self.__sampleName, self.__studyAcc, self.__studyName, self.__expName, self.__libName, self.__libStrategy,
               self.__libSource, self.__libSelection, self.__libConstruct, self.__instrument, self.__platform,  
               self.__submitterAcc, self.__centerName, self.__contactName, self.__labName, self.__expVersion, 
               self.__expStatus, self.__totalRuns, str(len(self.__publicRuns)), self.__totalSpots, self.__totalBases, 
               self.__totalSize, self.__loadDone, self.__clusterName, self.__createDate, self.__updateDate, self.__pubMed ]
    o = '\t'.join(output)
    self.__allFH.write(o + '\n')
    
  def printRun(self, id, acc, spots, bases, load, public, cluster, static):
    """ Print run information. """
    self.__runFH.write('ID=' + id + '\tRunACC=' + acc + '\tRunTotalSpots=' + spots + '\tRunTotalBases=' + bases + '\tRunLoadDone=' + load)
    self.__runFH.write('\tRunIsPublic=' + public + '\tRunClusterName=' + cluster + '\tRunAvailable=' + static + '\n')
    
  def printRunToExp(self):
    """ Print Run to Experiment information. """
    for i in range(len(self.__publicRuns)):
      self.__runToExp.write(self.__uid + '\t' + self.__publicRuns[i] + '\t' + self.__expAcc + '\t' + self.__sampleAcc + '\n')

if __name__ == '__main__':
  import Common
  c = Common.common('/data1/home/rpetit/staphopia/logs/UpdateSRA', 'UpdateSRA')
  SRA = slurpSRA(c)
