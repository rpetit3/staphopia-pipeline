#! /usr/bin/python
"""
     Author: Robert A Petit III
       Date: 5/2/2013
    
    Filter low quality reads, and reduce the read count to a given coverage.

    Input/Output:
      -q|--fastq STRING     Input compressed FASTQ file
      -o|--out STRING       Output directory

    Extra:
      -i|--jobid STRING     Job Id of the sequence for database purposes.
      --max_cov INT         Maximum coverage to keep.
      --genome_size INT     Required to esimate coverage.
      
    Optional:
      -h, --help            Show this help message and exit
      --version             Show program's version number and exit
"""

if __name__ == '__main__':
    import sys
    import os
    import time
    import argparse as ap
    from fastq import ProcessFastQ
    from queue import SGE
    
    parser = ap.ArgumentParser(prog='process_fastq.py', conflict_handler='resolve', 
                               description="Filter low quality reads, and reduce the read count to a given coverage.")
    group1 = parser.add_argument_group('Input/Output', '')
    group1.add_argument('-q', '--fastq', required=True, help='Input FASTQ file', metavar="STRING")
    group1.add_argument('-o', '--out', required=True, help='Output directory', metavar="STRING")
    
    group2 = parser.add_argument_group('Extra', '')
    group2.add_argument('-i', '--jobid', help='Job ID of sequence', metavar="STRING")
    group2.add_argument('-s', '--sample_id', help='Sample ID of sequence', metavar="STRING")
    group2.add_argument('--original', help='Save stats of the original FASTQ to the database', action='store_true')
    group2.add_argument('--cov', help='Maximum coverage to keep. (Default: 50)', metavar="INT", type=int, default=50)
    group2.add_argument('--genome_size', help='Required to esimate coverage. (Default: 2878897)', metavar="INT", 
                                         type=int, default=2878897)

    group3 = parser.add_argument_group('Optional', '')
    group3.add_argument('-d', '--debug', help='Do not use database functions, use for testing purposes.', 
                                         action='store_true')
    group3.add_argument('--qsub', help='By default, debug will not submit new jobs, use this to do so.', 
                                  action='store_true')
    group3.add_argument('-h', '--help', action='help', help='Show this help message and exit')
    group3.add_argument('--version', action='version', version='%(prog)s v0.1', 
                                     help='Show program\'s version number and exit')
                                     
    if len(sys.argv)==1:
        parser.print_usage()
        sys.exit(1)

    args = parser.parse_args()  
    error = False
    # Verify input FASTQ file exists
    if not os.path.isfile(args.fastq):
        print "Please check if input '%s' exists, then try again." % args.fastq
        error = True
        
    if error:
        print ""
        print "Use --help for more information."
        parser.print_usage()
        sys.exit(2)
        
    # Process FastQ
    start = time.time()
    fq = ProcessFastQ(args.out+'/logs/process_fastq.log', ' '.join(sys.argv))
    if not args.debug:
        fq.update_status('Filter', '2', args.jobid)
    fq.estimate_stats(args.fastq)
    fq.process_fastq(args.out, args.fastq, args.cov, args.genome_size)
    runtime = int(round(time.time() - start))
    
    fq.print_stats(args.genome_size)
    print 'Total runtime: %d seconds' % runtime

    if args.jobid and args.sample_id and not args.debug:
        
        if args.original:
            rows = fq.insert_stats(True, args.sample_id, runtime, args.genome_size)
            if rows != 1:
                print 'Error inserting original stats into the FilteredStats table'
            
        rows = fq.insert_stats(False, args.sample_id, runtime, args.genome_size)
        if rows == 1:
            # Update status
            fq.update_status('Filter', '3', args.jobid)
            
            # Queue the next step
            sge = SGE(args.jobid, args.out)
            script = sge.write_assembly(fq.fastq, args.sample_id, fq.final_stats['length']['min'], 
                                        fq.final_stats['length']['max'], fq.final_stats['length']['mean'], args.debug,
                                        args.qsub)
            sge.qsub(script)
            fq.update_status('Assembly', '1', args.jobid)
        else:
            print 'Error inserting stats into FilteredStats table'
    elif args.debug:
        sge = SGE(args.jobid, args.out)
        script = sge.write_assembly(fq.fastq, args.sample_id, fq.final_stats['length']['min'], 
                                    fq.final_stats['length']['max'], fq.final_stats['length']['mean'], args.debug,
                                    args.qsub)
        if args.qsub:
            sge.qsub(script)

    fq.clean_up(args.fastq)