# This is a helper script to set up inputs for SRST. See srst.sourceforge.net for details.
# Note this version was updated on March 1, 2012.
# Contact: kholt@unimelb.edu.au

import string, re
import os, sys, subprocess
from optparse import OptionParser
from Bio.Blast.Applications import NcbiblastnCommandline
from Bio.Blast import NCBIXML
from Bio import SeqIO
from Bio.SeqFeature import SeqFeature, FeatureLocation
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import generic_dna

def main():

  usage = "usage: %prog [options]"
  parser = OptionParser(usage=usage)

  parser.add_option("-d", "--db", action="store", dest="db", help="database (fasta)", default="")
  parser.add_option("-o", "--out", action="store", dest="out", help="output file", default="")

  return parser.parse_args()


if __name__ == "__main__":

  (options, args) = main()

  # prepare database
  if options.db == "":
    # go without flanking sequences
    for seq in args:
      (seqDir,seqFileName) = os.path.split(seq)	
      (seqBaseName,ext) = os.path.splitext(seqFileName)

      # generate file list for srst
      print '\t'.join([seqBaseName,seq,"NA"])
		
  else:
    # get flanking sequences
    if not os.path.exists(options.db + ".nin"):
      os.system("makeblastdb -in " + options.db + " -dbtype nucl" + " -logfile "+ options.out + "/blast.log")

    refseq_record = SeqIO.read(options.db, "fasta", generic_dna)
    refseq_record.id = "MLSTloci"
    refseq_record.name = "MLSTloci"

    fh = open(options.out + "/summary.txt", "w")
    for seq in args:

      (seqDir,seqFileName) = os.path.split(seq)	
      (seqBaseName,ext) = os.path.splitext(seqFileName)
      bait = options.out + "/" + seqBaseName + "_bait.fasta"

      # extract first sequence to use as bait
      os.system(' '.join(['seqret',seq,'-firstonly','-auto','-out',bait]))
      

      cline = NcbiblastnCommandline(query=bait, db=options.db,evalue=0.001, out=options.out + "/my_blast_tmp.xml", outfmt=5)
      stdout, stderr = cline()

      result_handle = open(options.out + "/my_blast_tmp.xml")
      blast_record = NCBIXML.read(result_handle) 
      query_length = blast_record.query_letters
      for alignment in blast_record.alignments:
        hsp = alignment.hsps[0] # only consider top hit
				
        if hsp.align_length/float(query_length) > 0.5:
				
          subject_start = hsp.sbjct_start - (hsp.query_start - 1)
          subject_end = hsp.sbjct_end + (query_length - hsp.query_end)

          revcomp = 1 # hit is in forward strand
          if hsp.sbjct_start > hsp.sbjct_end:
            revcomp = -1
	
          left_coords = [min(hsp.sbjct_start,hsp.sbjct_end)-100,min(hsp.sbjct_start,hsp.sbjct_end)-1]
          right_coords = [max(hsp.sbjct_start,hsp.sbjct_end)+1,max(hsp.sbjct_start,hsp.sbjct_end)+100]

          left_cmd = ["seqret ",options.db," -sbegin ",str(left_coords[0])," -send ",str(left_coords[1])," -osformat fasta -auto -out " + options.out + "/tmp_left_flank.fasta"]
          os.system(''.join(left_cmd)) # extract left flank using emboss
          right_cmd = ["seqret ",options.db," -sbegin ",str(right_coords[0])," -send ",str(right_coords[1])," -osformat fasta -auto -out " + options.out + "/tmp_right_flank.fasta"]
          os.system(''.join(right_cmd)) # extract right flank using emboss

          left_record = SeqIO.read(options.out + "/tmp_left_flank.fasta", "fasta")
          if revcomp < 0:
            left_record.id = "down"
            left_record.seq = left_record.seq.reverse_complement() # reverse the sequence
          else:
            left_record.id = "up"
						
          right_record = SeqIO.read(options.out + "/tmp_right_flank.fasta", "fasta")
          if revcomp < 0:
            right_record.id = "up"
            right_record.seq = right_record.seq.reverse_complement() # reverse the sequence
          else:
            right_record.id = "down"
						
          right_record.description = ""
          left_record.description = ""

          out_handle = open(options.out + "/" + seqBaseName + "_flanks.fasta", "w")
          out_handle.write(right_record.format("fasta"))
          out_handle.write(left_record.format("fasta"))
          out_handle.close()
					
          # generate file list for srst
          fh.write('\t'.join([seqBaseName,seq,seqBaseName + "_flanks.fasta"]) + "\n")

          # annotate features
          #refseq_record.features.append(SeqFeature(FeatureLocation(left_coords[0]-1, left_coords[1]), type="misc_feature", strand=revcomp, qualifiers = {'note' : [seqBaseName + ' flank']}))
          #refseq_record.features.append(SeqFeature(FeatureLocation(right_coords[0]-1, right_coords[1]), type="misc_feature", strand=revcomp, qualifiers = {'note' : [seqBaseName + ' flank']}))
          #refseq_record.features.append(SeqFeature(FeatureLocation(hsp.sbjct_start-1,hsp.sbjct_end), type="misc_feature", strand=revcomp, qualifiers = {'note' : [seqBaseName + ' locus']}))

    #SeqIO.write(refseq_record, options.db.split('.')[0] + "_MLSTloci.gbk", "genbank")
    fh.close()
    # clean up tmp files
    os.system("rm -f " + options.db  + ".n*")
    os.system("rm -f " + options.out + "/tmp_right_flank.fasta")
    os.system("rm -f " + options.out + "/tmp_left_flank.fasta")
    os.system("rm -f " + options.out + "/my_blast_tmp.xml")
