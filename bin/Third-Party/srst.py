#Copyright 2011 NICTA All rights reserved.
#
#Redistribution and use in source and binary forms, with or without modification, are
#permitted provided that the following conditions are met:
#
#   1. Redistributions of source code must retain the above copyright notice, this list of
#      conditions and the following disclaimer.
#
#   2. Redistributions in binary form must reproduce the above copyright notice, this list
#      of conditions and the following disclaimer in the documentation and/or other materials
#      provided with the distribution.
#
#THIS SOFTWARE IS PROVIDED BY <COPYRIGHT HOLDER> ''AS IS'' AND ANY EXPRESS OR IMPLIED
#WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
#FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> OR
#CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
#CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
#ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#The views and conclusions contained in the software and documentation are those of the
#authors and should not be interpreted as representing official policies, either expressed
#or implied, of NICTA.
#
#LAST UPDATE: Aug 4, 2012. kholt@unimelb.edu.au

import getopt
import os
import os.path
import pickle
import re
import subprocess
import sys
import Bio.Seq
import Bio.SeqIO
import Bio.SeqRecord

from math import *

#verbose = False

# binomial calculations
logFacCache = {0:0.0, 1:0.0, 2:log(2), 3:log(6), 4:log(24), 5:log(129)}

def logFac(n):
	if n in logFacCache:
		return logFacCache[n]
	r = n * log(n) - n + log(n * (1 + 4 * n * (1 + 2 * n))) / 6 + log(pi) / 2
	logFacCache[n] = r
	return r

def logChoose(n, k):
	return logFac(n) - logFac(k) - logFac(n - k)

def logBinEq(p, n, k):
	return logChoose(n, k) + log(p) * k + log(1 - p) * (n - k)

def logBinLe(p, n, k):
	return sum([logBinEq(p, n, j) for j in range(0, k + 1)])

logBinGeCache = {}
def logBinGe(p, n, k):
	if (n,k) in logBinGeCache:
		return logBinGeCache[(n,k)]
	r = sum([logBinEq(p, n, j) for j in range(k, n + 1)])
	if n < 20:
		logBinGeCache[(n,k)] = r
	return r

def logAdd(a, b):
	x1 = max(a, b)
	x2 = min(a, b)
	w = int(x1)
	y1 = x1 - w
	y2 = x2 - w
	z = exp(y1) + exp(y2)
	return w + log(z)

# functions
def expandSam(inf, outf):
	for l in inf:
		if len(l) == 0:
			continue
		if l[0] == '@':
			outf.write(l)
			continue
		t = l.split()
		if t[1] == '4':
			# Gak! We need to keep at least one
			# because samtools breaks if there are
			# no alignments. :-(
			outf.write(l)
			continue
		outf.write(l)
		tags = {}
		for f in t[11:]:
			g = f.split(':')
			tags[g[0]] = g
		if 'XA' not in tags:
			continue
		alts=tags['XA'][2].split(';')
		for alt in alts:
			v = alt.split(',')
			if len(v) < 3:
				continue
			pos = int(v[1])
			if pos > 0:
				r = (t[0], '0', v[0], str(pos), t[4], v[2], '*', '0', '0', t[9], t[10])
			else:
				r = (t[0], '16', v[0], str(-pos), t[4], v[2], '*', '0', '0', t[9], t[10])
			outf.write('\t'.join(r) + '\n')

# parse species information
def prepare(specFn, workDir, bwa):
  (specDir,summaryFileName) = os.path.split(specFn)
  spc = [] # (locus name, variants fasta, flanking sequences)
  for l in open(specFn):
    spc.append(l.split())

  # Build the reference sequence file
  try:
    os.makedirs(workDir)
  except OSError:
    pass

  refFn = os.path.join(workDir, "reference.fa")
  loci  = [] # array of locus names
  if not os.path.exists(refFn):
    print refFn + " - Not Found"
    rf = open(refFn, "w") # file handle for reference sequence fasta file
    ranges = {}
    refSeqs = {} # list of reference sequences (key = id, value = seq object)
    for (loc, variantsFn, flanksFn) in spc:
      loci.append(loc)
      fs = {} # flanking sequences at this locus (key = id, value = seq object)
      f = open(os.path.join(specDir, flanksFn))
      for r in Bio.SeqIO.parse(f, "fasta"):
        fs[r.id] = r.seq
      f = open(os.path.join(specDir, variantsFn))
      for r in Bio.SeqIO.parse(f, "fasta"):
        s = Bio.Seq.MutableSeq('', Bio.Alphabet.generic_dna) 
        s += fs['up'] # add upstream seq, allele seq, downstream seq
        s += r.seq
        s += fs['down']
        Bio.SeqIO.write([Bio.SeqRecord.SeqRecord(s, id=r.id)], rf, "fasta") # add to reference fasta file
        ranges[r.id] = (len(fs['up']), len(fs['up']) + len(r.seq)) # get range of allele sequence
        refSeqs[r.id] = s # store this reference sequence in list
    rf.close()
    rangesFn = os.path.join(workDir, "ranges.pkl")
    f = open(rangesFn, 'w')
    pickle.dump(ranges, f)
    f.close()
    lociFn = os.path.join(workDir, "loci.pkl")
    f = open(lociFn, 'w')
    pickle.dump(loci, f)
    f.close()
    refSeqsFn = os.path.join(workDir, "refSeqs.pkl")
    f = open(refSeqsFn, 'w')
    pickle.dump(refSeqs, f)
    f.close()

    null = open(os.devnull, 'w')
    p = subprocess.call([bwa, 'index', refFn], stderr=null) # generate index of reference fasta for mapping+
  else:
    print refFn + " - Found"
    for (loc, variantsFn, flanksFn) in spc:
      loci.append(loc)
    
  return loci

def alignReads(workDir, pref, paired, refFn, files, logFile, bwa):
	null = open(os.devnull, 'w')

	sais = []
	n=0
	for fn in files:
		sai = os.path.join(workDir, pref + '-' + str(n) + '.sai')
		n += 1
		#if verbose:
		#	print >> logFile, ' '.join([bwa, 'aln', '-f', sai, '-t', '8', refFn, fn])
		p = subprocess.call([bwa, 'aln', '-f', sai, '-t', '8', refFn, fn], stderr=null)
		sais.append(sai)
	return sais

def bamify(workDir, pref, files, sais, refFn, expand, logFile, insertSize, bwa, samtools):
  null = open(os.devnull, 'w')

  single = False
  if len(files) == 1:
    single = True
    
  out0 = os.path.join(workDir, pref)
  out = os.path.join(workDir, pref + '.bam')
    
  m = re.search('(.*)\\.sai', sais[0])
  sam = m.group(1) + '.sam'
  tmp = m.group(1) + '.tmp' # temporary sam output

  # generate sam from sai
  if expand:
    if single:
      p = subprocess.call([bwa, 'samse', '-n', '99999', '-f', tmp, refFn, sais[0], files[0]], stderr=null)
    else:
      cmd = [bwa, 'sampe']
      if insertSize:
        cmd += ['-a', insertSize]
      cmd += ['-n', '99999', '-f', tmp, refFn, sais[0], sais[1], files[0], files[1]] # write to tmp
      p = subprocess.call(cmd, stderr=null)
    # expand sam
    i = open(tmp)
    o = open(sam, 'w')
    expandSam(i, o)
    i.close()
    o.close()
  else:
    if single:
      p = subprocess.call([bwa, 'samse', '-n', '99999', '-f', sam, refFn, sais[0], files[0]], stderr=null)
    else:
      cmd = [bwa, 'sampe']
      if insertSize:
        cmd += ['-a', insertSize]
      cmd += ['-n', '99999', '-f', sam, refFn, sais[0], sais[1], files[0], files[1]] # write to sam
      p = subprocess.call(cmd, stderr=null)

  # convert to bam 
  #p = subprocess.call([samtools, 'view', '-bS', '-o', tmp, sam], stderr=null)
  # sort bam
  #p = subprocess.call([samtools, 'sort', tmp, out0], stderr=null)
  p1 = subprocess.Popen([samtools, 'view', '-uS', sam], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  p2 = subprocess.Popen([samtools, 'sort', '-', out0], stdin=p1.stdout, stdout=subprocess.PIPE)
  p1.stdout.close()
  p2.communicate()

  return out
  
def pileupReads(workDir, bam, refFn, pileupFile, logFile, samtools):
  null = open(os.devnull, 'w')
  p = subprocess.Popen([samtools, 'index', bam], stdout=subprocess.PIPE, stderr=null)
  p = subprocess.Popen([samtools, 'mpileup', '-f', refFn, bam], stdout=subprocess.PIPE, stderr=null)
  return p.stdout
  #for l in p.stdout:
    #pileupFile.write(l)

def align(workDir, paired, files, logFile, insertSize, bwa, samtools):
	refFn = os.path.join(workDir, "reference.fa")

	sais = alignReads(workDir, 'all', paired, refFn, files, logFile, bwa)

	bam = bamify(workDir, 'all', files, sais, refFn, True, logFile, insertSize, bwa, samtools)

	pileFn =  os.path.join(workDir, 'all.pileup')
	return pileupReads(workDir, bam, refFn, open(pileFn, 'w'), logFile, samtools)

def pile(r, s):
	r = r.lower()
	v = {'a':0, 'c':0, 'g':0, 't':0}
	skip = False
	indel = False
	for c in s:
		if skip:
			skip = False
		elif c == '.' or c == ',':
			v[r] += 1
		elif c == '^':
			skip = True
		elif c == '$':
			pass
		elif c == '+' or c == '-':
			pass
		elif "0123456789".count(c) > 0:
			indel = True
		elif indel:
			indel = False
		elif c.lower() in v:
			v[c.lower()] += 1
	return v

def getNovelAllele(variant,locus,ranges,files,workDir,logFile,samtools,bwa,resBase,nameSep,refSeqs,paired,insertSize):
	allele_name = locus + nameSep + variant ###CHECK DELIMITER VARIABLE NAME	
	# get reference sequence = closest match
	typeFn = workDir + "/" + allele_name + ".fa"
	typeFile = open(typeFn, "w")
	s = refSeqs[allele_name]
	Bio.SeqIO.write([Bio.SeqRecord.SeqRecord(s, id=allele_name)], typeFile, "fasta")
	typeFile.close()
	# index it
	null = open(os.devnull, 'w')
	p = subprocess.call([bwa, 'index', typeFn], stderr=null)	
	# align reads to it
	sais = alignReads(workDir, resBase+"."+allele_name, paired, typeFn, files, logFile, bwa)
	bam = bamify(workDir, resBase+"."+allele_name, files, sais, typeFn, False, logFile, insertSize, bwa, samtools)
	bamFn = workDir + "/" + resBase + "." + allele_name + ".bam"
	#p = subprocess.call(['mv',bam,bamFn],stderr=null) # copy to current directory from working dir
	print >> logFile, "Alignments are in " + bamFn
	# generate pileup and fastq
	piFn = workDir + "/" + resBase + "." + allele_name + '.pileup'
	fqFn = workDir + "/" + resBase + "." + allele_name + '.fastq'
	#r = allele_name + ":" + str(ranges[allele_name][0] + 1) + "-" + str(ranges[allele_name][1])
	print >> logFile, 'Writing pileup to ' + piFn
	f = open(piFn, 'w')
	p = subprocess.Popen([samtools, 'index', bamFn], stdout=subprocess.PIPE, stderr=null)
	p = subprocess.Popen([samtools, 'pileup', '-cf', typeFn,bamFn], stdout=subprocess.PIPE, stderr=null)
	for l in p.stdout:
		f.write(l)
	f.close()
	print >> logFile, 'Writing fastq to ' + fqFn
	f = open(fqFn, 'w')
	p = subprocess.Popen([samtools+'.pl', 'pileup2fq', piFn], stdout=subprocess.PIPE, stderr=null)
	for l in p.stdout:
		f.write(l)
	f.close()
	fq = Bio.SeqIO.read(fqFn,"fastq")
	slice = fq[	ranges[allele_name][0] : ranges[allele_name][1] ]
	slice.id = resBase+"."+allele_name
	Bio.SeqIO.write(slice, fqFn, "fastq")
	
def cmpSnpsScore(a, b):
	x = cmp(a[2], b[2])
	if x == 0:
		x = cmp(a[3], b[3])
	return x

def score(files, workDir, database, sig, paired, insertSize, outFile, logFile, locusList, nameSep, verboseFiles, bwa, samtools, pileup):
	# A comparison function to compare
	# elements of the histogram of bases.
	# r is th reference base,
	# a & b are tuples <base,count>
	def compGreater(r, a, b):
		if a[1] == b[1]:
			if a[0] == r:
				return -1
			elif b[0] == r:
				return 1
		return cmp(b[1], a[1])

	# the nominal probability of
	# an error is 1%
	p = 1 - 0.01 / 3.0

	pileFn =  os.path.join(workDir, 'all.pileup')
	rangesFn = os.path.join(workDir, "ranges.pkl")
	lociFn = os.path.join(workDir, "loci.pkl")
	refSeqsFn = os.path.join(workDir, "refSeqs.pkl")

	(_,resName) = os.path.split(files[0])
	(resBase, _) = os.path.splitext(resName)
	if len(files) > 1:
		m = re.search('^(.*)_1', resBase)
		if m is None:
			print >> sys.stderr, "bad filename group:\n" + '\n'.join(files)
			sys.exit(1)
		resBase = m.group(1)

	ranges = pickle.load(open(rangesFn))
	loci = pickle.load(open(lociFn))
	refSeqs = pickle.load(open(refSeqsFn))

	loc = '' # current locus reading through pileup
	pos = 1
	holes = 0
	snps = 0
	snpList = []
	covMin = 99999
	covMax = 0
	covSum = 0
	covSum2 = 0
	scores = []
	resScores = [] # original list
	resAlleleHash = {} # dictionary for oxford style naming
	resAlleleHashClosest = {} # dictionary for oxford style naming, allowing for closest alleles if exact matches aren't found
	resAlleleHashClosestSNP = {}
	results = {} # key = locus base name, value = tuple of results for each variant (locus, var, snps, sc, loc, scores, snpList, covStats)
	fullScoreSet = {} # ditto but for all variants

	for l in pileup: #open(pileFn):
		t = l.split()
		if loc == '':
			loc = t[0] # for first line in pileup, set locus name from first col in pileup
			pos = ranges[loc][0] + 1
		if t[0] != loc:
			# We've hit a new locus variant

			# The sorting of the scores is just
			# to get better numerical stability
			scores.sort()
			scores.reverse()
			
			# calculate score & coverage stats for this locus variant, if no indels
			if len(scores) > 0 and holes == 0:
				sc = scores[0]
				for s in scores[1:]:
					sc = logAdd(sc, s)
				# switch into log10 for sanity.
				sc = sc / log(10.0)
				m = re.search('([^'+nameSep+']+)'+nameSep+'?([0-9]+)', loc)
				locus = m.group(1) # base name for locus allele names, should match header in ST file
				if locus not in locusList:
					print "Locus " + locus + " from sequence file not recognised in ST file."
				var = m.group(2) # allele number
				n = float(len(scores))
				stdDev = sqrt(covSum2 / n - (covSum / n) * (covSum / n))
				covStats = (covMin, covMax, int(100 * covSum / n) / 100.0, int(100.0 * stdDev) / 100.0)
				res = (locus, var, snps, sc, loc, scores, snpList, covStats)
				if locus not in results:
					results[locus] = []
				results[locus].append(res)
			
			# reset locus vars
			loc = t[0] # update locus name
			pos = ranges[loc][0] + 1 # get position
			holes = 0
			snps = 0
			snpList = []
			scores = []
			covMin = 99999
			covMax = 0
			covSum = 0
			covSum2 = 0
		
		# score this base
		here = int(t[1])
		if here - 1 < ranges[loc][0]:
			continue
		elif here - 1 >= ranges[loc][1]:
			continue
		while pos < here:
			holes += 1
			pos += 1
		v = pile(t[2], t[4])
		x = v.items()
		x.sort(lambda a,b: compGreater(t[2], a, b))
		if x[0][0] != t[2].lower():
			snps += 1
			snpList.append((pos,t[2],v));
		c = x[0][1]
		covSum += c
		covSum2 += c * c
		if c > covMax:
			covMax = c
		if c < covMin:
			covMin = c
		n = int(t[3])
		for (_,j) in x[1:]:
			scores.append(logBinGe(p, n, j))
		pos = here + 1

	# finished reading through pileup, collate results
	resVec = []
	scoreVec = []
	statVec = []
	refNames = []
	snpsVec = []
	
	# delete pileup
	os.remove(pileFn)

	for locus in loci:
		if locus not in results:
			resVec.append('-') # didn't generate a result above
			scoreVec.append(locus + ':-:0:0') # didn't generate a result above
			resAlleleHash[locus] = '-'
			resAlleleHashClosest[locus] = '-'
			resScores.append(0) # set score to zero
			statVec.append("0/0/0/0") # set to zero
			continue
			
		vec = results[locus] # (locus, var, snps, sc, loc, scores, snpList, covStats)
		vec.sort(cmpSnpsScore)

		# Compute the number of significant, zero-SNP matches
		exact = filter(lambda itm: itm[2] == 0 and itm[3] < sig, vec) #(locus, var, snps, sc, loc, scores, snpList, covStats)
		
		if len(exact) > 0:
			resVec.append(str(exact[0][1])) # variant number of best match
			resAlleleHash[locus] = str(exact[0][1]) # variant number, indexed by locus
			resAlleleHashClosest[locus] = str(exact[0][1]) # variant number, indexed by locus
			resAlleleHashClosestSNP[locus] = 0 # number of SNPs
			scoreVec.append(locus + ':' + str(exact[0][1]) + ':' + str(round(exact[0][3]*(-1),1)) + ':1') # variant number / score 
			statVec.append('/'.join([str(i) for i in exact[0][7]])) # coverage stats
			refNames.append(exact[0][4]) # full locus id
			resScores.append(round(exact[0][3]*(-1),1)) # score
			
		if len(exact) > 1:
			print >> logFile, "locus " + exact[0][0] + " has multiple significant matches:"
			print >> logFile, "\tVariant\tScore"
			for e in exact:
				print >> logFile, "\t" + e[1] + "\t" +	str(round(e[3]*(-1),1))
			print >> logFile

		if len(exact) == 0:
			print >> logFile, "locus " + locus + " has no significant matches."
			print >> logFile
			resVec.append('-')
			resAlleleHash[locus] = '-' # for oxford style
			resAlleleHashClosest[locus] = str(vec[0][1])
			resAlleleHashClosestSNP[locus] = str(vec[0][2])
			#getNovelAllele(str(vec[0][1]),locus,ranges,files,workDir,logFile,samtools,bwa,resBase,nameSep,refSeqs,paired,insertSize) # generate sam, pileup, fastq for novel allele by mapping to closest match
			scoreVec.append(locus + ':' + str(vec[0][1]) + ':' + str(round(vec[0][3]*(-1),1)) + ':' + str(vec[0][2])) # flag uncertain results
			statVec.append('*'+'/'.join([str(i) for i in vec[0][7]])) # flag stats for uncertain results
			snpsVec.append((locus, vec[0][6]))
			refNames.append('-')
			resScores.append(0) # set score to zero
	
	# get ST
	
	locusVariants = [] # exact matches only
	for locus in locusList:
		locusVariants.append(resAlleleHash[locus])
	locusVariantsConcat = ' '.join(locusVariants)

	st = '' # closest ST
	closestLocusVariants = []
	
	if locusVariantsConcat in database:
		st = str(database[locusVariantsConcat]) # ST called OK
		
	elif '-' in locusVariants:
		uncertainLoci = locusVariants.count('-') # some loci not certain, try to infer closest ST
		locusVariantsClosest = [] # closest matches
		for locus in locusList:
			locusVariantsClosest.append(resAlleleHashClosest[locus])
		locusVariantsClosestConcat = ' '.join(locusVariantsClosest)
		if locusVariantsClosestConcat in database:
			st = '*' + str(database[locusVariantsClosestConcat])  + '/' + str(uncertainLoci) # closest allele and number of divergent loci
		else:
			st = 'NA' # can't detect close sequence
	
	else:
		st = max(database.values()) + 1 # novel combination of known alleles
		print >> logFile, 'introducing novel sequence type: ' + str(st)
		database[locusVariantsConcat] = st # store this combination as new ST
		st = "NOVEL-"+str(st) # for printing

	## output results
	
	# simple output, alleles (/snp if present)
	locVar = []
	for locus in resAlleleHashClosest:
		if locus in resAlleleHashClosestSNP:
			if resAlleleHashClosestSNP[locus] > 0:
				locVar.append(resAlleleHashClosest[locus] + "/" + resAlleleHashClosestSNP[locus])
			else:
				locVar.append(resAlleleHashClosest[locus])
		else:
			locVar.append(resAlleleHashClosest[locus])
			
	#print >> outFile, '\t'.join([resBase, st] + locVar), 
	#if (resScores):
	#	print >> outFile, '\t' + str(min(resScores)),
	#print >> outFile, '\n',

	# log file: annotated ST and alleles
	print >> logFile, '\t'.join(['Data', 'ST'] + loci + ['overall']) 
	print >> logFile, '\t'.join([resBase, st] + scoreVec),
	if (resScores):
		print >> logFile, '\t' + str(min(resScores)),
	print >> logFile
	print >> logFile
	print >> logFile, '\t'.join(['Stats', 'Min/Max/Avg/StdDev'])
	print >> logFile, '\t'.join([resBase] + statVec)
	print >> logFile
		
	# log file: evidence for SNPs in best match
	if len(snpsVec) > 0:
		print >> logFile, 'SNPs:'
		for (loc,snps) in snpsVec:
			for (pos, ref, dist) in snps:
				ds = 'A(' + str(dist['a']) + ') C(' + str(dist['c']) + ') G(' + str(dist['g']) + ') T(' + str(dist['t']) + ')'
				print >> logFile, loc + '/' + str(pos) + ' ' + ref + ' ' + ds
				print >> logFile

	if (resScores):
		minscore = str(min(resScores))
	else:
		minscore = 'NA'
		
	# return string for printing at end of log file
	print >> outFile, '\t'.join(['FinalResults', resBase, str(st)] + scoreVec + [minscore])
	return ('\t'.join(['Dataset', 'ST'] + loci + ['overall']), '\t'.join(['FinalResults', resBase, str(st)] + scoreVec + [minscore]))

def usage():
	print >> sys.stderr, 'usage: srst [options] filename...'
	print >> sys.stderr, '	options:'
	print >> sys.stderr, '		-h	    | --help				    print this help message'
	print >> sys.stderr, '		-S file | --summaryFile=file		summary file pointing to allele sequences & flanks for each locus (tab-delim)'
	print >> sys.stderr, '		-d file | --database=file			filename for the database of known sequence types (tab-delim)'
	print >> sys.stderr, '		-i int	| --insert=int				insert size for paired data'
	print >> sys.stderr, '		-w dir	| --working-directory=dir	provide the directory into which all the working files should be placed'
	print >> sys.stderr, '		-l file | --log=file				filename to write detailed logging information (defaults to stderr)'
	print >> sys.stderr, '		-n file | --name-sep				separator for allele names (either - (default) or _)'
	print >> sys.stderr, '		-V      | --verboseFiles			generate/store all output files'
	print >> sys.stderr, '		-s int	| --significance=int		cutoff score (default 10)'
	print >> sys.stderr, '		-b path	| --bwa=path				path to bwa command'
	print >> sys.stderr, '		-t path	| --samtools=path			path to samtools command'


def main():

	options = 'hS:d:i:w:l:n:Vs:b:t:'
	long_options = ['help', 'summaryFile=', 'database=', 'insert=', 'working-directory=',
				'log==', 'name-sep=', 'verboseFiles', 'significance=', 'bwa=', 'samtools=']

	try:
		(opts,args) = getopt.getopt(sys.argv[1:],  options, long_options)
	except getopt.GetoptError, err:
		print >> sys.stderr, str(err)
		usage()
		sys.exit(1)

	# defaults
	workingDir = 'tmp'
	summaryFileName = None
	verbose = True
	insertSize = None
	log = sys.stderr
	out = sys.stdout
	outFn = 'out'
	databaseFn = None
	significance = -10 ## CUTOFF
	nameSep = "-"
	verboseFiles = False
	bwa = 'bwa'
	samtools = 'samtools'

	for o, a in opts:
		if o in ('-h', '--help'):
			usage()
			sys.exit(0)
		elif o in ('-v', '--verbose'):
			verbose = True
		elif o in ('-V', '--verboseFiles'):
			verboseFiles = True
		elif o in ('-S', '--summaryFile'):
			summaryFileName = a
		elif o in ('-i', '--insert'):
			insertSize = a
		elif o in ('-w', '--working-directory'):
			workingDir = a
		elif o in ('-d', '--database'):
			databaseFn = a
		elif o in ('-n', '--name-sep'):
			nameSep = a
		elif o in ('-l', '--log'):
			log = open(a, 'w')
		elif o in ('-s', '--significance'):
			significance = (-1) * float(a)
		elif o in ('-b', '--bwa'):
			bwa = a
		elif o in ('-t', '--samtools'):
			samtools = a
		else:
			assert False, 'unhandled option: ' + o

	if summaryFileName:
		lociHeader = prepare(summaryFileName, workingDir, bwa)
	else:
		usage()
		print >> sys.stderr, '-S,--summary must be supplied.'
		sys.exit(1)

	# read in ST assignment
	database = None
	locusList = []
	if databaseFn is not None:
		for l in open(databaseFn):
			if database is None:
				database = {}
				locusList = l.split()[1:]
				continue
			t = l.split()
			st = int(t[0])
			v = ' '.join([s for s in t[1:]])
			if v in database:
				print >> sys.stderr, 'sequence type ' + str(st) + ' is a duplicate of ' + str(database[v])
			database[v] = st

	# header for output file
	print "Data\tST\t" + "\t".join(lociHeader) + "\tscore"

	scores = []
	
	fileSets = {} # key = id, value = list of files (2 for paired, defined as _1 and _2; 1 for single)
	for fastq in args:
		(fqDir,fqFileName) = os.path.split(fastq)
		(fqBaseName,fqExt) = os.path.splitext(fqFileName)
		if fqExt == ".gz":
			(fqBaseName,fqExt) = os.path.splitext(fqBaseName)
		m=re.match("(.*)(\_[12])",fqBaseName)
		if m:
			(b,i) = (m.groups(1)[0],"p") # part of a pair
		else:
			(b,i) = (fqBaseName,"s") # single file
		index = b+"_"+i
		if index in fileSets:
			fileSets[index].append(fastq) # if we have seen its pair before
		else:
			fileSets[index] = [fastq]
			
	for base in fileSets:
		files = fileSets[base] # list
		if len(files) == 2:
			paired = True
			pair = files
			
			print >> log, '-' * 70
			print >> log, pair[0]
			print >> log, pair[1]
			print >> log
			if os.path.exists(pair[0]) and os.path.exists(pair[1]):
				pileup = align(workingDir, paired, pair, sys.stderr, insertSize, bwa, samtools)
				s = score(pair, workingDir, database, significance, paired, insertSize, out, log, locusList, nameSep, verboseFiles, bwa, samtools, pileup)
				scores.append(s)
				out.flush()
				log.flush()
			else:
				print >> log, "input file/s not found:" + pair[0] + "," + pair[1]

		elif len(files) == 1:
			paired = False
			print >> log, '-' * 70
			print >> log, files[0]
			print >> log
			if os.path.exists(files[0]):
				pileup = align(workingDir, paired, [files[0]], sys.stderr, insertSize, bwa, samtools)
				s = score([files[0]], workingDir, database, significance, paired, insertSize, out, log, locusList, nameSep, verboseFiles, bwa, samtools, pileup)
				scores.append(s)
				out.flush()
				log.flush()
			else:
				print >> log, "input file not found:" + files[0]
				
		else:
			print >> log, "not sure how to handle file list:" + files

	# print final results summary to log file
	if len(scores) > 0:
		print >> log
		print >> log, '=' * 70
		print >> log
		print >> log, "Results Summary"
		print >> log
		print >> log, scores[0][0]
		for (_,s) in scores:
			print >> log, s
			
	# remove temporary files
	#if not verboseFiles:
	#for f in os.listdir(workingDir):
	#	os.remove(os.path.join(workingDir,f))
	#os.rmdir(workingDir)

if __name__ == '__main__':
	main()
