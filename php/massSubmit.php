<?php

    $error    = 0;
    $errorMsg = '';
    $uid      = 0;
    $user     = 'public';

    // Connect to DB
    try {
        $DBH = new PDO("mysql:host=localhost;dbname=rpetit_staphopia", 'rpetit', 'rpetit');
    }
    catch (PDOException $e) {
        echo "Unable to connect to db.";
        die();
    }
    
    $list = array (
                'BCM' => array( 
                    'name' => 'Baylor College of Medicine', 
                    'link' => 'http://www.hgsc.bcm.tmc.edu' 
                ),
                'BI' => array( 
                    'name' => 'Broad Institute', 
                    'link' => 'http://www.broadinstitute.org' 
                ),
                'Emory University' => array( 
                    'name' => 'Emory University', 
                    'link' => 'http://med.emory.edu/main/research/core_labs/gra_genome_center/index.html' 
                ),
                'FG13' => array( 
                    'name' => 'FG13', 
                    'link' => '' 
                ),
                'Geneva University Hospitals' => array( 
                    'name' => 'Geneva University Hospitals', 
                    'link' => 'http://www.unige.ch/international/index_en.html' 
                ),
                'ICL-CFB' => array( 
                    'name' => 'Imperial College London - Centre for Bioinformatics', 
                    'link' => 'http://www.bioinformatics.imperial.ac.uk/'     
                ),
                'ILLUMINA' => array( 
                    'name' => 'Illumina Cambridge Ltd.', 
                    'link' => 'http://www.illumina.com' 
                ),
                'JCVI' => array( 
                    'name' => 'JCVI', 
                    'link' => 'http://www.jcvi.org' 
                ),
                'NCBS-TIFR' => array( 
                    'name' => 'National Center for Biological Sciences-TIFR', 
                    'link' => 'http://www.ncbs.res.in' 
                ),
                'NCGR' => array( 
                    'name' => 'National Center for Genome Resources', 
                    'link' => 'http://www.ncgr.org' 
                ),
                'NIAID-RML-RTS' => array( 
                    'name' => 'NIAID Rocky Mountain Laboratories', 
                    'link' => 'http://www.niaid.nih.gov/about/organization/dir/rml/Pages/default.aspx' 
                ),
                'SC' => array( 
                    'name' => 'Wellcome Trust Sanger Institute', 
                    'link' => 'http://www.sanger.ac.uk' 
                ),
                'Statens Serum Institut' => array( 
                    'name' => 'Statens Serum Institut', 
                    'link' => 'http://www.ssi.dk/English.aspx' 
                ),
                'STSI' => array( 
                    'name' => 'Scripps Translational Science Institute', 
                    'link' => 'http://www.stsiweb.org/index.php' 
                ),
                'TGEN' => array( 
                    'name' => 'Translational Genomics Research Institute', 
                    'link' => 'https://www.tgen.org' 
                ),
                'The Roslin Institute' => array( 
                    'name' => 'The Roslin Institute', 
                    'link' => 'http://www.roslin.ed.ac.uk' 
                ),
                'UMIGS' => array( 
                    'name' => 'University of Maryland IGS', 
                    'link' => 'http://www.igs.umaryland.edu' 
                ),
                'University of Edinburgh' => array( 
                    'name' => 'University of Edinburgh', 
                    'link' => 'http://genepool.bio.ed.ac.uk' 
                ),
                'University of Melbourne' => array( 
                    'name' => 'University of Melbourne', 
                    'link' => 'http://agd.path.unimelb.edu.au' 
                ),
                'WUGSC' => array( 
                    'name' => 'Washington University GSC', 
                    'link' => 'http://genome.wustl.edu' 
                )
            );

  
    $handle = fopen("/data1/home/rpetit/staphopia-pipeline/php/SRA.txt", "r");
    $i = 1;
    $j = 1;
    if ($handle) {
        while (($buffer = fgets($handle, 4096)) !== false) {
            $cols = preg_split("/\t/", $buffer);

            /*
             * Columns:
             *  0 - Study Accession          6 - Sample ID
             *  1 - Study Title              7 - Sample Name
             *  2 - Experiment Accession     8 - Library Method
             *  3 - Experiment ID            9 - Sequencer
             *  4 - Experiment Title        10 - Submitter
             *  5 - Sample Accession        11 - Publication Status
             */
            $_POST = array(
                'project_type' => 'Genome', 
                'project_status' => 'Incomplete', 
                'seq_status' => 'Draft', 
                'contact_name' => 'SRA Public Experiment', 
                'contact_email' => 'public@staphopia.com', 
                'ncbi_id' => $cols[6], 
                'seq_status_link' => "http://www.ncbi.nlm.nih.gov/sra?term=$cols[2]",
                'comments' => "SRA entry $cols[2] titled '$cols[4]' sequenced using $cols[9].",
                'seq_center' => $list[$cols[10]]['name'], 
                'seq_center_link' => $list[$cols[10]]['link'], 
                'public' => 1,
                'sample' => $cols[5],
                'instrument' => $cols[9]
            );

            if (!empty($cols[6])) {
                $_POST['strain'] = $cols[6]; 
            }
            
            if (!empty($cols[8])) {
                $_POST['library_method'] = $cols[8]; 
            }

            $publication = preg_replace("/[\n\r]/","",$cols[11]);
            $published = 0;
            
            if ($publication != 'Unpublished') {
                $published = 1;
                
                $ids = preg_replace("/Pubmed=/", "", $publication);
                $pmids = preg_split("/:/", $ids);
            }

            $files = FileCount($cols[5],$cols[2]);
            if (file_exists($files[0])) {
                if (count($files) < 3) {
                    
                    if (count($files) == 1) {
                        list($cmd, $file) = $file = Decompress($files[0]);
                        $paired = "single";
                        $filesize = filesize($files[0]) * 3; // file is compressed using bzip, use conservative estimate of uncompressed file
                    } else {
                        list($bz2a, $fq1) = Decompress($files[0]);
                        list($bz2b, $fq2) = Decompress($files[1]);
                        list($cat, $file) = Concatenate($fq1, $fq2, $cols[2]);
                        $cmd = $bz2a . $bz2b . $cat;
                        $paired = "paired";
                        
                        $filesize = (filesize($files[0]) + filesize($files[0])) * 3; // file is compressed using bzip, use conservative estimate of uncompressed file
                    }
                    
                    if ($filesize/1048576 > 90) {
                        print $i++ ."\t". $cols[2] ." (Sample: ". $cols[5] .") ($paired)\n";
                        //if ($i == 10) {
                        //    die();
                        //}
                        // Create seq name 
                        $seqname = MakeSeqName($uid,$user,$DBH);
          
                        // seq
                            // Create/Insert query
                            $insert = $DBH->prepare("INSERT INTO `seq` (seqname, seqhash, public, published) VALUES (?,?,?,?)");
                            $insert->execute(array($seqname, md5($seqname), 1, $published));

                        // seqUser
                            $sid    = GetSeqID($DBH, $seqname);
                            $insert = $DBH->prepare("INSERT INTO `seqUser` (sid,uid) VALUES (?,?)");
                            $insert->execute(array($sid, $uid));
                
                        // seqInfo
                            // Get seqInfoList id's
                            $seqInfoList = SeqInfoList($DBH);
                  
                            // Insert info into seqInfo
                            $insert = $DBH->prepare("INSERT INTO `seqInfo` (sid, silid, value) VALUES (?,?,?)");
                            foreach ($_POST as $k => $v) {
                                if (!empty($v)) {
                                    $insert->execute(array($sid, $seqInfoList[$k], $v));
                                }
                            }
                            
                        // seqPublication
                            if (isset($pmids)) {
                                $insert = $DBH->prepare("INSERT INTO `seqPublication` (id, pmid) VALUES (?,?)");
                                foreach ($pmids as $pmid) {
                                    $insert->execute(array($sid, $pmid));
                                }
                            } 
                
                        // Submit job to queue system
                            // Move the tmp file
                            $ROOT = "/data1/home/rpetit/staphopia-pipeline/output/data";
                            `mkdir -p $ROOT/$user/$seqname`;
                            
                            // create queue script
                            Template("$ROOT/$user/$seqname", $file, $sid, $seqname, $cmd);
                            
                            // queue job
                            $job    = "cd $ROOT/$user/$seqname && qsub $seqname.sh";
                            $output = `$job`;
                    } else {
                        print $j++ ."\t". $cols[2] ." (Sample: ". $cols[5] .") is less then 90mb, skipping.\n";
                        $rm = `rm $file`;
                    }
                } else {
                    print $j++ ."\t". $cols[2] ." (Sample: ". $cols[5] .") has 3 fastq files, skipping.\n";
                }
            } else {
                print $j++ ."\t". $cols[2] ." (Sample: ". $cols[5] .") does not exist, skipping.\n";
            }
        }
        fclose($handle);
    }  
    
    function FileCount($s,$e) {
        $cmd = "ls /data1/home/rpetit/readgp/StaphSRA/Samples/$s/$e*.fastq.bz2 2>/dev/null";
        $result = `$cmd`;
        $result = preg_replace("/\n$/", "", $result);
        
        return preg_split("/\n/", $result);
    }
    
    function Decompress($f) {
        $cmd  = "cp $f /data1/home/rpetit/staphopia-pipeline/output/tmp\n";
        $cmd .= "bunzip2 /data1/home/rpetit/staphopia-pipeline/output/tmp/" . basename($f) . "\n";
    
        return array($cmd, "/data1/home/rpetit/staphopia-pipeline/output/tmp/" . basename($f, ".bz2"));
    }
    
    function Concatenate($f1, $f2, $e) {
        $cmd  = "cat $f1 $f2 > /data1/home/rpetit/staphopia-pipeline/output/tmp/$e.fastq\n";
        $cmd .= "rm $f1\n";
        $cmd .= "rm $f2\n";
        
        return array($cmd, "/data1/home/rpetit/staphopia-pipeline/output/tmp/$e.fastq");
    }
  
    function SeqInfoList($DBH) {
        $seqInfoList = array();
        try {
            $sth = $DBH->query("SELECT id,tag FROM seqInfoList");
            $sth->execute();
            $rows = $sth->fetchAll(PDO::FETCH_ASSOC);

            foreach ($rows as $row) {
                $seqInfoList[$row['tag']] = $row['id'];
            }
        } catch (PDOException $e) {
            return 0;
        }

        return $seqInfoList;
    }

    function MakeSeqName($uid,$user,$DBH) {
        $seqname = '';
        try {
            $row = $DBH->query("SELECT COUNT(uid) AS n FROM seqUser WHERE uid=$uid")->fetch();
            $seqname = $user . "_" . sprintf("%05s", $row['n'] + 1);
        } catch (PDOException $e) {
            return 0;
        }
        return $seqname;
    }
  
    function GetSeqID ($DBH, $seqname) {
        try {
            $row = $DBH->query("SELECT id FROM seq WHERE seqname=". $DBH->quote($seqname) ."")->fetch();
        } catch (PDOException $e) {
            return 'error';
        }

        return $row['id'];
    }

  
    function Template($outdir, $input, $sid, $seqname, $cmd) {
        $template = fopen("$outdir/$seqname.sh", 'w');
        fwrite($template, "#!/bin/bash\n");
        fwrite($template, "### Change to the current working directory:\n");
        fwrite($template, "#$ -cwd\n");
        fwrite($template, "### Job name:\n");
        fwrite($template, "#$ -N j$sid\n");
        fwrite($template, "#$ -S /bin/bash\n");
        fwrite($template, "#$ -pe orte 4\n");
        fwrite($template, "#$ -V\n");
        fwrite($template, "MY_HOST=`hostname`\n");
        fwrite($template, "MY_DATE=`date`\n");
        fwrite($template, "echo \"Running on \$MY_HOST at \$MY_DATE\"\n");
        fwrite($template, "echo \"Running environment:\"\n");
        fwrite($template, "export PATH=/data1/home/rpetit/bin/MUMmer:\$PATH\n");
        fwrite($template, "export PYTHONPATH=/data1/home/rpetit/staphopia-pipeline/lib/python/:/data1/home/rpetit/lib/python/:\$PYTHONPATH\n");
        fwrite($template, "export PYTHON_EGG_CACHE=/tmp/.python-eggs:\$PYTHON_EGG_CACHE\n");
        fwrite($template, "export MPLCONFIGDIR=/tmp/.matplotlib:\$MPLCONFIGDIR\n");
        fwrite($template, "export OMP_NUM_THREADS=1\n");
        fwrite($template, "export OMP_THREAD_LIMIT=2\n");
        fwrite($template, "env\n");
        fwrite($template, "echo \"================================================================\"\n");
        fwrite($template, "# Get Start Time\n");
        fwrite($template, "T1=$(date +%s.%N)\n\n");
        fwrite($template, "# Command To Run Goes Here\n");
        fwrite($template, "\n$cmd\n");
        fwrite($template, "echo \"python /data1/home/rpetit/staphopia-pipeline/bin/Staphopia.py --outdir $outdir --input $input --id $sid --name $seqname\" \n");
        fwrite($template, "python /data1/home/rpetit/staphopia-pipeline/bin/Staphopia.py --outdir $outdir --input $input --id $sid --name $seqname \n");
        fwrite($template, "rm $input\n");
        fwrite($template, "\n");
        fwrite($template, "# Get End Time\n");
        fwrite($template, "T2=$(date +%s.%N)\n");
        fwrite($template, "\n");
        fwrite($template, "# Convert Seconds To Days:Hours:Minutes:Seconds  \n");     
        fwrite($template, "RUNTIME=$(echo \"\$T2 - \$T1\"|bc )\n");
        fwrite($template, "TIME=$(echo \"\$RUNTIME\" | awk '{split($1,a,\".\"); if (a[1]==\"\"){print \"0\";}else{print a[1];} }')\n");
        fwrite($template, "FRAC=$(echo \"\$RUNTIME\" | awk '{split($1,a,\".\"); print \".\"a[2];}')\n");
        fwrite($template, "DAYS=$(((\$TIME/86400)))\n");
        fwrite($template, "HOURS=$(((\$TIME/3600)-(\$DAYS*24)))\n");
        fwrite($template, "MINS=$(((\$TIME/60)-(\$DAYS*1440)-(\$HOURS*60)))\n");
        fwrite($template, "SECS=$(((\$TIME)-(\$DAYS*86400)-(\$HOURS*3600)-(\$MINS*60)))\n");
        fwrite($template, "\n");
        fwrite($template, "# Print Start, End and Run Times\n");
        fwrite($template, "echo \"================================================================\"\n");
        fwrite($template, "echo \"Start time: \$T1\"\n");
        fwrite($template, "echo \"Stop time:  \$T2\"\n");
        fwrite($template, "printf \"Run Time (seconds): %.5F\\n\" \$RUNTIME\n");
        fwrite($template, "printf \"Run Time (d:h:m:s): \$DAYS:\$HOURS:\$MINS:%.5F\\n\" \$SECS\$FRAC\n");
        fclose($template);
    }

?>
