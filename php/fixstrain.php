<?php
  
  // Import DB credentials
  include("/var/www/staphopia/php/db.php");
  
  // Connect to DB
  try {
    $DBH = new PDO("mysql:host=$dbHost;dbname=$dbName", $dbUser, $dbPass);
  }
  catch (PDOException $e) {
    CheckForErrors(1, "Unable to reach the database, please try again later.");
  }
  
  $handle = fopen("/projects/robert/staphopia/php/fix.txt", "r");

  if ($handle) {
    while (($buffer = fgets($handle, 4096)) !== false) {
      $buffer = preg_replace("/\n/", "", $buffer);
      $buffer = preg_replace("/\r/", "", $buffer);
      $cols = preg_split("/\t/", $buffer);
  
      // Make changes to the database          
	// Insert info into seqInfo
	$insert = $DBH->prepare("INSERT INTO `seqInfo` (sid, silid, value) VALUES (?,?,?)");
	$insert->execute(array($cols[0], 1, $cols[1]));

	if (!empty($cols[2])) {
	  $insert->execute(array($cols[0], 3, $cols[2]));
	}
    }
    fclose($handle);
  }  


?>