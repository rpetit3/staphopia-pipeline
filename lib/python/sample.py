#! /usr/bin/python
"""
    Author: Robert A. Petit III
    Date: 4/30/2013
    Version: v0.1

    Staphopia's sample related commands.
  
    Change Log:
        v0.1
            - Initial set up.
"""
from common import Shell
from common import RegEx
from database import Database

class Sample(object):
    def __init__(self, job_id, fastq):
        #self.__sh = Shell(log, args)
        self.__db = Database()

class NewSample(object):
    
    def __init__(self, sh):
        super(NewSample, self).__init__()
        self.__sh = sh
        self.__db = Database()
        
    def rename(self, old, new):
        self.__sh.run_command('mv', ['mv', old, new])
        return new
        
    def make_sample_directory(self, folder):
        self.__sh.run_command('mkdir', ['mkdir', '-p', folder])
        
    def init_status(self, jobid):
        return self.__db.query('update', """UPDATE Job 
                                            SET Begun=CURRENT_TIMESTAMP, Valid=%s
                                            WHERE JobID=%s""", ['2', jobid])
        
    def update_status(self, column, status, jobid):
        return self.__db.query('update', """UPDATE Job 
                                            SET """+ column +"""=%s 
                                            WHERE JobID=%s""", [status, jobid])
        
    def job_info(self, jobid):
        return self.__db.query('select', """SELECT u.UserName, u.UserID, j.Post, j.MD5sum, u.UserTag
                                            FROM `Job` AS j
                                            LEFT JOIN User AS u 
                                            ON j.UserID = u.UserID 
                                            WHERE j.JobID=%s
                                            LIMIT 1""", [jobid])
                                            
    def sample_tag(self, user_id, user_tag):
        # How many samples does the user have
        rows = self.__db.query('select', """SELECT count(UserID) AS i
                                            FROM Sample
                                            WHERE UserID = %s
                                            LIMIT 1""", [user_id])
        if rows[0]:
            return user_tag +'_%.6d' % (rows[0]['i']+1)
        else:
            return False
            
    def new_sample(self, user_id, sample_tag, md5sum):
        rows = self.__db.query('insert', """INSERT INTO Sample (UserID, SampleTag, MD5sum)
                                            VALUES (%s, %s, %s)""", [user_id, sample_tag, md5sum])
        if rows:
            return rows
        else:
            return False
            
    def sample_information(self, sample_id, post):
        tags = self.__get_tags()
        for t, v in post.iteritems():
            tag = str(t)
            value = str(v)
            if tag not in tags:
                tags[ tag ] = self.__insert_tag(tag)
                
            if tags[ tag ] and len(value) > 0:
                rows = self.__db.query('insert', """INSERT INTO Information (SampleID, TagID, Value)
                                                    VALUES (%s, %s, %s)""", [sample_id, tags[ tag ], value])
                if rows != 1:
                    print 'Failed to insert into Information: (%s, %s, %s)' % (sample_id, tags[ tag ], value)
        
    def __get_tags(self):
        tags = {}
        rows = self.__db.query('select', """SELECT TagID, Tag FROM Tag""", [])
        if rows[0]['TagID']:
            for i in xrange(len(rows)):
                tags[ rows[i]['Tag'] ] = rows[i]['TagID']
            return tags
        else:
            return False
            
    def __insert_tag(self, tag):
        rows = self.__db.query('insert', """INSERT INTO Tag (Tag) VALUES (%s)""", [tag])
        if rows:
            return rows
        else:
            print 'There was an error updated the Tags table.'
            return False
        