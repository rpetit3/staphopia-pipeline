#! /usr/bin/python
"""
     Author: Robert A Petit III
       Date: 4/26/2013
    Version: v0.1
    
    Staphopia's FASTQ validation step.  A modified version of FastQValidator is used to test
    the validity of an input FASTQ file.  FastQvalidator has been modified to only look at 
    the first 1000 reads.
    
    FastQValidator - http://genome.sph.umich.edu/wiki/FastQValidator

    Change Log:
        v0.1
            - Initial set up
"""
from common import Shell
from common import RegEx

class ValidateFastQ(object):

    def __init__(self, log, args):
        self.sh = Shell(log, args)
        
    def decompress(self, out, archive):
        self.sh.run_command('7zip', ['7z', '-y', 'x', archive])
        
        files = []
        stdout = self.sh.stdout.split('\n')
        for line in stdout:
            m = RegEx( line )
            if m.match(r'^Extracting  (.*)$'):
                files.append(m.group(1))
                
        if len(files) > 1:
            self.sh.log('Error: More then one file contained within %s.\n' % archive)
        elif len(files) == 1:
            self.sh.log('Found %s, begin validation of FASTQ format.\n' % files[0])
            self.fastq = files[0]
        else:
            self.sh.log('Error: %s is empty, or not an archive at all.\n' % archive)
        
        return 1

    def test(self):
        self.sh.run_command('fastQValidator', ['fastQValidator', '--file', self.fastq, '--minReadLen', '1', 
                                               '--disableSeqIDCheck'])
        
        self.is_fastq = False
        stdout = self.sh.stdout.split('\n')
        for line in stdout:
            m = RegEx( line )
            if m.match(r'^Returning(.*)FASTQ_SUCCESS$'):
                self.is_fastq = True
                
        return 1       

    def new_sample(self):
        self.sh = NewSample(self.__sh, )