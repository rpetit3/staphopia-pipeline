#! /usr/bin/python
"""
     Author: Robert A. Petit III
       Date: 5/15/2013
    Version: v0.4

    Staphopia's MLST algorithm uses two methods of determining a ST and CC.  The first method blasts the loci against the 
    assembled contigs, and retrieves the matches.  Locus variants are only assigned if a perfect match it found. Reads are
    also mapped against known loci and flanking regions.  These mapped reads are then extracted and used as an input for 
    SRST. Locus hits which have a score greater then 10, and only one hit can be asigned a locus variant id.  If all seven
    loci have an assigned id, then a ST is called.  If a there are five or more loci with an assigned id, then the clonal
    complex can be called.

    SRST - http://sourceforge.net/projects/srst/
  
    Change Log:
        v0.4
            - Working with new database schema
        v0.3
            - Refactored code
            - Modified to work with new naming schema in Common.py
            - Fixed spacing (all tabs replaced by 4 spaces)
        v0.2
            - Replaced range with xrange, (for loops)
            - Corrected bug, where ST called without 7 matches (was calling ST with >= 5 matches)
        v0.1
            - Initial set up.
"""
import os
from common import Shell
from database import Database

class SequenceType(object):
    
    def __init__(self, log, args, fastq, assembly, out, mean, blastdb):
        self.VERSION = "0.4"
        self.__fastq = fastq
        self.__assembly = assembly
        self.__sample_tag = os.path.splitext(os.path.basename(fastq))[0].replace('_filtered', '')
        self.__out = out + '/MLST'
        self.__root = out
        self.__fastx = self.__out + '/'+ self.__sample_tag +'.fastq'
        self.__mean = int(mean)
        self.__blastdb = blastdb
        self.__sh = Shell(log, args)
        self.__db = Database()
        self.__sh.run_command('mkdir', ['mkdir', '-p', self.__out])
        
        self.__st = '0'
        self.__cc = '0'
        self.__alleles = {'arcc':{'id':0, 'assScore':999, 'assCov':0, 'assIdent':0, 'assCount':0, 'mapScore':0, 'mapCount':0, 'status':"none"}, 
                          'aroe':{'id':0, 'assScore':999, 'assCov':0, 'assIdent':0, 'assCount':0, 'mapScore':0, 'mapCount':0, 'status':"none"}, 
                          'glpf':{'id':0, 'assScore':999, 'assCov':0, 'assIdent':0, 'assCount':0, 'mapScore':0, 'mapCount':0, 'status':"none"}, 
                          'gmk_':{'id':0, 'assScore':999, 'assCov':0, 'assIdent':0, 'assCount':0, 'mapScore':0, 'mapCount':0, 'status':"none"}, 
                          'pta_':{'id':0, 'assScore':999, 'assCov':0, 'assIdent':0, 'assCount':0, 'mapScore':0, 'mapCount':0, 'status':"none"}, 
                          'tpi_':{'id':0, 'assScore':999, 'assCov':0, 'assIdent':0, 'assCount':0, 'mapScore':0, 'mapCount':0, 'status':"none"}, 
                          'yqil':{'id':0, 'assScore':999, 'assCov':0, 'assIdent':0, 'assCount':0, 'mapScore':0, 'mapCount':0, 'status':"none"}}
        self.__loci = ['arcc','aroe','glpf', 'gmk_', 'pta_', 'tpi_', 'yqil']   
        self.__Assembly = False
        self.__Mapping = False
        self.__Compared = False
        self.__aTime = ""
        self.__mTime = ""
        
        self.mlst = {
            'index':'/staphopia-ebs/staphopia-pipeline/index/MLST/MLST',
            'summary':'/staphopia-ebs/staphopia-pipeline/fasta/MLST/SRST/summary.txt',
            'database':'/staphopia-ebs/staphopia-pipeline/fasta/MLST/saureus.txt',
            
            # Loci
            'arcc':'/staphopia-ebs/staphopia-pipeline/fasta/MLST/arcc.tfa', 
            'aroe':'/staphopia-ebs/staphopia-pipeline/fasta/MLST/aroe.tfa', 
            'glpf':'/staphopia-ebs/staphopia-pipeline/fasta/MLST/glpf.tfa', 
            'gmk_':'/staphopia-ebs/staphopia-pipeline/fasta/MLST/gmk_.tfa', 
            'pta_':'/staphopia-ebs/staphopia-pipeline/fasta/MLST/pta_.tfa', 
            'tpi_':'/staphopia-ebs/staphopia-pipeline/fasta/MLST/tpi_.tfa', 
            'yqil':'/staphopia-ebs/staphopia-pipeline/fasta/MLST/yqil.tfa', 
            'st':'/staphopia-ebs/staphopia-pipeline/fasta/MLST/saureus.txt'
        }

    """
        Assembly Method
    """    
    def __blast_locus(self, locus):
        """ Blast the locus sequences against the assembled genome. """
        outfmt = "6 qseqid bitscore qlen length gaps mismatch pident evalue"
        self.__sh.run_command('blastn',['blastn', '-db', self.__blastdb, '-query', self.mlst[locus], 
                                        '-outfmt', outfmt, '-max_target_seqs', '1'])
        self.__parse_blast(self.__sh.stdout, locus)
        
    def __parse_blast(self, blast_out, locus):
        """ Parse the blast output and keep track of the hits. """
        lines   = blast_out.split('\n')
        best_hit = {'score':999, 'id':locus + '-0','pident':'0.00', 'count':0, 'coverage':0.0} 
        for i in xrange(len(lines)):
            # 0: query seqid, 1: bitscore, 2: query length,      3: hit length
            # 4: total gaps,  5: mismatch, 6: percent identical, 7: evalue
            cols = lines[i].split('\t')
            if len(cols) == 8:
                score = int(cols[2]) - int(cols[3]) + int(cols[4]) + int(cols[5])

                # Best hit
                if score < best_hit['score']:
                    best_hit['score'] = score
                    best_hit['id'] = cols[0]
                    best_hit['pident'] = float(cols[6])
                    best_hit['count'] = 1
                    best_hit['coverage'] = float(cols[3]) / float(cols[2]) * 100
                    best_hit['bitscore'] = int(cols[1])
                elif score == best_hit['score']:
                    if float(cols[6]) > best_hit['pident']:
                        # Hit has a higher identity then previous best hit.
                        best_hit['id'] = cols[0]
                        best_hit['pident'] = float(cols[6])
                        best_hit['count'] = 1
                        best_hit['coverage'] = float(cols[3]) / float(cols[2]) * 100
                        best_hit['bitscore'] = int(cols[1])
                    elif int(cols[1]) > best_hit['bitscore']:
                        # Hit has a higher bitscore then previous best hit.
                        best_hit['id'    ] = cols[0]
                        best_hit['pident'] = float(cols[6])
                        best_hit['count' ] = 1
                        best_hit['coverage'] = float(cols[3]) / float(cols[2]) * 100
                        best_hit['bitscore'] = int(cols[1])
                    elif float(cols[6]) == best_hit['pident']:
                        # This variant has the same score and identity as another variant
                        best_hit['count'] += 1
                
        self.__blast_results[locus] = {'id':best_hit['id'][5:],     'score':best_hit['score'], 
                                      'pident':best_hit['pident'], 'count':best_hit['count'],
                                      'coverage': best_hit['coverage']}
                                      
    """
       Mapping Method
    """
    def __SRST(self):
        """ Determine ST using SRST """
        self.__sh.run_command('SRST', ['python', '/staphopia-ebs/staphopia-pipeline/bin/Third-Party/srst.py', 
                                       '-S', self.mlst['summary'], '-d', self.mlst['database'], 
                                       '-w', self.__out + '/srst', '-l', self.__out + '/srst.log', self.__fastx])
  
    def __parse_SRST(self):
        """ Parse the results of SRST """
        self.__srst_results = {}
        input = open(self.__out + '/srst.log', 'r')
        for l in input:
            cols = l.split('\t')
            if (cols[0] == 'FinalResults'):
                for i in xrange(3,10):
                    locus,id,score,count = cols[i].split(':')
                    self.__srst_results[locus] = {'id':id, 'score':score, 'count':count}
                    
    """
        Public Functions
    """
    def call_st(self, method):
        """Attempt to determine the ST of the input sequence."""
        if method == 'assembly':
            # Use the assembly
            self.__blast_results = {}
            for i in xrange(len(self.__loci)):
                self.__blast_locus(self.__loci[i])
            self.__Assembly = True
        elif method == 'mapping':
            # Use the raw reads
            if self.__mean <= 160:
                self.__sh.run_command('bwa aln', ['bwa', 'aln', '-f', self.__out + '/MLST.sai', '-t', '4', 
                                                  self.mlst['index'], self.__fastq])
                self.__sh.run_command('bwa samse', ['bwa', 'samse', '-n', '99', '-f', self.__out + '/MLST.sam', 
                                                    self.mlst['index'], self.__out + '/MLST.sai', self.__fastq])
            else:
                self.__sh.run_command('bwa bwasw', ['bwa', 'bwasw', '-t', '4', '-f', self.__out + '/MLST.sam', 
                                                    self.mlst['index'], self.__fastq])
            self.__sh.SamToFastX(self.__out + '/MLST.sam', self.__fastx, 'fastq')
            self.__SRST()
            self.__parse_SRST()
            self.__Mapping = True
        else:
            return False

    def compare_results(self):
        """ Compare the results of the assembly and mapping methods. """
        if self.__Assembly == True and self.__Mapping == True:
            for i in xrange(len(self.__loci)):
                origin = '0'
                flag   = '4'
                id     = '0'
                if self.__blast_results[self.__loci[i]]['score'] == 0:
                    if self.__blast_results[self.__loci[i]]['id'] == self.__srst_results[self.__loci[i]]['id'] and float(self.__srst_results[self.__loci[i]]['score']) >= 10:
                        id     = self.__blast_results[self.__loci[i]]['id']
                        flag   = '1'
                        origin = 'Assembly/Mapping'
                    elif self.__blast_results[self.__loci[i]]['count'] == 1:
                        id     = self.__blast_results[self.__loci[i]]['id']
                        flag   = '2'
                        origin = 'Assembly'
                    elif self.__srst_results[self.__loci[i]]['id'] != '-' and float(self.__srst_results[self.__loci[i]]['score']) >= 10 and int(self.__srst_results[self.__loci[i]]['count']) == 1:
                        id     = self.__srst_results[self.__loci[i]]['id']
                        flag   = '3'
                        origin = 'Mapping'          
      
                self.__alleles[self.__loci[i]]['id'] = id
                self.__alleles[self.__loci[i]]['assScore'] = self.__blast_results[self.__loci[i]]['score']
                self.__alleles[self.__loci[i]]['assCov'] = '%.2f' % self.__blast_results[self.__loci[i]]['coverage']
                self.__alleles[self.__loci[i]]['assIdent'] = self.__blast_results[self.__loci[i]]['pident']
                self.__alleles[self.__loci[i]]['assCount'] = self.__blast_results[self.__loci[i]]['count']
                self.__alleles[self.__loci[i]]['mapScore'] = self.__srst_results[self.__loci[i]]['score']
                self.__alleles[self.__loci[i]]['mapCount'] = self.__srst_results[self.__loci[i]]['count']
                self.__alleles[self.__loci[i]]['status'] = flag
            self.__Compared = True
        else:
            return False

    def print_MLST_results(self):
        """Print the final results."""
        if self.__Compared == True:
            status = ['','Assembly/Mapping','Assembly','Mapping','Unable to determine.']
            print "ST\t" + self.__st
            print "CC\t" + self.__cc
            for i in xrange(len(self.__loci)):
                l = self.__alleles[self.__loci[i]]
                print self.__loci[i],'\t',l['id'],'\t',l['assScore'],'\t',l['assCov'],'\t',l['assIdent'],'\t',l['assCount'],'\t',l['mapScore'],'\t',l['mapCount'],'\t',status[int(l['status'])]
        else:
            return False
       
    def print_MLST_table(self):
        """Print the final results."""
        if self.__Compared == True:
            id = ''
            for i in xrange(len(self.__loci)):
                id += str(self.__alleles[self.__loci[i]]['id']) + '\t'
                
            print self.__name, '\t', id, self.__st, '\t', self.__cc
        else:
            return False
       
    def determine_sequence_type(self):
        """Read the database to determine the ST and Clonal Complex"""
        if self.__Compared == True:
            sql = ''
            values = []
            matches = 0
            # Build the SQL statement
            for loci in self.__alleles:
                if (int(self.__alleles[loci]['id']) > 0):
                    sql += loci + '=%s AND '
                    values.append(self.__alleles[loci]['id'])
                    matches += 1
          
            if matches >= 5:
                sql = sql[:-5]
                rows = self.__db.query("select", """SELECT SequenceType 
                                                    FROM SequenceTypeProfile
                                                    WHERE """+ sql +"""
                                                    ORDER BY SequenceType ASC""", values)
                ST = []
                CC = {}
                for row in rows:
                    ST.append(row['SequenceType'])
                    cc = self.__db.query("select", """SELECT ClonalComplex 
                                                      FROM ClonalComplex
                                                      WHERE SequenceType=%s""", [str(row['SequenceType'])])
                    if len(cc) > 0:
                        if cc[0]['ClonalComplex'] in CC:
                            CC[ cc[0]['ClonalComplex'] ] += 1
                        else:
                            CC[ cc[0]['ClonalComplex'] ]  = 1
                  
                if len(ST) == 1 and matches == 7:
                    self.__st = str(ST[0])

                self.__top_clonal_complex(CC)
        else: 
            return False
      
       
    def __top_clonal_complex(self, CC):
        max = 0
        for cc in CC:
            if (CC[cc] > max):
                self.__cc  = str(cc)
                max = CC[cc]

    """
        Update Database
    """
    def update_status(self, status, jobid):
        return self.__db.query('update', """UPDATE Job
                                            SET MLST=%s 
                                            WHERE JobID=%s""", [status, jobid])

    def insert_stats(self, sample_id, runtime):
        sql = """INSERT INTO `SequenceType` (`SampleID`, `Version`, `RunTime`, `SequenceType`, `ClonalComplex`, 
                                             `arcc_id`, `arcc_aScore`, `arcc_aCov`, `arcc_aIdent`, `arcc_aCount`, 
                                             `arcc_mScore`, `arcc_mCount`, `arcc_status`, `aroe_id`, `aroe_aScore`, 
                                             `aroe_aCov`, `aroe_aIdent`, `aroe_aCount`, `aroe_mScore`, `aroe_mCount`, 
                                             `aroe_status`, `glpf_id`, `glpf_aScore`, `glpf_aCov`, `glpf_aIdent`, 
                                             `glpf_aCount`, `glpf_mScore`, `glpf_mCount`, `glpf_status`, `gmk__id`, 
                                             `gmk__aScore`, `gmk__aCov`, `gmk__aIdent`, `gmk__aCount`, `gmk__mScore`, 
                                             `gmk__mCount`, `gmk__status`, `pta__id`, `pta__aScore`, `pta__aCov`, 
                                             `pta__aIdent`, `pta__aCount`, `pta__mScore`, `pta__mCount`, `pta__status`, 
                                             `tpi__id`, `tpi__aScore`, `tpi__aCov`, `tpi__aIdent`, `tpi__aCount`, 
                                             `tpi__mScore`, `tpi__mCount`, `tpi__status`, `yqil_id`, `yqil_aScore`, 
                                             `yqil_aCov`, `yqil_aIdent`, `yqil_aCount`, `yqil_mScore`, `yqil_mCount`, 
                                             `yqil_status`) 
                 VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s, %s,%s,%s,%s,%s,%s,%s,%s,%s,%s, %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,
                         %s,%s,%s,%s,%s,%s,%s,%s,%s,%s, %s,%s,%s,%s,%s,%s,%s,%s,%s,%s, %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,
                         %s)"""
                         
        values = [sample_id, self.VERSION, runtime, self.__st, self.__cc, 
                  self.__alleles['arcc']['id'], self.__alleles['arcc']['assScore'], 
                  self.__alleles['arcc']['assCov'], self.__alleles['arcc']['assIdent'], 
                  self.__alleles['arcc']['assCount'], self.__alleles['arcc']['mapScore'], 
                  self.__alleles['arcc']['mapCount'], self.__alleles['arcc']['status'], 
                  self.__alleles['aroe']['id'], self.__alleles['aroe']['assScore'], 
                  self.__alleles['aroe']['assCov'], self.__alleles['aroe']['assIdent'], 
                  self.__alleles['aroe']['assCount'], self.__alleles['aroe']['mapScore'], 
                  self.__alleles['aroe']['mapCount'], self.__alleles['aroe']['status'], 
                  self.__alleles['glpf']['id'], self.__alleles['glpf']['assScore'], 
                  self.__alleles['glpf']['assCov'], self.__alleles['glpf']['assIdent'], 
                  self.__alleles['glpf']['assCount'], self.__alleles['glpf']['mapScore'], 
                  self.__alleles['glpf']['mapCount'], self.__alleles['glpf']['status'], 
                  self.__alleles['gmk_']['id'], self.__alleles['gmk_']['assScore'], 
                  self.__alleles['gmk_']['assCov'], self.__alleles['gmk_']['assIdent'],
                  self.__alleles['gmk_']['assCount'], self.__alleles['gmk_']['mapScore'], 
                  self.__alleles['gmk_']['mapCount'], self.__alleles['gmk_']['status'], 
                  self.__alleles['pta_']['id'], self.__alleles['pta_']['assScore'], 
                  self.__alleles['pta_']['assCov'], self.__alleles['pta_']['assIdent'], 
                  self.__alleles['pta_']['assCount'], self.__alleles['pta_']['mapScore'], 
                  self.__alleles['pta_']['mapCount'], self.__alleles['pta_']['status'], 
                  self.__alleles['tpi_']['id'], self.__alleles['tpi_']['assScore'], 
                  self.__alleles['tpi_']['assCov'], self.__alleles['tpi_']['assIdent'], 
                  self.__alleles['tpi_']['assCount'], self.__alleles['tpi_']['mapScore'], 
                  self.__alleles['tpi_']['mapCount'], self.__alleles['tpi_']['status'],  
                  self.__alleles['yqil']['id'], self.__alleles['yqil']['assScore'], 
                  self.__alleles['yqil']['assCov'], self.__alleles['yqil']['assIdent'], 
                  self.__alleles['yqil']['assCount'], self.__alleles['yqil']['mapScore'], 
                  self.__alleles['yqil']['mapCount'], self.__alleles['yqil']['status']]

        return self.__db.query('insert', sql, values)
        
    def clean_up(self):
        """ Clean up the mess! """
        self.__sh.run_command('mv', ['mv', self.__out +'/srst.log', self.__root +'/logs/'])
        self.__sh.run_command('bzip2', ['bzip2', '--best', self.__assembly])
        self.__sh.run_command('rm', ['rm', '-rf', self.__out])
        self.__sh.run_command('touch', ['touch', self.__root +'/logs/complete.mlst'])
        
