#! /usr/bin/python
"""
    Author: Robert A. Petit III
    Date: 3/25/2013
    Version: v0.4

    Staphopia's Assembly algorithm.  Based on the technology, reads are assembled with either Newbler (454, IonTorrent) 
    or Velvet (Illumina).  These contigs are then converted to a pseudo contig using Abacas, with Staphylococcus aureus 
    Newman as the reference strain.  Assembly statistics are determined with Assemblathon's method.

    Newbler      - http://454.com/products/analysis-software/index.asp
    Velvet       - http://www.ebi.ac.uk/~zerbino/velvet/
    Abasas       - http://abacas.sourceforge.net/
    Assemblathon - http://assemblathon.org/assemblathon-2-basic-assembly-metrics
  
    Change Log:
        v0.4 
            - Working with new database schema
        v0.3
            - Velvet assemblies are now split up, then reassembled using Newbler.
            - Removed getters and setters.
            - Removed unused code
            - Fixed spacing (all tabs replaced by 4 spaces)
        v0.2
            - Velvet now uses 4 different kmers, then those are merged.
            - use xrange instead of range, (for loops)
        v0.1
            - Initial set up.
"""
import os
from common import Shell
from database import Database
class Assemble(object):

    def __init__(self, log, args, fastq, out):
        self.VERSION = "0.4" 
        self.__sh = Shell(log, args)
        self.__db = Database()
        self.__fastq = fastq
        self.__out = out + '/Assembly'
        self.__root = out
        self.sample_tag = os.path.splitext(os.path.basename(fastq))[0].replace('_filtered', '')
        self.__sh.run_command('mkdir', ['mkdir', '-p', self.__out])
        
        self.__newbler = False
        self.__velvet = False
        
    def run_assembly(self, tech, mean):
        """Assemble the input according to the technology."""
        if tech == "454":
            self.__run_newbler()
        else:
            self.__run_multi_velvet(mean)

        self.__run_abacas()
        self.__run_stats()
        
    """ 
        Newbler - Assemble 454/Ion Torrent reads
    """
    def __newbler_version(self):
        """Get the version of Newbler."""
        self.__sh.run_command('runAssembly',['runAssembly', '-version'])
        lines = self.__sh.stdout.split('\n')
        self.__assembler_version = lines[0][12:15]
        self.__assembler = 'Newbler'
    
    def __run_newbler(self):
        """Run Newbler on the input."""
        self.__newbler = True
        self.__newbler_version()
        self.__sh.run_command('runAssembly', ['runAssembly', '-force', '-cpu', '4', '-a', '400', '-o', 
                                              self.__out + '/Newbler', self.__fastq])
        self.__sh.run_command('cp', ['cp',self.__out + '/Newbler/454AllContigs.fna', 
                                     self.__root + '/' + self.sample_tag + '_assembly.fa'])
        self.final_assembly = self.__root + '/' + self.sample_tag + '_assembly.fa'
        
    """ 
        Velvet - Assemble Illumina reads using multiple k-mers
    """
    def __velvet_version(self):
        """Get the version of Velvet"""
        self.__sh.run_command('velveth', ['velveth', '--version'])
        lines = self.__sh.stdout.split('\n')
        self.__assembler_version = lines[1][8:]
        self.__assembler = 'Velvet'
        
    def __run_velvet(self, kmer):
        """Run Velvet on the input."""
        kmer = str(kmer)
        velvet_dir = self.__out + '/Velvet/' + kmer
        self.__sh.run_command('velveth', ['velveth', velvet_dir, kmer, '-short', '-fastq', self.__fastq])
        self.__sh.run_command('velvetg', ['velvetg', velvet_dir, '-exp_cov', 'auto', '-cov_cutoff', 'auto', 
                                          '-min_contig_lgth', '100'])

    def __run_multi_velvet(self, mean):
        """Run Velvet on three kmers at once."""
        from multiprocessing import Process
        import shutil
        self.__velvet = True
        self.__velvet_version()
        self.__sh.run_command('mkdir', ['mkdir', self.__out + "/Velvet"]);
        
        # Determine kmers to use
        kmers = []
        if mean > 75:
            kmers = [int(round(mean * 0.39)), int(round(mean * 0.53)), int(round(mean * 0.67)), int(round(mean * 0.81))]
        elif mean > 49:
            kmers = [int(round(mean * 0.50)), int(round(mean * 0.60)), int(round(mean * 0.70)), int(round(mean * 0.80))]
        else:
            kmers = [int(round(mean * 0.75)), int(round(mean * 0.80)), int(round(mean * 0.85)), int(round(mean * 0.90))]

        # First two kmers
        a = Process(target=self.__run_velvet, args=(kmers[0],))
        b = Process(target=self.__run_velvet, args=(kmers[1],))
        a.start()
        b.start()
        a.join()
        b.join()
        
        # Last two kmers
        c = Process(target=self.__run_velvet, args=(kmers[2],))
        d = Process(target=self.__run_velvet, args=(kmers[3],))
        c.start()
        d.start()
        c.join()
        d.join()
        
        # Concatenate Velvet assemblies
        cat_assembly = self.__out + '/Velvet/catAssembly.fa'
        out = open(cat_assembly,'wb')
        for i in xrange(len(kmers)):
            shutil.copyfileobj(open(self.__out +'/Velvet/'+ str(kmers[i]) +'/contigs.fa'), out)
        out.close()
        
        # Split Contigs
        split_assembly = self.__out + '/Velvet/catAssembly.split.fa'
        self.__sh.run_command('splitter', ['splitter', '-sequence', cat_assembly, '-outseq', split_assembly, '-size', 
                                           '300', '-overlap', '200'])
        
        # Re-assemble using Newbler
        self.__newbler = True
        self.__sh.run_command('runAssembly', ['runAssembly', '-force', '-cpu', '4', '-a', '300', '-mi', '98', '-ml', 
                                              '75', '-ud',  '-rip', '-o', self.__out + '/Newbler', split_assembly ])
        self.__sh.run_command('cp', ['cp',self.__out + '/Newbler/454AllContigs.fna', 
                                     self.__root + '/' + self.sample_tag + '_assembly.fa'])
        self.final_assembly = self.__root + '/' + self.sample_tag + '_assembly.fa'

    """ 
        Abacas - Pseudocontig creation
    """
    def __run_abacas(self):
        """Join the assembly into a pseudocontig."""
        abacas_dir = self.__out + "/Abacas"
        self.__sh.run_command('mkdir', ['mkdir', abacas_dir]);
        self.__sh.run_command('abacas', ['abacas', '-r', '/staphopia-ebs/staphopia-pipeline/fasta/N315.fasta', 
                                         '-q', self.final_assembly, '-p', 'nucmer', '-c', '-o', 
                                         abacas_dir + "/pseudocontig"])
        self.__sh.run_command('cp', ['cp', abacas_dir + "/pseudocontig.fasta", 
                                     self.__root + '/'+ self.sample_tag + '_pseudocontig.fa'])
        self.pseudo_contig = self.__root + '/' + self.sample_tag + '_pseudocontig.fa'

    """ 
        Assembly Statisitcs/Summary 
    """
    def __run_stats(self):
        """Get the stats of the assembly."""
        self.__sh.run_command('AssemblathonStats.pl', ['AssemblathonStats.pl', self.final_assembly])
        self.stats = {}
        categories = self.__sh.stdout.split(';')
        for i in xrange(len(categories)-1):
            tag, value = categories[i].split('=')
            self.stats[ tag ] = value
            
    """
        Output/Database
    """  
    def print_stats(self):
        """Print the assembly stats."""

        for tag, value in self.stats.iteritems() :
            print tag,'\t',value
        print "assembler\t", self.__assembler
        print "version\t", self.__assembler_version
        
    def update_status(self, column, status, jobid):
        return self.__db.query('update', """UPDATE Job 
                                            SET """+ column +"""=%s 
                                            WHERE JobID=%s""", [status, jobid])

    def insert_stats(self, sample_id, runtime):
        program_id = self.__db.select_program(self.__assembler, self.__assembler_version)
        sql = """INSERT INTO `Assembly`(`SampleID`, `Version`, `RunTime`, `N50`, `GC`, `TotalContigs`, `TotalSize`, 
                                          `Mean`, `Median`, `MaxContig`, `MinContig`, `Greater500`, `Greater1K`, 
                                          `Greater10K`, `Greater100K`, `ProgramID`) 
                 VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""
                 
        values = [sample_id, self.VERSION, runtime, self.stats['N50'], self.stats['gc'], self.stats['total_contig'], 
                  self.stats['total_size'], self.stats['mean'], self.stats['median'], self.stats['max_contig'], 
                  self.stats['min_contig'], self.stats['greater_500'], self.stats['greater_1K'], 
                  self.stats['greater_10K'], self.stats['greater_100K'], program_id]
                      
        return self.__db.query('insert', sql, values)

    """
        Blast Database
    """
    def make_blast_database(self, which):
        blast = self.__root +'/blastdb'
        if not os.path.exists(blast):
            self.__sh.run_command('mkdir', ['mkdir', '-p', blast])
        
        cmd = False
        db = False
        if which == 'assembly':
            cmd = ['makeblastdb', '-in', self.final_assembly, '-dbtype', 'nucl', '-title', 
                   "'" + self.sample_tag + "' denovo assembly", '-out', blast +'/'+ self.sample_tag +'_assembly']
            db = blast +'/'+ self.sample_tag +'_assembly'
        else:
            cmd = ['makeblastdb', '-in', self.pseudo_contig, '-dbtype', 'nucl', '-title', 
                   "'"+ self.sample_tag +"' pseudocontig", '-out', blast +'/'+ self.sample_tag +'_pseudocontig']
            db = blast +'/'+ self.sample_tag +'_pseudocontig'
        
        self.__sh.run_command('makeblastdb', cmd)
        return db

    def clean_up(self):
        """ Clean up the mess """
        self.__sh.run_command('rm', ['rm', '-rf', self.__out])
        self.__sh.run_command('rm', ['rm', self.__root +'/nucmer.delta'])
        self.__sh.run_command('rm', ['rm', self.__root +'/nucmer.filtered.delta'])
        self.__sh.run_command('rm', ['rm', self.__root +'/nucmer.tiling'])
        self.__sh.run_command('rm', ['rm', self.__root +'/unused_contigs.out'])
        self.__sh.run_command('bzip2', ['bzip2', '--best', self.pseudo_contig])

