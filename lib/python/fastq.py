#! /usr/bin/python
"""
     Author: Robert A. Petit III
       Date: 03/25/2013
    Version: v0.4

    Staphopia's FastQ filtering algorithm.  The first 50000 reads are used to assess statistics such as average read 
    length, average quality, estimated coverage, among others.  This information is then used to filter out low quality
    reads, and trim if necessary.

    Change Log:
        v0.4
            - Renamed version to VERSION
        v0.3
            - Refactored code
            - Remove reads with avg q-score below 2.5 STD of the mean
            - Remove reads with avg length below 2 STD of the mean
            - No longer outputs FASTA
            - Stats are over the whole set of reads, instead of just the initial 50k reads
            - Use xrange instead of range of __Randomize() and for loops
        v0.2
            - Reads with an average quality below two standard deviations from the mean are removed.
        v0.1
            - Initial set up.
"""
import random
import re
import numpy as np
from common import Shell
from database import Database

class ProcessFastQ(object):
    
    def __init__(self, log, args):
        self.VERSION = "0.5" 
        self.__sh = Shell(log, args)
        self.__db = Database()

    def __numpy_stats(self, array):
        a = np.array(array)
        return {'mean':np.mean(a), 'std':np.std(a), 'median':np.median(a), 'min':np.min(a), 'max':np.max(a), 
                'total': np.sum(a), 'count': len(a)}
        
    def __mean_quality(self, qual, base_qual):
        """Create a count of the quality score"""
        qscore = 0
        for j in xrange(len(qual)):
            if base_qual:   
                if j not in base_qual:
                    base_qual[j] = []
                base_qual[j].append(ord(qual[j]) - 33)
            qscore += ord(qual[j]) - 33
 
        return [qscore/len(qual),base_qual]
    
    def __quality_per_base(self, base_qual):
        for i in xrange(len(base_qual)):
            a = np.array(self.perBase[i])
            base_qual[i] = {'mean':np.mean(a), 'std':np.std(a), 'median':np.median(a), 'min':np.min(a), 'max':np.max(a), 
                            'total': np.sum(a), 'count': len(a)}
            if self.upper_bound == self.estimates['length']['max'] and base_qual[i]['mean'] < self.min_qual:
                self.upper_bound = i - 1
        return base_qual
        
    def estimate_stats(self, fastq):
        """Determine summary statistics of the input file"""
        self.estimates = {}
        quals = []
        lengths = []
        base_qual = {}

        fq = open(fastq, 'r')
        while (len(quals) < 50000):
            head = fq.readline()
            if not head:
                break
            seq  = fq.readline().rstrip()
            plus = fq.readline()
            qual = fq.readline().rstrip()

            if not re.search('N', seq):
                lengths.append(len(seq))
                mean, base_qual = self.__mean_quality(qual, base_qual)
                quals.append(mean)
                
        fq.close()
        self.estimates['length'] = self.__numpy_stats(lengths)
        self.lower_bound = int(self.estimates['length']['mean']) - int(2 * self.estimates['length']['std'])
        self.upper_bound = int(self.estimates['length']['max'])
        
        self.estimates['read_qual'] = self.__numpy_stats(quals)
        self.min_qual = int(self.estimates['read_qual']['mean']) - int(2.5 * self.estimates['read_qual']['std'])
        
        self.estimates['base_qual'] = self.__quality_per_base(base_qual)
    
    def __total_reads(self, fastq):
        self.__sh.run_command('wc', ["wc", "-l", fastq])
        return int(self.__sh.stdout.split(' ')[0]) / 4
        
        
    def process_fastq(self, out, fastq, coverage, genome_size):
        self.original_stats = {}
        self.final_stats = {}
        self.read_set = {}
        self.reads = { 
            'max':self.__total_reads(fastq),
            'subset': int(coverage * genome_size / self.estimates['length']['mean']),
            'buffer': int(coverage * 0.33 * genome_size / self.estimates['length']['mean'])
        }
        
        self.__randomize()
        self.__write_fastq(out, fastq)


    def __randomize(self):
        """Creates a random number dictionary"""
        if self.reads['max'] <= self.reads['subset']:
            # Asked for more reads, than exists
            self.reads['subset'] = self.reads['max']
            for i in xrange(1,self.reads['max']+1):
                self.read_set[ i ] = 'subset'
        else:
            # Create random sample
            L = 0
            if self.reads['subset'] + self.reads['buffer'] > self.reads['max']:
                L = random.sample(xrange(1,self.reads['max']+1), self.reads['max']-1)
            else:
                L = random.sample(xrange(1,self.reads['max']+1), self.reads['subset'] + self.reads['buffer']) 

            for i in xrange(len(L)):
                if i < self.reads['subset']:
                    self.read_set[L[i]] = 'subset'
                else:
                    self.read_set[L[i]] = 'buffer' 
                    
    def __write_fastq(self, out, fastq):
        """Pull reads from an input fastq file"""
        import os
        i = 0
        Head = { 'subset':[],'buffer':[] }
        Seq  = { 'subset':[],'buffer':[] }
        Qual = { 'subset':[],'buffer':[] }
        quals = { 'subset':[],'buffer':[], 'all':[] }
        lengths = []
        all_lengths = []

        fq = open(fastq, 'r')
        while 1:
            head   = fq.readline()
            if not head:
                break

            seq = fq.readline().rstrip()
            length = len(seq)
            plus   = fq.readline()
            qual   = fq.readline().rstrip()
            
            # Stats for whole file
            all_lengths.append(length)
            tmpQual,base_qual = self.__mean_quality(qual.rstrip(), False)
            quals['all'].append(tmpQual)
            
            i += 1
            if (i in self.read_set):
                if (not re.search('N', seq) and length >= self.lower_bound):
                    if (length > self.upper_bound):
                        seq = seq[:self.upper_bound]
                        qual = qual[:self.upper_bound]
                        tmpQual,base_qual = self.__mean_quality(qual, False)

                    if tmpQual >= self.min_qual:
                        Head[self.read_set[i]].append(head)
                        Seq[self.read_set[i]].append(seq)
                        Qual[self.read_set[i]].append(qual)
                        quals[self.read_set[i]].append(tmpQual)

        fq.close()
        
        # Check if we need to use some buffer reads
        r = self.reads['subset'] - len(Head['subset'])
        b = len(Head['buffer'])
        if r > 0 and b > 0:
            if r > b:
                r = b - 1
            L = random.sample(range(1,b), r)
            for i in xrange(len(L)):
                Head['subset'].append(Head['buffer'][i])
                Seq['subset'].append(Seq['buffer'][i])
                Qual['subset'].append(Qual['buffer'][i])
                quals['subset'].append(quals['buffer'][i])
        
        # Write the fastq
        self.fastq = out +'/'+ os.path.splitext(os.path.basename(fastq))[0].replace('_original', '') +'_filtered.fastq'
        outfq = open(self.fastq, 'w')
        for i in xrange(len(Head['subset'])):
            outfq.write(Head['subset'][i] + Seq['subset'][i] + "\n+\n" + Qual['subset'][i] + "\n")
            lengths.append(len(Seq['subset'][i]))
        outfq.close()
        
        # Calcualte Stats
        self.original_stats['length'] = self.__numpy_stats(all_lengths)        
        self.original_stats['read_qual'] = self.__numpy_stats(quals['all'])
        self.original_stats['fastq'] = fastq
        self.final_stats['length'] = self.__numpy_stats(lengths)        
        self.final_stats['read_qual'] = self.__numpy_stats(quals['subset'])

    def insert_stats(self, original, sample_id, runtime, genome_size):
        if original:
            md5sum = self.MD5sum(self.original_stats['fastq'])
            values = [sample_id, '0', runtime, md5sum, self.original_stats['length']['total'], 
                      self.original_stats['length']['count'], self.original_stats['length']['min'], 
                      self.original_stats['length']['mean'], self.original_stats['length']['max'],
                      "%3.3f" % self.original_stats['read_qual']['mean'], 
                      "%3.3f" % (self.original_stats['length']['total']/float(genome_size))]
                      
        else:
            md5sum = self.MD5sum(self.fastq)
            values = [sample_id, self.VERSION, runtime, md5sum, self.final_stats['length']['total'], 
                      self.final_stats['length']['count'], self.final_stats['length']['min'], 
                      self.final_stats['length']['mean'], self.final_stats['length']['max'],
                      "%3.3f" % self.final_stats['read_qual']['mean'], 
                      "%3.3f" % (self.final_stats['length']['total']/float(genome_size))]
                      
        return self.__db.query('insert', """INSERT INTO FilteredStat (SampleID, Version, RunTime, MD5sum, BaseCount, 
                                                                       ReadCount, MinLength, MeanLength, MaxLength, 
                                                                       Quality, Coverage) 
                                            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""", values)

    def update_status(self, column, status, jobid):
        return self.__db.query('update', """UPDATE Job
                                            SET """+ column +"""=%s 
                                            WHERE JobID=%s""", [status, jobid])

    def print_stats(self, genome_size):
        print 'Original FASTQ:'
        print '-----'
        print 'Base Count: %d bp' % self.original_stats['length']['total']
        print 'Read Count: %d' % self.original_stats['length']['count']
        print 'Mean Read Length: %d bp' % self.original_stats['length']['mean']
        print 'Minimum Read Length: %d bp' % self.original_stats['length']['min']
        print 'Maximum Read Length: %d bp' % self.original_stats['length']['max']
        print 'Mean Quality: Q%3.3f' % self.original_stats['read_qual']['mean']
        print 'Estimated Coverage: %3.3fx' % (self.original_stats['length']['total']/float(genome_size))
        print ''
        print 'Processed FASTQ:'
        print '-----'
        print 'Base Count: %d bp' % self.final_stats['length']['total']
        print 'Read Count: %d' % self.final_stats['length']['count']
        print 'Mean Read Length: %d bp' % self.final_stats['length']['mean']
        print 'Minimum Read Length: %d bp' % self.final_stats['length']['min']
        print 'Maximum Read Length: %d bp' % self.final_stats['length']['max']
        print 'Mean Quality: Q%3.3f' % self.final_stats['read_qual']['mean']
        print 'Estimated Coverage: %3.3fx' % (self.final_stats['length']['total']/float(genome_size))
        print ''
        
    def MD5sum(self, file):
        """ Calculate the md5 hash of an input file. """
        self.__sh.run_command('md5sum', ['md5sum', file])
        md5sum = self.__sh.stdout.split(' ')
        return str(md5sum[0])

    def clean_up(self, fastq):
        """ Compress the original FASTQ """
        
        # If its public (sra data) delete the original FASTQ
        if "/public/public_" in fastq:
            self.__sh.run_command('rm', ['rm', fastq])
        else:
            self.__sh.run_command('bzip2', ['bzip2','--best', fastq])
