#! /usr/bin/python
import re
import time
import subprocess

"""
    Author: Robert A. Petit III
    Date: 4/29/2013
    Version: v0.2

    Staphopia's common commands and program usage.  Primarily used to write to log
  
    Change Log:
        v0.2
            - Removed unused methods
        v0.1
            - Initial set up.
"""

class Shell(object):
    """ Functions common throughout Staphopia's pipeline. """
    def __init__(self, log, args):
        self.VERSION = "0.2"
        self.__log = open(log , 'a+')
        self.__log.write(str(time.asctime(time.localtime(time.time())) +'\n'))
        self.__log.write(args +'\n\n')
        
    def run_command(self, program, command):
        """ Allows execution of a simple command. """
        self.stderr = None
        self.stdout = None 
        p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.stdout, self.stderr = p.communicate()
        
        # Log the program, output, and errors
        self.__log.write('---[ '+ program +' ]---\n')
        self.__log.write('Command: \n' + ' '.join(command) + '\n\n')
        if (len(self.stdout) > 0 and len(self.stdout) < 30000):
            self.__log.write('Standard Output: \n' + self.stdout + '\n\n')
        if (len(self.stderr) > 0):
            self.__log.write('Standard Error: \n' + self.stderr + '\n\n')
            
    def log(self, message):
        self.__log.write(message)
        
    def blast_version(self, blast):
        """Get the version of BLAST."""
        self.run_command('blast version',[blast, '-version'])
        lines = self.stdout.split('\n')
        return lines[0].replace(blast+': ', '')
        
    def SamToFastX(self, input, output, type):
        """ Write the aligned reads to Fasta/Fastq format. """
        sam = open(input, 'r')
        out = open(output, 'w')
        c = 1
        while 1:
            l = sam.readline()
            if not l:
                break

            F = l.split('\t')
            if (len(F) >= 9) and (F[2] != "*"):
                if (type == 'fasta'):
                    out.write('>'+ str(c) + '\n' + F[9] + '\n')
                    c += 1
                elif (F[10] != "*"):
                    if (len(F[9]) <= 160):
                        out.write('@' + F[0] + '\n' + F[9] + '\n+\n' + F[10] + '\n')
                    else:
                        seqs  = self.__split(F[9], 100)
                        quals = self.__split(F[10], 100)
                        for i in range(0,len(seqs)):
                            if (len(seqs[i]) >= 25):
                                out.write('@' + F[0] + '_' + str(i) + '\n' + seqs[i] + '\n+\n' + quals[i] + '\n')
                
        out.close()
        sam.close()
            
    def __split(self, input, size):
        return [input[start:start+size] for start in range(0, len(input), size - 25)]
            
class RegEx(object):
    def __init__(self, matchstring):
        self.matchstring = matchstring

    def match(self,regexp):
        self.rematch = re.match(regexp, self.matchstring)
        return bool(self.rematch)

    def group(self,i):
        return self.rematch.group(i)