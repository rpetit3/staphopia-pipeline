#! /usr/bin/python
"""
     Author: Robert A. Petit III
       Date: 5/9/2013
    Version: v0.4

    Staphopia's SCCmec algorithm takes a few approaches.    It maps reads against SCCmec cassettes, blasts proteins and primers
    against the assembled genomes.    Based on the results of these, a SCCmec cassette is determined, if there is enough 
    confidence in the results.    Reads are mapped using BWA, and proteins/primers are aligned using blast.
    
    Change Log:
        v0.5
            - Coverage plots now drawn using ggplot2
        v0.4
            - Working with new databse schema
        v0.3
            - Refactored code
            - Modified to work with new naming schema in Common.py
            - Fixed spacing (all tabs replaced by 4 spaces)
        v0.2
            - Replaced range with xrange, (for loops)
        v0.1
            - Initial set up.
"""
import os
from common import Shell
from database import Database

class SCCmecType:
    def __init__(self, log, args, fastq, out, blastdb):
        self.VERSION = "0.5"
        self.__fastq = fastq
        self.__sample_tag = os.path.splitext(os.path.basename(fastq))[0].replace('_filtered', '')
        self.__out = out + '/SCCmec'
        self.__root = out
        self.__blastdb = blastdb
        
        self.__results = {'best':0}
        self.__types = ["I"  , "IIa", "IIb", "IIe", "III", "IVa", "IVb", "IVc" , "IVd", "IVe", "IVg",
                        "IVh", "IVi", "IVj", "IVl", "V"  , "VI" , "VII", "VIII", "IX" , "X"  , "XI"]
        self.__lengths = {
               "I":39332, "IIa":58237, "IIb":26207, "IIe":27029, "III":68256, "IVa":26090, "IVb":21777, "IVc":31263,
             "IVd":14223, "IVe":23600, "IVg":23157, "IVh":46332, "IVi":23124, "IVj":23328, "IVl":35517,   "V":28612,
              "VI":23293, "VII":33261,"VIII":33743,  "IX":44355,   "X":51483,  "XI":33042
        }
        self.__ccrA = {'ccrA':1, 'ccrA1':1, 'ccrA2':1, 'ccrA3':1, 'ccrA4':1}
        self.__ccrB = {'ccrB':1, 'ccrB1':1, 'ccrB2':1, 'ccrB3':1, 'ccrB4':1}
        self.__ccrC = {'ccrC':1, 'ccrCf':1, 'ccrCr':1, 'ccrC1':1, 'ccrC2':1}
        self.__output = {'coverage': '', 'protein': '', 'primer': ''}
        self.__mec_type = {'coverage': 'Negative', 'protein': 'Negative', 'primer': 'Negative'}
        self.__mecA = False
        for i in xrange(len(self.__types)):
            self.__results[self.__types[i]] = {'max': 0, 'cov': "0", 'align': 0, 'x':[], 'y':[], 'seen':False}
            
        self.__sccmec = {
            'index':'/staphopia-ebs/staphopia-pipeline/index/SCCmec/SCCmec',
            'primers':'/staphopia-ebs/staphopia-pipeline/fasta/SCCmec/primers.fna',
            'proteins':'/staphopia-ebs/staphopia-pipeline/fasta/SCCmec/proteins.faa'
        }
        
        self.__sh = Shell(log, args)
        self.__db = Database()
        self.__sh.run_command('mkdir', ['mkdir', '-p', self.__out])
        self.__mec_types()
        
    def sccmec_mapping(self, mean):
        """Align reads against SCCmec cassettes to produce graphs of the coverage."""
        # Use the raw reads  
        if mean <= 160:
            self.__sh.run_command('bwa aln', ['bwa', 'aln', '-f', self.__out + '/SCCmec.sai', '-t', '4', 
                                              self.__sccmec['index'], self.__fastq])
            self.__sh.run_command('bwa samse', ['bwa', 'samse', '-n', '9999', '-f', self.__out + '/SCCmec.sam', 
                                                self.__sccmec['index'], self.__out + '/SCCmec.sai', self.__fastq])
        else:
            self.__sh.run_command('bwa bwasw', ['bwa', 'bwasw', '-t', '4', '-f', self.__out + '/SCCmec.sam', 
                                                self.__sccmec['index'], self.__fastq])
        # SAM to BAM
        self.__sh.run_command('samtools view', ['samtools', 'view', '-bSh', '-o', self.__out +'/SCCmec.bam', 
                                                self.__out + '/SCCmec.sam'])
        self.__sh.run_command('samtools sort', ['samtools', 'sort', self.__out +'/SCCmec.bam', self.__out +'/SCCmec_s'])

        self.__sccmec_coverage()
        self.__alignment_results()
        
        # Finalize coverage results
        for i in xrange(len(self.__types)):
            self.__output['coverage'] += self.__types[i] + '\t' + str(self.__results[self.__types[i]]['max']) + '\t' 
            self.__output['coverage'] += str(self.__results[self.__types[i]]['cov']) + '\t' 
            self.__output['coverage'] += str("%3.2f" % self.__results[self.__types[i]]['align']) + "\n"
        
        if (self.__results['best'] > 80):
            self.__mec_type['coverage'] = self.__results['bestType']
            print "Top\t", self.__results['bestType'], '\t', self.__results['best']
        elif (self.__mecA):
            self.__mec_type['coverage'] = 'unknown'
            print "mecA found, but due to low coverage, we are unable to determine a SCCmec type."
        else:
            print "There were not any SCCmec cassettes that were significantly covered."
        
    def __sccmec_coverage(self):
        self.__sh.run_command('GenomeCoverageBed', ['genomeCoverageBed', '-ibam', self.__out + '/SCCmec_s.bam', '-d'])
        self.__cov = self.__sh.stdout.split('\n')
                    
    def __alignment_results(self):
        """Review and analyze the alignment results."""
        self.__reset("I",0,0)
            
        self.coverage = self.__out + '/coverage.txt'
        outfh = open(self.coverage, 'w')        
        for i in xrange(len(self.__cov)):
            if not self.__cov[i]:
                self.__add_results(self.__percent_aligned())
                break

            id,pos,hits = self.__cov[i].split('\t')
            hits = int(hits)
            if (hits > 100):
                hits = 100
            tmpType = id.split('|')[0]
            outfh.write(str(tmpType) + "\t" + str(pos) + "\t" + str(hits) + "\n")
            self.__results[tmpType]['x'].append(int(pos))
            self.__results[tmpType]['y'].append(hits)
            
            if (tmpType == self.__type):
                self.__total += 1
                self.__sum += hits
                self.__check_max(hits)
                self.__check_cov(hits)
            else:
                self.__add_results(self.__percent_aligned())
                self.__reset(tmpType,1,hits)
                self.__check_max(hits)
                self.__check_cov(hits)
                
        # Add Unaligned Types
        for i in xrange(len(self.__types)):
            t = self.__types[i]
            l = self.__lengths[t]
            if not self.__results[t]['seen']:
                for j in xrange(l):
                    outfh.write(t + "\t" + str(j) + "\t0\n")
        outfh.close()        
                
    def __reset(self, type, total, hits):
        """Reset the valuee for a new SCCmec type."""
        self.__type = type
        self.__max = 0
        self.__total = total
        self.__count = 0
        self.__sum = hits
        
    def __percent_aligned(self):
        return float(self.__count)/self.__total*100
                
    def __add_results(self, aligned):
        """Add the results to a dictionary of results"""
        if (aligned > self.__results['best']):
            self.__results['best'] = aligned
            self.__results['bestType'] = self.__type
         
        self.__results[self.__type]['max'] = self.__max
        self.__results[self.__type]['cov'] = "%3.2f" % (float(self.__sum)/self.__count)
        self.__results[self.__type]['align'] = aligned
        self.__results[self.__type]['seen'] = True
        
    def __check_max(self, i):
        """Check for a new max."""
        if (i > self.__max):
            self.__max = i
    
    def __check_cov(self,i):
        """Check if the position is covered."""
        if (i > 0):
            self.__count += 1
 
    """
        Assembly Method - Search for primers and proteins.
    """
    def sccmec_assembly(self):
        """Use the assembled genome to serch for primer and protein hits."""
        self.__proteins()
        self.__primers()
        
    def __primers(self):
        self.__sh.run_command('blastn', ['blastn', '-max_target_seqs', '1', '-dust', 'no', '-word_size', '7', 
                                         '-perc_identity', '100', '-db', self.__blastdb, '-outfmt', 
                                         '6 qseqid nident qlen length sstart send pident ppos bitscore evalue', 
                                         '-query', self.__sccmec['primers']])
        self.__output['primer'] = self.__sh.stdout
        blastOut = self.__sh.stdout.split('\n')
        blast = []
        for i in xrange(len(blastOut) - 1):
            col = blastOut[i].split('\t')
            if (float(col[1])/float(col[2]) >= 0.80):
                blast.append(blastOut[i])

        if (len(blast) < 3):
            print "There were no significant SCCmec related primer hits."
            self.__mec_type['primer'] = 'Negative'
        else:
            type = self.__parse_blast(blast)
            self.__mec_type['primer'] = type
            if (type == 0):
                print "Unable to determine a SCCmec type via primer hits."
            else:
                print "The SCCmec type based off primer hits is: " + type

    def __proteins(self):
        self.__sh.run_command('tblastn', ['tblastn', '-max_target_seqs', '1', '-outfmt', 
                                          '6 qseqid nident qlen length sstart send pident ppos bitscore evalue gaps', 
                                          '-query', self.__sccmec['proteins'], '-db', self.__blastdb])
        self.__output['protein'] = self.__sh.stdout
        blastOut = self.__sh.stdout.split('\n')
        blast = []
        for i in xrange(len(blastOut) - 1):
            col = blastOut[i].split('\t')
            # 80% identity, %50 covered
            print col[0], '\t',col[6], '\t', float(col[3])/float(col[2])
            if (float(col[6]) >= 80 and float(col[3])/float(col[2]) >= 0.50):
                blast.append(blastOut[i])

        if (len(blast) < 3):
            print "There were no significant SCCmec related protein hits."
            self.__mec_type['protein'] = 'Negative'
        else:
            type = self.__parse_blast(blast)
            self.__mec_type['protein'] = type
            if (type == 0):
                print "Unable to determine a SCCmec type via protein hits."
            else:
                print "The SCCmec type based off protein hits is: " + type
                 
    def __parse_blast(self,list):
        "Determines the SCCmec type based on Blast hits."
        seen = { 'ccrA': 0,'ccrB': 0,'ccrC': 0,'mecA': 0,'mecR1': 0,'mecI': 0,'IS431': 0,'IS1272': 0,'blaZ': 0}
        SCCmec = {}     
                        
        for i in xrange(len(list)):
            col = list[i].split('\t')
            header = col[0].split('|')
            if (header[0] in self.__ccrA):
                if (seen['ccrA']):
                    if (col[8] > SCCmec['ccrA']['bitscore']):
                        SCCmec['ccrA'] = { 'type': header[0][3:], 'bitscore': col[8] }
                else:
                    seen['ccrA'] = 1
                    SCCmec['ccrA'] = { 'type': header[0][3:], 'bitscore': col[8] }
            elif (header[0] in self.__ccrB):
                if (seen['ccrB']):
                    if (col[8] > SCCmec['ccrB']['bitscore']):
                        SCCmec['ccrB'] = { 'type': header[0][3:], 'bitscore': col[8] }
                else:
                    seen['ccrB'] = 1
                    SCCmec['ccrB'] = { 'type': header[0][3:], 'bitscore': col[8] }
            elif (header[0] in self.__ccrC):
                if (seen['ccrC']):
                    if (col[8] > SCCmec['ccrC']['bitscore']):
                        SCCmec['ccrC'] = { 'type': header[0][3:], 'bitscore': col[8] }
                else:
                    seen['ccrC'] = 1
                    SCCmec['ccrC'] = { 'type': header[0][3:], 'bitscore': col[8] }
            elif (header[0] == "mecA"):
                SCCmec['mecA'] = 1
                self.__mecA = True
            elif (header[0] == "mecR1"):
                SCCmec['mecR1'] = 1
            elif (header[0] == "mecI"):
                SCCmec['mecI'] = 1
            elif (header[0] == "IS431"):
                SCCmec['IS431'] = 1
            elif (header[0] == "IS1272"):
                SCCmec['IS1272'] = 1
            elif (header[0] == "blaZ"):
                SCCmec['blaZ'] = 1
                
        return self.__get_type(SCCmec)
        
    def __get_type(self, SCCmec):
        "Returns the SCCmec Type"
        ccr = ''
        if ('ccrA' in SCCmec and 'ccrB' in SCCmec):
            ccr = SCCmec['ccrA']['type'] + SCCmec['ccrB']['type'] 
        elif ('ccrC' in SCCmec):
            ccr = SCCmec['ccrC']['type']

        mec = ''
        if ('IS431' in SCCmec and 'mecA' in SCCmec and 'mecR1' in SCCmec):
            if ('mecI' in SCCmec):
                mec = 'A'
            elif ('IS1272' in SCCmec):
                mec = 'B'
            elif ('blaZ' in SCCmec):
                mec = 'E'
            else:
                mec = 'C'
                
        type = ''
        if (mec == 'A'):
            if (ccr == 'A2B2' or ccr == 'A2B'):
                type = 'II'
            elif (ccr == 'A3B3' or ccr == 'A3B'):
                type = 'III'
            elif (ccr == 'A4B4'):
                type = 'VIII'
        elif (mec == 'B'):
            if (ccr == 'A1B1' or ccr == 'A1B'):
                type = 'I'
            elif (ccr == 'A2B2' or ccr == 'A2B'):
                type = 'IV'
            elif (ccr == 'A4B4'):
                type = 'VI'
        elif (mec == 'E'):
            if (ccr == 'A1B3' or ccr == 'A1B'):
                type = 'XI'
        elif (mec == 'C'):
            if (ccr == 'C1' or ccr == 'A1B1' or ccr == 'A1B' or ccr == 'A1B6'):
                type = 'V,VII,IX,X'

        if (type == ''):
            if ('mecA' in SCCmec):
                return 'Unknown'
            else:
                return "Negative"
        else:
            return type
        
    def print_results(self):
        print "SCCmec Type"
        print "Coverage: "+ self.__mec_type['coverage'] +" "+ str(self.__sccmec_types[ self.__mec_type['coverage'] ])
        print self.__output['coverage']
        print ""
        print "-----------"
        print "Protein: " + self.__mec_type['protein'] + " " + str(self.__sccmec_types[ self.__mec_type['protein'] ])
        print self.__output['protein']
        print ""
        print "-----------"
        print "Primer: " + self.__mec_type['primer'] + " " + str(self.__sccmec_types[ self.__mec_type['primer'] ])
        print self.__output['primer']


    def draw_plots(self, sample_id):
        self.__sh.run_command('scccmec_coverage.r',['Rscript','/staphopia-ebs/staphopia-pipeline/bin/sccmec_coverage.r',
                                                    str(sample_id), self.coverage, '/var/www/staphopia/html/images/',
                                                    self.__sample_tag])
        
    """
        Update Database
    """
    def __mec_types(self):
        """Retrieve a list of SCCmec types from the database."""
        self.__sccmec_types = {}
        rows = self.__db.query("select", "SELECT TypeID,Type FROM SCCmecType", [])
        for row in rows:
            self.__sccmec_types[ row['Type'] ] = row['TypeID'] 
            
    def update_status(self, status, jobid):
        return self.__db.query('update', """UPDATE Job 
                                            SET SCCmec=%s 
                                            WHERE JobID=%s""", [status, jobid])
                                            
    def insert_stats(self, sample_id, runtime):
        # Get ProgramID and BlastID
        program_id = self.__db.select_program('tblastn', self.__sh.blast_version('tblastn'))
        if program_id:
            blast_id = self.__db.new_blast(program_id, sample_id)
            if blast_id:
                sccmec_id = self.__db.query('insert', 
                                            """INSERT INTO `SCCmec`(`SampleID`, `BlastID`, `Version`, `RunTime`, 
                                                                    `CoverageType`, `PrimerType`, `ProteinType`) 
                                               VALUES (%s, %s, %s, %s, %s, %s, %s)""", 
                                            [sample_id, blast_id, self.VERSION, runtime, 
                                             self.__sccmec_types[ self.__mec_type['coverage'] ],
                                             self.__sccmec_types[ self.__mec_type['primer'] ],
                                             self.__sccmec_types[ self.__mec_type['protein'] ]])
                if sccmec_id:
                    self.__db.select_accessions()
                    self.__db.select_genes()
                    self.__db.select_primers()
                    self.__db.select_databases()
                    
                    # Insert coverage
                    coverage = self.insert_coverage(sccmec_id)
                    
                    # Insert proteins
                    proteins = self.insert_proteins(blast_id)
                    
                    # Insert primers
                    primers = self.insert_primers(sccmec_id)
                    
                    if coverage and proteins and primers:
                        return True
                    else:
                        return False
                else:
                    print 'There was an error creating a new SCCmec entry.'
            else:
                print 'There was an error creating a new BLAST entry.'
        else:
            print 'There was an error inserting/retrieving protein blast program id.'
        
    def insert_coverage(self, sccmec_id):
        success = True
        for i in xrange(len(self.__types)):
            rows = self.__db.query('insert', """INSERT INTO `SCCmecCoverage`(`SCCmecID`, `TypeID`, `Max`, `Mean`, 
                                                                              `Percent`) 
                                                VALUES (%s,%s,%s,%s,%s)""", 
                                    [sccmec_id, self.__sccmec_types[ self.__types[i] ], 
                                     self.__results[self.__types[i]]['max'], self.__results[self.__types[i]]['cov'],
                                     "%3.2f" % self.__results[self.__types[i]]['align']])
            if not rows:
                print 'There was an error inserting coverage result'
                success = False
                break
            
        return success
        
    def insert_proteins(self, blast_id):
        # '6 qseqid nident qlen length sstart send pident ppos bitscore evalue gaps'
        success = True
        blast = self.__output['protein'].split('\n')
        for i in xrange(len(blast) - 1):
            col = blast[i].split('\t')
            gene, accession = col[0].split('|')
            if not 'UniProt' in self.__db.databases:
                self.__db.insert_database('UniProt')
            
            if not gene in self.__db.genes:
                self.__db.insert_gene(gene)
                
            if not accession in self.__db.accessions:
                self.__db.insert_accession(self.__db.databases['UniProt'], self.__db.genes[gene], accession)
                
            score = int(col[2]) - int(col[3]) + int(col[10]) # (Query Length - Alignment Length) + Number of Gaps
            coverage = int(col[3])/float(col[2])
            rows = self.__db.insert_blast(blast_id, self.__db.accessions[accession], col[3], col[6], col[9], col[8], 
                                          "%3.2f" % coverage, score)
            if not rows:
                print 'There was an error inserting protein result'
                success = False
                break
        
        return success

    def insert_primers(self, sccmec_id):
        # '6 qseqid nident qlen length sstart send pident ppos bitscore evalue'
        success = True
        blast = self.__output['primer'].split('\n')
        for i in xrange(len(blast) - 1):
            col = blast[i].split('\t')
            name = col[0].split('|')
            if len(name) == 1:
                name.append(name[0])
                
            gene = name[0]
            primer = name[1]
            
            if not gene in self.__db.genes:
                self.__db.insert_gene(gene)   
                
            if not primer in self.__db.primers:
                self.__db.insert_primer(self.__db.genes[ gene ], primer)
            coverage = int(col[3])/float(col[2])   
            rows = self.__db.query('insert', """INSERT INTO `SCCmecPrimer`(`SCCmecID`, `PrimerID`, `Length`,`Identity`,
                                                                            `Evalue`, `BitScore`, `Coverage`) 
                                                VALUES (%s,%s,%s,%s,%s,%s,%s)""", 
                                    [sccmec_id, self.__db.primers[ primer ]['PrimerID'], col[3], col[6], 
                                     col[9], col[8], "%3.2f" % coverage ])
            if not rows:
                print 'There was an error inserting primer result'
                success = False
                break
        
        return success
        
    def clean_up(self):
        """ Clean up the mess """
        self.__sh.run_command('rm', ['rm', '-rf', self.__out])
        self.__sh.run_command('touch', ['touch', self.__root +'/logs/complete.sccmec'])