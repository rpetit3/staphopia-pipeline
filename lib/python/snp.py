#! /usr/bin/python
import MySQLdb.cursors
import subprocess
import os
import types

class SNPpipeline():

  # Program Locations 
  def __init__(self, input, outdir, reference, name, mkIndex, bwasw, paired, input2, 
                     verbose, debug, argString, annotate, genbank):
    self.__input      = input
    self.__root       = outdir
    self.__fOut       = outdir + "/SNP"
    self.__outdir     = self.__fOut + "/tmp"
    self.__reference  = reference
    self.__name       = name
    self.__mkIndex    = mkIndex
    self.__paired     = paired
    self.__input2     = input2
    self.__bwasw      = bwasw
    self.__verbose    = verbose
    self.__bwaSam     = ""
    self.__logged     = False
    self.__CallCommand('mkdir', ['mkdir', self.__fOut])
    self.__log        = outdir + "/logs/call_snp.log"
    self.__logFH      = open(self.__log, 'w')
    self.__logFH.write(argString + "\n\n")
    self.__logged     = True
    self.__GATKbam    = ""
    self.__GATKfinal  = ""
    self.__DEBUG      = debug
    self.__annotate   = annotate
    self.__genbank    = genbank

    # Mapping
    self.__bwa        = '/usr/bin/bwa'
    self.__samtools   = '/usr/bin/samtools'
    
    # Picard-Tools
    self.__sortsam    = '/usr/local/bin/SortSam.jar'
    self.__samformat  = '/usr/local/bin/SamFormatConverter.jar'
    self.__readgroups = '/usr/local/bin/AddOrReplaceReadGroups.jar'
    self.__bamindex   = '/usr/local/bin/BuildBamIndex.jar'
    self.__seqdiction = '/usr/local/bin/CreateSequenceDictionary.jar'
    
    # SNP / InDel Calling
    self.__gatk       = '/usr/local/bin/GenomeAnalysisTK.jar'
    self.__gems       = '/usr/local/bin/gems'
    self.__varscan    = '/usr/local/bin/VarScan.jar' 
    
    # Other
    self.__bcftools   = '/usr/bin/bcftools'
    self.__vcfutils   = '/usr/local/bin/vcfutils.pl' 
    self.__annotator  = '/usr/local/bin/annotator.pl'     
    
  """ Shell Execution Functions """
  def __CallCommand(self, program, command):
    """ Allows execution of a simple command. """
    out = ""
    err = ""
    p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out,err = p.communicate()
    
    if (type(program) is list):
      o = open(program[1], 'w')
      o.write(out)
      o.close()
      out = ""
      program = program[0]
      
    
    if (self.__logged):
      self.__logFH.write('---[ '+ program +' ]---\n')
      self.__logFH.write('Command: \n' + ' '.join(command) + '\n\n')
      if (len(out) > 0):
        self.__logFH.write('Standard Output: \n' + out + '\n\n')
      if (len(err) > 0):
        self.__logFH.write('Standard Error: \n' + err + '\n\n')
      
    return 1
    
  """ Common Program Calls """     
  def __bwaIndex(self, out):
    """ Make an index of the gicen reference genome. """ 
    self.__CallCommand('mkdir', ['mkdir', '-p', out])
    self.__CallCommand('cp', ['cp', self.__reference, out + "/ref.fa"])
    self.__reference = out + "/ref.fa"
    self.__CallCommand('bwa index', [self.__bwa, 'index', self.__reference])
    self.__CallCommand('CreateSequenceDictionary', ['java', '-jar', self.__seqdiction, 'R='+self.__reference, 'O='+ out + "/ref.dict"])
    self.__CallCommand('samtools faidx', [self.__samtools, 'faidx', self.__reference ])
    
  def __bwaShortReads(self, out):
    """ Make use of bwa aln/samse for shorter reads. """
    self.__bwaSam = out+ "/bwa.sam"
    self.__ifVerbose("   Running BWA aln.")
    self.__CallCommand('bwa aln', [self.__bwa, 'aln', '-e', '10', '-f', out+ "/bwa.sai", self.__reference, self.__input])
    
    if (self.__paired):
      self.__ifVerbose("   Running second set of BWA aln.")
      self.__CallCommand('bwa aln', [self.__bwa, 'aln', '-f', out+ "/bwa2.sai", self.__reference, self.__input2])
      self.__ifVerbose("   Running BWA sampe.")
      self.__CallCommand('bwa sampe', [self.__bwa, 'sampe','-f', self.__bwaSam , self.__reference, out+ "/bwa.sai", 
                                       out+ "/bwa2.sai", self.__input, self.__input2])
    else:
      self.__ifVerbose("   Running BWA samse.")
      self.__CallCommand('bwa samse', [self.__bwa, 'samse', '-f', self.__bwaSam, self.__reference, out+ "/bwa.sai", self.__input])
    
    return 1
    
  def __bwaLongReads(self, out):
    """ Make use of bwa bwasw for longer reads. """
    self.__bwaSam = out+ "/bwa.sam"
    
    if (self.__paired):
      self.__ifVerbose("   Running BWA bwasw on paired end reads.")
      self.__Common.CallCommand('bwa bwasw', [self.__bwa, 'bwasw', '-f', self.__bwaSam, self.__reference, self.__input, self.__input2])
    else:
      self.__ifVerbose("   Running BWA bwasw on single end reads.")
      self.__Common.CallCommand('bwa bwasw', [self.__bwa, 'bwasw', '-f', self.__bwaSam, self.__reference, self.__input])
    
    return 1
    
  def runBWA(self):
    """ Align reads against the reference using bwa."""
    self.__ifVerbose("Running BWA.")
    self.__logFH.write("########## Running BWA. ##########\n")
    bwaOut = self.__outdir + "/bwa"
    self.__CallCommand('mkdir', ['mkdir', '-p', bwaOut])
    
    if (self.__mkIndex):
      self.__ifVerbose("   Building BWA index.")
      self.__bwaIndex(bwaOut + "/index")
    
    if (self.__bwasw):
      self.__bwaLongReads(bwaOut)
    else:
      self.__bwaShortReads(bwaOut)
      
    self.__ifVerbose("")
      
    return 1
    
  def runGATK(self):
    """ Call SNPs and InDels using GATK """
    if (len(self.__bwaSam) > 0 and os.path.isfile(self.__bwaSam)):
      self.__ifVerbose("Calling SNPs/InDels with GATK.")
      self.__logFH.write("########## Calling SNPs/InDels with GATK. ##########\n")
      GATKdir = self.__outdir + "/GATK"
      self.__CallCommand('mkdir', ['mkdir', '-p', GATKdir])
      
      """ Convert SAM to BAM, Format, Sort, and Index BAM file using Picard tools """
      self.__ifVerbose("   Running SamFormatConverter.")
      self.__CallCommand('SamFormatConverter', ['java', '-Xmx4g', '-jar', self.__samformat, 'INPUT='+ self.__bwaSam, 'VALIDATION_STRINGENCY=LENIENT', 'OUTPUT='+ GATKdir +'/GATK.bam'])
      self.__ifVerbose("   Running AddOrReplaceReadGroups.")
      self.__CallCommand('AddOrReplaceReadGroups', ['java', '-Xmx4g', '-jar', self.__readgroups, 'INPUT='+ GATKdir +'/GATK.bam', 'OUTPUT='+ GATKdir +'/GATK_s.bam',
                                                    'SORT_ORDER=coordinate', 'RGID=GATK', 'RGLB=GATK', 'RGPL=Illumina', 'RGSM=GATK', 'RGPU=GATK', 'VALIDATION_STRINGENCY=LENIENT'])
      self.__ifVerbose("   Running BuildBamIndex.")
      self.__CallCommand('BuildBamIndex', ['java', '-Xmx4g', '-jar', self.__bamindex, 'INPUT='+ GATKdir +'/GATK_s.bam', 'VALIDATION_STRINGENCY=LENIENT'])
      
      """ Re-alignment around InDels using GATK """
      self.__ifVerbose("   Running RealignerTargetCreator.")
      self.__CallCommand('RealignerTargetCreator', ['java', '-Xmx4g', '-jar', self.__gatk, '-T', 'RealignerTargetCreator', '-I', GATKdir +'/GATK_s.bam',
                                                    '-R', self.__reference, '-o', GATKdir +'/GATK.intervals'])
      self.__ifVerbose("   Running IndelRealigner.")
      self.__CallCommand('IndelRealigner', ['java', '-Xmx4g', '-jar', self.__gatk, '-T', 'IndelRealigner', '-l', 'INFO', '-I', GATKdir +'/GATK_s.bam',
                                            '-R', self.__reference, '-targetIntervals', GATKdir +'/GATK.intervals', '-o', GATKdir +'/GATK_c.bam'])
      self.__ifVerbose("   Running SortSam.")
      self.__CallCommand('SortSam', ['java', '-Xmx4g', '-jar', self.__sortsam, 'INPUT='+ GATKdir +'/GATK_c.bam', 'SORT_ORDER=coordinate',
                                     'OUTPUT='+ GATKdir +'/GATK_cs.bam', 'VALIDATION_STRINGENCY=LENIENT'])
      self.__ifVerbose("   Running BuildBamIndex.")
      self.__CallCommand('BuildBamIndex', ['java', '-Xmx4g', '-jar', self.__bamindex, 'INPUT='+ GATKdir +'/GATK_cs.bam', 'VALIDATION_STRINGENCY=LENIENT'])
      
      """ Filter out unmapped reads """
      self.__GATKbam = GATKdir +'/GATK_csm.bam'
      self.__ifVerbose("   Running samtools view.")
      self.__CallCommand('samtools view', [self.__samtools, 'view', '-bhF', '4', '-o', self.__GATKbam, GATKdir +'/GATK_cs.bam'])
      self.__ifVerbose("   Running BuildBamIndex.")
      self.__CallCommand('BuildBamIndex', ['java', '-Xmx4g', '-jar', self.__bamindex, 'INPUT='+ self.__GATKbam, 'VALIDATION_STRINGENCY=LENIENT'])
      
      
      """ Call SNPs/InDels with GATK """
      self.__ifVerbose("   Running UnifiedGenotyper.")
      self.__CallCommand('UnifiedGenotyper', ['java', '-Xmx4g', '-jar', self.__gatk, '-T', 'UnifiedGenotyper', '-glm', 'BOTH', '-R', self.__reference, '-dcov', '500',
                                              '-I', GATKdir +'/GATK_csm.bam', '-o', GATKdir +'/GATK.vcf', '-stand_call_conf', '30.0', '-stand_emit_conf', '10.0']) 
      self.__ifVerbose("   Running VariantFiltration.")
      self.__CallCommand('VariantFiltration', ['java', '-Xmx4g', '-jar', self.__gatk, '-T', 'VariantFiltration', '-R', self.__reference, '-filter', '-cluster', '3',
                                               '-window', '10', '-V', GATKdir +'/GATK.vcf', '-o', GATKdir +'/GATK_c.vcf'])
                                             
      """ Filter the VCF file. """
      self.__ifVerbose("   Filtering GATK VCF.")
      self.__GATKfinal = self.__fOut + "/" + self.__name +'_GATK.vcf'
      self.__FilterVCF(GATKdir +'/GATK_c.vcf', self.__GATKfinal)
      if (self.__annotate):
        self.__ifVerbose("   Annotating GATK VCF.")
        self.__CallCommand('annotator.pl', [self.__annotator, self.__GATKfinal, self.__genbank, '100', self.__fOut + "/" + self.__name +'_GATK_annotation.txt'])
      self.__ifVerbose("")
    else:
      self.runBWA()
      self.runGATK()
      
    return 1
    
  def runSamTools(self):
    """ Call SNPs and InDels using SamTools """
    if (len(self.__bwaSam) > 0 and os.path.isfile(self.__bwaSam)):
      self.__ifVerbose("Calling SNPs/InDels with SamTools.")
      self.__logFH.write("########## Calling SNPs/InDels with SamTools. ##########\n")
      samDir = self.__outdir + "/SamTools"
      self.__CallCommand('mkdir', ['mkdir', '-p', samDir])
      
      """ Reindex the reference, Convert SAM to BAM, Format, Sort, and Index BAM file """
      self.__ifVerbose("   Running samtools faidx.")
      self.__CallCommand('samtools faidx', [self.__samtools, 'faidx', self.__reference ])
      self.__ifVerbose("   Running samtools view.")
      self.__CallCommand('samtools view', [self.__samtools, 'view', '-bShF', '4', '-o', samDir +'/SamTools.bam', self.__bwaSam])
      self.__ifVerbose("   Running samtools sort.")
      self.__CallCommand('samtools sort', [self.__samtools, 'sort', samDir +'/SamTools.bam', samDir +'/SamTools_s'])
      self.__ifVerbose("   Running samtools index.")
      self.__CallCommand('samtools index', [self.__samtools, 'index', samDir +'/SamTools_s.bam'])
      
      """ Filter out the un-mapped reads. """
      self.__ifVerbose("   Running samtools view.")
      self.__CallCommand('samtools view', [self.__samtools, 'view', '-bhF', '4', '-o', samDir +'/SamTools_sm.bam', samDir +'/SamTools_s.bam'])
      self.__ifVerbose("   Running samtools index.")
      self.__CallCommand('samtools index', [self.__samtools, 'index', samDir +'/SamTools_sm.bam'])
      
      """ Call SNPs / InDels using mpileup, bcftools, vcfutils. """
      self.__ifVerbose("   Running samtools mpileup.")
      self.__CallCommand(['samtools mpileup', samDir + '/samtools.mpileup'], [self.__samtools, 'mpileup', '-D', '-ugf', self.__reference, samDir +'/SamTools_sm.bam'])
      self.__ifVerbose("   Running bcftools view.")
      self.__CallCommand(['bcftools view', samDir + '/samtools.vcf'], [self.__bcftools, 'view', '-vcg', samDir + '/samtools.mpileup'])
      self.__ifVerbose("   Running vcfutils.pl varFilter.")
      self.__CallCommand(['vcfutils.pl varFilter', samDir + '/samtools_mpileup.vcf'], [self.__vcfutils, 'varFilter', '-D1500', samDir + '/samtools.vcf'])
      
      """ Filter the VCF file. """
      self.__ifVerbose("   Filtering SamTools VCF.")
      self.__FilterVCF(samDir + '/samtools_mpileup.vcf', self.__fOut + "/" + self.__name +'_SamTools.vcf')
      self.__ifVerbose("")
    else:
      self.runBWA()
      self.runSamTools()
    
    return 1
    
    
  def runGeMs(self):
    """ Call SNPs and InDels using GeMs """
    if (len(self.__GATKbam) > 0 and os.path.isfile(self.__GATKbam)):
      self.__ifVerbose("Calling SNPs/InDels with GeMs.")
      self.__logFH.write("########## Calling SNPs/InDels with GeMs. ##########\n")
      gemsDir = self.__outdir + "/GeMs"
      self.__CallCommand('mkdir', ['mkdir', '-p', gemsDir])
      self.__ifVerbose("   Running samtools mpileup.")
      self.__CallCommand(['samtools mpileup', gemsDir + '/gems.mpileup'], [self.__samtools, 'mpileup', '-D', '-sf', self.__reference, self.__GATKbam])
      self.__ifVerbose("   Running GeMs.")
      self.__CallCommand('GeMs', [self.__gems, '-i', gemsDir + '/gems.mpileup', '-o', gemsDir + '/gems.txt', '-d', '1'])
      self.__ifVerbose("   Filtering GeMs output.")
      self.__FilterGeMs(gemsDir + '/gems.txt', self.__fOut + "/" + self.__name +'_GeMs.txt')
      self.__ifVerbose("")
    else:
      self.runGATK()
      self.runGeMs()
      
    return 1
    
  def runVarScan(self):
    """ Call SNPs and InDels using VarScan """
    if (len(self.__GATKbam) > 0 and os.path.isfile(self.__GATKbam)):
      self.__ifVerbose("Calling SNPs/InDels with VarScan.")
      self.__logFH.write("########## Calling SNPs/InDels with VarScan. ##########\n")
      varScanDir = self.__outdir + "/VarScan"
      self.__CallCommand('mkdir', ['mkdir', '-p', varScanDir])
      self.__ifVerbose("   Running samtools mpileup.")
      self.__CallCommand(['samtools mpileup', varScanDir + '/VarScan.mpileup'], [self.__samtools, 'mpileup', '-D', '-f', self.__reference, self.__GATKbam])
      self.__ifVerbose("   Running VarScan mpileup2snp.")
      self.__CallCommand(['varscan mpileup2snp', self.__fOut + "/" + self.__name +'_VarScan_SNP.txt'],['java', '-Xmx4g', '-jar', self.__varscan, 'mpileup2snp', 
                          varScanDir + '/VarScan.mpileup', '--min-coverage', '3', '--min-reads2', '3', '--min-avg-qual', '17', '--min-freq-for-hom', '0.80'])
      self.__ifVerbose("   Running VarScan mpileup2indel.")
      self.__CallCommand(['varscan mpileup2indel', self.__fOut + "/" + self.__name +'_VarScan_InDel.txt'],['java', '-Xmx4g', '-jar', self.__varscan, 'mpileup2indel', 
                          varScanDir + '/VarScan.mpileup', '--min-coverage', '3', '--min-reads2', '3', '--min-avg-qual', '17', '--min-freq-for-hom', '0.80'])
      self.__ifVerbose("")
    else:
      self.runGATK()
      self.runVarScan()
    
    return 1
      
  def __FilterGeMs(self, input, output):
    """ Filter input GeMs file. """
    fh = open(input, 'r')
    o  = open(output, 'w')
    
    while 1:
      line = fh.readline().rstrip()
      if not line:
        break
        
      cols = line.split('\t')  
      if (cols[2] != cols[3]):
        o.write(line + "\n")
        
    o.close()
    fh.close()
    
    return 1
      
      
  def __FilterVCF(self, input, output):
    """ Filter input VCF file. """
    import re
    fh = open(input, 'r')
    o  = open(output, 'w')
    c = 0

    
    while 1:
      line = fh.readline().rstrip()
      AD = ""
      DP = ""
      AF = ""
      GQ = ""
      G  = ""
      Q  = ""
      t  = False
      if not line:
        break

      if (c == 1):
        cols   = line.split('\t')
        
        j = cols[7].split(';')
        for i in range(len(j)):
          if re.search('^DP4', j[i]):
            b  = j[i].split('=')
            d  = b[1].split(',')
            AD = float(d[2]) + float(d[3]) 
        
        j = cols[8].split(':')
        for i in range(len(j)):
          if (j[i] == "DP"):
            DP = i
          elif (j[i] == "AD"):
            AD = i
            t  = True
          elif (j[i] == "GQ"):
            GQ = i
            
        j = cols[9].split(':')
        
        if (t):
          if re.search(',', j[AD]):
            a = j[AD].split(',')
            AF = float(a[1]) / float(j[DP])
          else:
            AF = AD / float(j[DP])
        else:
          AF = AD / float(j[DP])
          
        if (float(j[DP]) > 2 and AF > 0.70):
          if (float(cols[5]) < 20):
            Q = ",LowQual"
            
          if (float(j[GQ]) < 20):
            G = ",Low GQ"
          
          if (AF >= 0.95):
            o.write(line + "\tSuperpass" + Q + G + "\n")
          else:
            o.write(line + "\tPass" + Q + G + "\n")
        else:
          o.write(line + "\tFail\n")
      elif re.search('^#CHROM', line):
        o.write(line + "\tRemarks\n")
        c = 1
      elif re.search('^#', line):
        o.write(line + "\n")
        
    o.close()
    fh.close()
    
    return 1

  def CleanUp(self):
    """ Clean up the temporary files, and move them to a proper folder. """
    self.__CallCommand('bzip2', ['bzip2', '--best', self.__GATKfinal])
    self.__CallCommand('mv', ['mv', self.__GATKfinal +".bz2", self.__root])
    self.__CallCommand('rm', ['rm', '-r', self.__fOut])
    self.__CallCommand('touch', ['touch', self.__root +'/logs/complete.snp'])

  def __ifVerbose(self, msg):
    """ If verbose print a given message. """
    if (self.__verbose):
      print msg
