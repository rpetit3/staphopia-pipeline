#! /usr/bin/python
"""
    Author: Robert A. Petit III
    Date: 5/2/2013
    Version: v0.1

    Staphopia's methods to work with SGE (qstat, qsub, etc...).
  
    Change Log:
        v0.1
            - Initial set up.
"""
from common import Shell

class SGE(object):
    def __init__(self, id, out):
        self.__id = id
        self.__out = out
        self.__logs = out +'/logs/sge'
        self.__scripts = out +'/scripts'
        self.__bin = '/staphopia-ebs/staphopia-pipeline/bin'
        self.__sh = Shell(out +'/logs/sge/qsub.log', 'qsub')
        
    def qsub(self, script):
        self.__sh.run_command('qsub', ['qsub', script])
        return self.__sh.stdout
        
    def __write_header(self, fh, script, cpu, job):
        fh.write('#!/bin/bash\n')
        fh.write('#$ -wd '+ self.__out +'\n')
        fh.write('#$ -V\n')
        fh.write('#$ -N j'+ self.__id +'_'+ job +'\n')
        fh.write('#$ -S /bin/bash\n')
        fh.write('#$ -pe orte '+ cpu +'\n')
        fh.write('#$ -o '+ self.__logs +'/'+ script +'.out\n')
        fh.write('#$ -e '+ self.__logs +'/'+ script +'.err\n')
        fh.write('export PYTHONPATH=/staphopia-ebs/staphopia-pipeline/lib/python/:$PYTHONPATH\n')
        fh.write('export PYTHONPATH=/staphopia-ebs/lib/python2.7/site-packages:$PYTHONPATH\n')
        fh.write('export PATH=/staphopia-ebs/staphopia-pipeline/bin/:$PATH\n')
        fh.write('export PATH=/staphopia-ebs/staphopia-pipeline/bin/Third-Party:$PATH\n')
        fh.write('\n')
     
    def write_bzip2(self, file):
        script = self.__scripts +'/99_bzip2.sh'
        fh = open(script, 'w')
        self.__write_header(fh, name, '1', '50')
        fh.write('#!/bin/bash\n')
        fh.write('#$ -wd '+ self.__out +'\n')
        fh.write('#$ -V\n')
        fh.write('#$ -S /bin/bash\n')
        fh.write('#$ -pe orte 1\n')
        fh.write('#$ -o '+ self.__logs +'/bzip2.out\n')
        fh.write('#$ -e '+ self.__logs +'/bzip2.err\n')
        fh.write('\n')
        fh.write('bzip2 --best '+ file +'\n')
        fh.close()
        
        return script
        
    def write_process(self, sample_id, fastq, old, debug, qsub):
        script = self.__scripts +'/01_process.sh'
        fh = open(script, 'w')
        self.__write_header(fh, 'process', '1', '1')

        fh.write('cp -r '+ old +'/logs '+ self.__out +' \n')
        fh.write('cp -r '+ old +'/scripts '+ self.__out +' \n')
        fh.write('cp -r '+ old +'/*.fastq '+ self.__out +' \n')

        if not debug:
            fh.write('rm -rf '+ old +'\n')
        else:
            fh.write('rm '+ old +'/staphopia_sample_original.fastq\n')
            
        fh.write('\n')
        cmd  = 'python '+ self.__bin +'/process_fastq.py --out '+ self.__out +' --fastq '+ fastq +' --jobid '+ self.__id
        
        if debug:
            cmd += ' --debug'
            
        if qsub:
            cmd += ' --qsub'
        
        cmd += ' --sample_id '+ sample_id +' --original\n'
        fh.write(cmd)
        
        fh.close()
        
        return script
    
    def write_assembly(self, fastq, sample_id, min, max, mean, debug, qsub):
        script = self.__scripts +'/02_assembly.sh'
        fh = open(script, 'w')
        self.__write_header(fh, 'assembly', '3', '2')
        fh.write('export OMP_NUM_THREADS=1\n')   
        fh.write('export OMP_THREAD_LIMIT=2\n')   
        fh.write('\n')

        cmd  = 'python '+self.__bin +'/assemble_fastq.py --out '+ self.__out +' --fastq '+ fastq +' --jobid '+ self.__id
        
        if debug:
            cmd += ' --debug'
            
        if qsub:
            cmd += ' --qsub'
            
        cmd += ' --sample_id '+ sample_id +' --min '+ str(min) +' --max '+ str(max) +' --mean '+ str(int(mean)) +'\n'
        fh.write(cmd)        
        fh.close()
        
        return script
    
    def write_mlst(self, fastq, assembly, sample_id, blastdb, mean, debug):
        script = self.__scripts +'/03_mlst.sh'
        fh = open(script, 'w')
        self.__write_header(fh, 'mlst', '3', '3')
        cmd  = 'python '+ self.__bin +'/sequence_type.py --out '+ self.__out +' --fastq '+ fastq +'  --mean '+ str(mean)
        
        if debug:
            cmd += ' --debug'
        
        cmd += ' --assembly '+ assembly +' --jobid '+ self.__id +' --sample_id '+ sample_id +' --blastdb '+ blastdb +'\n'
        fh.write(cmd)
        fh.close()
        
        return script
        
    def write_sccmec(self, fastq, sample_id, blastdb, mean, debug):
        script = self.__scripts +'/03_sccmec.sh'
        fh = open(script, 'w')
        self.__write_header(fh, 'sccmec', '3', '4')
        cmd  = 'python '+ self.__bin +'/sccmec_type.py --out '+ self.__out +' --fastq '+ fastq +'  --mean '+ str(mean)
        
        if debug:
            cmd += ' --debug'
        
        cmd += ' --jobid '+ self.__id +' --sample_id '+ sample_id +' --blastdb '+ blastdb +'\n'
        fh.write(cmd)
        fh.close()
        
        return script
         
    def write_resistance(self, sample_id, blastdb, debug):
        script = self.__scripts +'/03_resistance.sh'
        fh = open(script, 'w')
        self.__write_header(fh, 'resistance', '3', '5')
        cmd  = 'python '+ self.__bin +'/resistance_profile.py --blastdb '+ blastdb +' --jobid '+ self.__id
        
        if debug:
            cmd += ' --debug'
        
        cmd += ' --out '+ self.__out +' --sample_id '+ sample_id +'\n'
        fh.write(cmd)
        fh.close()
        
        return script
        
    def write_virulence(self, sample_id, blastdb, debug):
        script = self.__scripts +'/03_virulence.sh'
        fh = open(script, 'w')
        self.__write_header(fh, 'virulence', '3', '6')
        cmd  = 'python '+ self.__bin +'/virulence_profile.py --blastdb '+ blastdb +' --jobid '+ self.__id
        
        if debug:
            cmd += ' --debug'
        
        cmd += ' --out '+ self.__out +' --sample_id '+ sample_id +'\n'
        fh.write(cmd)
        fh.close()
        
        return script
        
    def write_snp(self, sample_id, fastq, name, debug):
        script = self.__scripts +'/03_snp.sh'
        fh = open(script, 'w')
        self.__write_header(fh, 'snp', '3', '7')

        cmd  = 'python '+ self.__bin +'/call_snp.py -q '+ fastq +' --jobid '+ self.__id
        
        if debug:
            cmd += ' --debug'
        
        cmd += ' --out '+ self.__out +' --sample_id '+ sample_id +' --name '+ name +'\n'
        fh.write(cmd)
        fh.close()
        
        return script
        
    def write_cleanup(self, fastq):
        script = self.__scripts +'/04_cleanup.sh'
        fh = open(script, 'w')
        self.__write_header(fh, 'cleanup', '1', '8')

        cmd   = 'python '+ self.__bin +'/cleanup.py -q '+ fastq +' --out '+ self.__out +'\n'
        cmd  += 'python '+ self.__bin +'/check_finished.py \n'
        fh.write(cmd)
        fh.close()
        
        return script
        
    def write_check(self, job, jobscript, sample_id):
        script = self.__scripts +'/99_'+ job +'.sh'
        fh = open(script, 'w')
        self.__write_header(fh, job +'check', '4', '99')
        cmd  = 'python '+ self.__bin +'/check_jobs.py --job '+ job +' --jobid '+ self.__id +' --out '+ self.__out
        cmd += ' --script '+ jobscript +' --sample_id '+ sample_id +' --check '+ script +'\n'
        fh.write(cmd)
        fh.close()
        
        return script
        