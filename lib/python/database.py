#! /usr/bin/python
import MySQLdb.cursors

"""
    Author: Robert A. Petit III
    Date: 4/30/2013
    Version: v0.1

    Staphopia's database commands.
  
    Change Log:
        v0.1
            - Initial set up.
"""
class Database(object):
    
    def __init__(self):
        self.db = {
            'host':'staphopiadev.cus0ahi8gdoj.us-east-1.rds.amazonaws.com',
            'port':29466,
            'database':'staphopia',
            
            # Select user credentials
            'select':{
                'username':'DeepThought',
                'password':'da208f6115574ef429684fa388b04fc958da12c7d2bd2c4d'
            },
            
            # Insert user credentials
            'insert':{
                'username':'Earth',
                'password':'50328320fc2f121a80a2061b804fae0615379f82822fa22b'
            },
            
            # Delete user credentials
            'delete':{
                'username':'Krikkit',
                'password':'91d424e389e75d4be12787bc2814e971fa8f6239cdc0000c'
            },
            
            # Update user credentials
            'update':{
                'username':'HeartOfGold',
                'password':'8d485021b098e4ece1b6d8775102cfc425c816808a8960e0'
            },
            
            # Transaction 
            'transaction':{
                'username':'Slartibartfast',
                'password':'f15b12d0ab5806659ff754bea7f6a6de77929dd2bf5fcc3a'
            },
            
            # MySQL Dump
            'dump':{
                'username':'Whale',
                'password':'938e494346d1a407fdbe579fb7d964395b5bab711fad3095'
            }
        }
        
        self.__dbh = {
            'select':False,
            'insert':False,
            'delete':False,
            'update':False,
            'transaction':False,
            'dump':False
        }
        
    def __connect(self, conn):
        if not self.__dbh[conn]:
            self.__dbh[conn] = MySQLdb.connect(db=self.db['database'], host=self.db['host'], port=self.db['port'],
                                                     user=self.db[conn]['username'], passwd=self.db[conn]['password'], 
                                                     cursorclass=MySQLdb.cursors.DictCursor)
                                                     
    def query(self, conn, sql, values):
        self.__connect(conn)
        cur = self.__dbh[conn].cursor()
        rows = False
        try:
            cur.execute(sql, tuple(values))
            self.__dbh[conn].commit()
            
            if conn == 'select':
                rows = cur.fetchall()
            elif cur.lastrowid:
                rows = cur.lastrowid
            else:
                rows = cur.rowcount
        except MySQLdb.Warning, e:
            print "An exception occurred"
            print e
            self.__dbh[conn].rollback()
                
        cur.close()
        return rows        

    """ 
        Common database methods
    """
    def select_program(self, name, version):
        rows = self.query('select', """SELECT ProgramID
                                       FROM Program
                                       WHERE Name=%s AND Version=%s
                                       LIMIT 1""", [name, version])
        if rows:
            return rows[0]['ProgramID']
        else:
            return self.insert_programs(name, version)
        
    def insert_programs(self, name, version):
        return self.query('insert', """INSERT INTO Program (Name, Version)
                                       VALUES (%s, %s)""", [name, version])
                                       
    def select_accessions(self):
        self.accessions = {}
        rows = self.query('select', """SELECT AccessionID, Accession FROM Accession""", [])
        if rows:
            for i in xrange(len(rows)):
                self.accessions[ rows[i]['Accession'] ] = rows[i]['AccessionID']
            return True
        else:
            return False
        
    def insert_accession(self, database_id, gene_id, accession):
        accession_id = self.query('insert', """INSERT INTO `Accession`(`GeneID`, `DatabaseID`, `Accession`) 
                                               VALUES (%s,%s,%s)""", [gene_id, database_id, accession])
        if accession_id:
            self.accessions[ accession ] = accession_id
            return accession_id
        else:
            return False
    
    def select_genes(self):
        self.genes = {}
        rows = self.query('select', """SELECT GeneID, Gene FROM Gene""", [])
        if rows:
            for i in xrange(len(rows)):
                self.genes[ rows[i]['Gene'] ] = rows[i]['GeneID']
            return True
        else:
            return False
            
    def insert_gene(self, gene):
        gene_id = self.query('insert', """INSERT INTO `Gene`(`Gene`) VALUES (%s)""", [gene])
        if gene_id:
            self.genes[ gene ] = gene_id
            return gene_id
        else:
            return False
            
    def select_databases(self):
        self.databases = {}
        rows = self.query('select', """SELECT `DatabaseID`,`Database` FROM `Database`""", [])
        if rows:
            for i in xrange(len(rows)):
                self.databases[ rows[i]['Database'] ] = rows[i]['DatabaseID']
            return True
        else:
            return False
            
    def insert_database(self, database):
        database_id = self.query('insert', """INSERT INTO `Database` (`Database`) VALUES (%s)""", [database])
        if database_id:
            self.databases[ database ] = database_id
            return database_id
        else:
            return False
            
    def new_blast(self, program_id, sample_id):
        blast_id = self.query('insert', 
                              """INSERT INTO `Blast` (`ProgramID`, `SampleID`) 
                                 VALUES (%s, %s)""", [program_id, sample_id])
        if blast_id:
            return blast_id
        else:
            return False
            
    def insert_blast(self, blast_id, accession_id, length, identity, evalue, bitscore, coverage, score):
        return self.query('insert', """INSERT INTO `BlastResult`(`BlastID`, `AccessionID`, `Length`, `Identity`, 
                                                                  `Evalue`, `BitScore`, `Coverage`, `Score`) 
                                       VALUES (%s,%s,%s,%s,%s,%s,%s,%s)""", [blast_id, accession_id, length, identity,
                                                                             evalue, bitscore, coverage, score])
                                                                             
    def select_primers(self):
        self.primers = {}
        rows = self.query('select', """SELECT `PrimerID`, `GeneID`, `Primer` FROM `Primer`""", [])
        if rows:
            for i in xrange(len(rows)):
                self.primers[ rows[i]['Primer'] ] = {}
                self.primers[ rows[i]['Primer'] ]['PrimerID'] = rows[i]['PrimerID']
                self.primers[ rows[i]['Primer'] ]['GeneID'] = rows[i]['GeneID']
            return True
        else:
            return False
        
    def insert_primer(self, gene_id, primer):
        primer_id = self.query('insert', """INSERT INTO `Primer`(`GeneID`, `Primer`) 
                                            VALUES (%s,%s)""", [gene_id, primer])
        if primer_id:
            self.primers[ primer ] = {}
            self.primers[ primer ]['PrimerID'] = primer_id
            self.primers[ primer ]['GeneID'] = gene_id
            return primer_id
        else:
            return False
            
    def update_finished(self):
        rows = self.query('select', """ SELECT s.SampleID
                                        FROM `Sample` AS s
                                        LEFT JOIN `Job` AS j
                                        ON s.MD5sum=j.`MD5sum`
                                        WHERE j.`Valid`=3 AND j.`Filter`=3 AND j.`Assembly`=3 AND j.`MLST`=3 
                                          AND j.`SCCmec`=3 AND j.`Resistance`=3 AND j.`Virulence` AND 
                                              s.`IsFinished`=FALSE """, [])
                                              
        if rows:
            print "%s job(s) to be updated." % len(rows)
            for i in xrange(len(rows)):
                print "Updating %s..." % rows[i]['SampleID']
                update = self.query('update', """ UPDATE `Sample` 
                                                  SET `IsFinished`=TRUE 
                                                  WHERE `SampleID`=%s """, [rows[i]['SampleID']])
        else:
            print "Nothing to do."
        