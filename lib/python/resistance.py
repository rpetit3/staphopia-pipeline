#! /usr/bin/python
"""
     Author: Robert A. Petit III
       Date: 5/18/2013
    Version: v0.1

    Staphopia's resistance typing algorithm.
  
    Change Log:
        v0.1
            - Initial set up.
"""
import os
from common import Shell
from database import Database

class ResistanceType(object):
    
    def __init__(self, log, args, blastdb):
        self.VERSION = "0.1"
        self.__blastdb = blastdb
        self.__sh = Shell(log, args)
        self.__db = Database()
        self.__fasta = '/staphopia-ebs/staphopia-pipeline/fasta/resistance/resistance.fasta'

    def run_blast(self):
        self.__sh.run_command('tblastn', ['tblastn', '-query', self.__fasta, '-db', self.__blastdb, 
                                          '-num_threads', '4', '-max_target_seqs', '1', '-outfmt', 
                                          '6 qseqid nident qlen length sstart send pident ppos bitscore evalue gaps'])
        self.__blast = self.__sh.stdout
        
        
    def print_results(self):
        print 'qseqid nident qlen length sstart send pident ppos bitscore evalue gaps'
        print self.__blast
        
    def insert_stats(self, sample_id, runtime):
        # Get ProgramID and BlastID
        program_id = self.__db.select_program('tblastn', self.__sh.blast_version('tblastn'))
        if program_id:
            blast_id = self.__db.new_blast(program_id, sample_id)
            if blast_id:
                self.__db.select_accessions()
                self.__db.select_genes()
                self.__db.select_databases()
                proteins = self.insert_proteins(blast_id)
                
                if proteins:
                    rows = self.__db.query('insert', """INSERT INTO `Resistance` (`SampleID`, `BlastID`, `Version`, 
                                                                                  `RunTime`) 
                                                        VALUES (%s,%s,%s,%s)""", [sample_id, blast_id, 
                                                                                  self.VERSION, runtime])
                    if rows:
                        return True
                    else:
                        return False
                else:
                    print 'There was an error inserting blast results.'
            else:
                print 'There was an error creating a new BLAST entry.'
        else:
            print 'There was an error inserting/retrieving protein blast program id.'
                
    def insert_proteins(self, blast_id):
        # '6 qseqid nident qlen length sstart send pident ppos bitscore evalue gaps'
        success = True
        blast = self.__blast.split('\n')
        for i in xrange(len(blast) - 1):
            col = blast[i].split('\t')
            database, accession, full_accession = col[0].split('|')
                            
            score = int(col[2]) - int(col[3]) + int(col[10]) # (Query Length - Alignment Length) + Number of Gaps
            coverage = int(col[3])/float(col[2])
            rows = self.__db.insert_blast(blast_id, self.__db.accessions[accession], col[3], col[6], col[9], col[8], 
                                          "%3.2f" % coverage, score)
            if not rows:
                print 'There was an error inserting protein result'
                success = False
                break
        
        return success
        
    def update_status(self, status, jobid):
        return self.__db.query('update', """UPDATE Job 
                                            SET Resistance=%s 
                                            WHERE JobID=%s""", [status, jobid])
                                      