#! /usr/bin/python
"""
    Author: Robert A. Petit III
    Date: 7/8/2013
    Version: v0.1

    Monitor job activity and resubmit jobs that might have failed.
  
    Change Log:
        v0.1
            - Initial set up.
"""
import os
import subprocess
from database import Database
from queue import SGE

class Jobs(object):

    def __init__(self):
        self.VERSION = "0.1" 
        self.__db = Database()
        self.__sge = SGE('RERUN', 'RERUN')
        self.__jobs = {
            'Valid':'0', 
            'Filter':'1', 
            'Assembly':'2', 
            'MLST':'3', 
            'SCCmec':'4', 
            'Resistance':'5', 
            'Virulence':'6'   
        }
        
        self.__scripts = {
            'Valid':'00_validate.sh', 
            'Filter':'01_process.sh', 
            'Assembly':'02_assembly.sh', 
            'MLST':'03_mlst.sh', 
            'SCCmec':'03_sccmec.sh', 
            'Resistance':'03_resistance.sh', 
            'Virulence':'03_virulence.sh'    
        }

    def run_command(self, command):
        """ Allows execution of a simple command. """
        self.stderr = None
        self.stdout = None 
        p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.stdout, self.stderr = p.communicate()

    def unfinished_samples(self):
        return self.__db.query('select', '''SELECT JobID, UserName, SampleTag, Queued, Begun, Valid, Filter, Assembly, 
                                                   MLST, SCCmec, Resistance, Virulence 
                                            FROM  `Sample` AS s
                                            INNER JOIN  `Job` AS j 
                                            ON j.MD5sum = s.MD5sum
                                            INNER JOIN `User` AS u
                                            ON j.UserID=u.UserID
                                            WHERE s.IsFinished=FALSE''', [])
        
    def check_invalid(self):
        return self.__db.query('select', '''SELECT JobID, MD5sum, Queued, Begun, Valid
                                            FROM Job
                                            WHERE Valid >= 8''', [])
                                          
    def insert_rerun(self, jobid, job):
        return self.__db.query('insert', '''INSERT INTO `JobRerun`(JobID, Job) 
                                            VALUES (%s,%s)''', [jobid, self.__jobs[job]])
        
    def check_rerun(self, jobid, job):
        return self.__db.query('select', '''SELECT RunID, Reruns
                                            FROM JobRerun
                                            WHERE JobID=%s AND Job=%s
                                            LIMTI 1''', [jobid, job])
                                            
    def increment_rerun(self, runid, count):
        return self.__db.query('update', '''UPDATE JobRerun
                                            SET Reruns=%s
                                            WHERE RunID=%s''', [count+1, runid])
                                            

    def submit_job(self, dir, job):
        self.run_command(['qsub', dir + '/' + self.__scripts[ job ]])
        return self.stdout
        
    def manage_job(self, jobid, job, status, script_dir, prev):
        qstat = self.__job_status('j'+ jobid +'_'+ self.__jobs[ job ])
        resubmit = False
        
        if status == 0 and os.path.exists(script) and not qstat:
            resubmit = True    
        elif status == 1 and qstat != 'qw':
            # might be running and database is slow to update
            if qstat != 'r':
                resubmit = True
        elif status == 2 and qstat != 'r':
            resubmit = True
        elif status == 0 and not os.path.exists(script:
            job = prev
            resubmit = True
        else:
            resubmit = True
            
            
        if resubmit:
            # Has it been rerun?
            reruns = self.check_rerun(jobid, job)
            
            if reruns[0]['RunID']:
                if reruns[0]['Reruns'] < 5:
                    j.increment_rerun(reruns[0]['RunID'])
                    print j.submit_job(script_dir, job)
                    print '%s for job %s, retry number %s.' % job, jobid, reruns[0]['RunID']
                else:
                    print 'Job %s has been rerun more 5 times, this has been logged.' % rows[0]['JobID']
            else: 
                rerun = j.insert_rerun(rows[i]['JobID'], 'Valid')
                print j.submit_job(script_dir, job)
                print '%s for job %s, retry number 1.' % job,rows[0]['JobID']
            return True
        else:
            return False

        
    def __job_status(self, name):
        
        status = False
        self.run_command([qstat])
        qstat = self.stdout.split('\n')
        
        for i in xrange(len(qstat)):
            if name in qstat[i]:
                cols = qstat[i].split('\t')
                status = cols[4]
                break
            
        return status
