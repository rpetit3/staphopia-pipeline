#! /usr/bin/env perl

package Staphopia;
use strict;
use warnings;
use Bio::SearchIO;
use Bio::AlignIO;
use Sort::Fields;
use DBI;

use Exporter;
our @ISA = qw(Exporter);
our @EXPORT = qw(denovo insertStats);

use constant {
      TBLASTN => "/data1/home/rpetit/bin/tblastn",
       BLASTN => "/data1/home/rpetit/bin/blastn",
    VIRULENCE => "/data1/home/rpetit/staphopia-pipeline/fasta/virulence/virulence",
   RESISTANCE => "/data1/home/rpetit/staphopia-pipeline/fasta/resistance/resistance"
};

sub denovo {
  my ($blastdb, $sid) = @_;
  # Blast the consesus against virulence and resistance genes
  my $thr1 = threads->create(\&runBLAST, $blastdb, VIRULENCE);
  my $thr2 = threads->create(\&blastRes, $blastdb, RESISTANCE);

  my $virulence_ref  = $thr1->join();
  my $resistance_ref = $thr2->join();
  
  # Insert Virulence and Resistance Info
  insertVirulence($virulence_ref, $sid);
  insertResistance($resistance_ref, $sid);

  return 1;
}

### Accessory
sub getAccession {
  my ($accessory) = @_;
  my %accession_db;

  open(ACCESSION, "$accessory.accession");
  while (my $line = <ACCESSION>) {
    my $gene = (split(/\t/, $line))[0];
    my $accession = (split(/\t/, $line))[1];
    $accession =~ s/\s//;
    if ($accessory =~ /resistance/) {
      my $ardb_status = (split(/\t/, $line))[2];
      chomp($ardb_status);
    
      $accession_db{ $accession } = "$gene\t$ardb_status";
    }
    else {
      $accession_db{ $accession } = $gene;
    }
  }
  close(ACCESSION);

  return (\%accession_db);
}


sub getCategory {
  my ($accessory) = @_;
  my %category_db;

  open(CATEGORY, "$accessory.category");
  while (my $line = <CATEGORY>) {
    chomp($line);
    my $gene = (split(/\t/, $line))[0];
    my $category = (split(/\t/, $line))[1];
    
    $category_db{ $gene } = $category;
  }
  close (CATEGORY);

  return (\%category_db);
}

sub blastRes {
    my ($blastdb, $accessory) = @_;
    my $input = $accessory . ".fasta";
    my $outfmt = "'6 qseqid length pident evalue bitscore qlen'";
    my $blast_command = TBLASTN . " -query $input -db $blastdb -num_threads 8 -max_target_seqs 1 -outfmt $outfmt";

    my @hits = `$blast_command`;
    return (\@hits);
}

sub runBLAST {
  my ($blastdb, $accessory) = @_;
  my $input = $accessory . ".fasta";
  my $outfmt = "'6 qseqid sseqid pident evalue bitscore qlen length gaps'";
  my $blast_command = TBLASTN . " -query $input -db $blastdb -num_threads 8 -max_target_seqs 1 -outfmt $outfmt";
  my $previous_gene = "";
  my $count = 1;

  my @accepted_results = `$blast_command`;
  
  my $accession_db_ref = getAccession($accessory);
  my $category_db_ref = getCategory($accessory);
  my $hits = scalar(@accepted_results);
  for (my $i = 0; $i < $hits; $i++) {
    my $accession = (split(/\t/, $accepted_results[$i]))[0];
  
    if ($accessory =~ /resistance/) {
      my $value = $$accession_db_ref{ $accession };
      my $ardb_status = (split(/\t/, $value))[1];
      my $gene = (split(/\t/, $value))[0];
      my $category = $$category_db_ref{ $gene };

      $accepted_results[$i] = "$gene\t$category\t$ardb_status\t$accepted_results[$i]"; 
    }
    else {
      my $gene = $$accession_db_ref{ $accession };
      my $category = $$category_db_ref{ $gene };

      $accepted_results[$i] = "$gene\t$category\t$accepted_results[$i]"; 
    }
  }

  my (@sorted_results, @top_results);
  if ($accessory =~ /resistance/) {
    @sorted_results = fieldsort '\t', [ '2', '1', '-8n' ], @accepted_results;
  }
  else {
    @sorted_results = fieldsort '\t', [ '2', '1', '-7n' ], @accepted_results;
  }

  foreach (@sorted_results) {
    my $line = $_;
    my $gene = (split(/\t/, $line))[0];

    if ($gene ne $previous_gene) {
      $previous_gene = $gene;
      $count = 2;
    
      push(@top_results, $line);
    }
    elsif ($gene eq $previous_gene && $count < 2) {
      push(@top_results, $line);
      $count++;
    }
    else {
      next;
    }
  }

  return (\@top_results);
}

sub insertResistance {
    my ($accessory_ref, $id) = @_;

    # Connect to the DataBase
    my $db_handle = DBI->connect("DBI:mysql:database=rpetit_staphopia;host=localhost", "rpetit", "rpetit", {'RaiseError' => 1});
    
    foreach(@$accessory_ref) {
        my @hit = split(/\t/, $_);
        my $acc = (split(/\|/, $hit[0]))[1];
        my $len = $hit[1];
        my $perc_id = $hit[2];
        my $evalue = $hit[3];
        my $bitscore = $hit[4];
        my $cov = $len / $hit[5] * 100;
        my $score = $bitscore / $len;

        $db_handle->do("INSERT INTO seqResistance(id, acc, len, perc_id, evalue, bitscore, cov, score) 
                        VALUES ($id, '$acc', '$len', '$perc_id', '$evalue', '$bitscore', '$cov', '$score')");
    }

    # Disconnect from the DB
    $db_handle->disconnect();
}
  
sub insertVirulence {
  my ($accessory_ref, $id) = @_;
  
  # Connect to the DataBase
  my $db_handle = DBI->connect("DBI:mysql:database=rpetit_staphopia;host=localhost", "rpetit", "rpetit", {'RaiseError' => 1});

  foreach(@$accessory_ref) {
    my @hit = split(/\t/, $_);
    my $gene      = $hit[0];
    my $category  = $hit[1];
    my $accession = $hit[2];
    my $perc_id   = $hit[4];
    my $evalue    = $hit[5];
    my $bitscore  = $hit[6];
    my $cov       = ($hit[8] - $hit[9])  / $hit[7] * 100;
    my $score     = $hit[7] - $hit[8] + $hit[9];

    $db_handle->do("INSERT INTO seqVirulence (id, gene, category, accession, perc_id, evalue, bit_score, cov, score) 
                    VALUES ($id, '$gene', '$category', '$accession', '$perc_id', '$evalue', '$bitscore', '$cov', '$score')");
  }

  # Disconnect from the DB
  $db_handle->disconnect();
}
